<?
header("Content-Type: application/json; charset=utf-8");
define('STOP_STATISTICS', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<?
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
global $USER, $APPLICATION;

switch ($_REQUEST['action']) {
    case 'addOneClickOrder':
		$name = htmlspecialchars($_REQUEST['name']);
		$email = htmlspecialchars($_REQUEST['email']);
		$phone = htmlspecialchars($_REQUEST['phone']);
		$time = htmlspecialchars($_REQUEST['time']);
        $productId = intval($_POST['productId']);

        $arProduct = CIBlockElement::GetByID($productId)->Fetch();

        $arFields = array(
			'IBLOCK_ID' => IB_ONECLICKORDER,
			'ACTIVE' => 'Y',
			'NAME' => $name,
			'PROPERTY_VALUES' => [
				'EMAIL' => $email,
				'PHONE' => $phone,
				'TIME' => $time,
				'PRODUCT' => $arProduct['XML_ID']
			]
        );

        $element = new CIBlockElement;
        if ($oneClickOrder = $element->Add($arFields)) {
            $arFields = array(
				'LINK' => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=12&type=content&ID={$oneClickOrder}&lang=ru&find_section_section=0&WF=Y",
				'NAME' => $name,
				'EMAIL' => $email,
				'PHONE' => $phone,
				'TIME' => $time,
				'PRODUCT' => $arProduct['NAME']
            );
            CEvent::Send('SALE_ONE_CLICK_ORDER', 's1', $arFields);

            $arResult['STATUS'] = 'SUCCESS';
        } else {
            $arResult['STATUS'] = 'FAIL';
            $arResult['MESSAGE'] = "Произошла ошибка: {$element->LAST_ERROR}";
        }
        break;

    case 'addFeedback':
		$name = htmlspecialchars($_REQUEST['name']);
		$email = htmlspecialchars($_REQUEST['email']);
		$phone = htmlspecialchars($_REQUEST['phone']);
		$time = htmlspecialchars($_REQUEST['time']);


        $arFields = array(
			'IBLOCK_ID' => IB_FEEDBACK,
			'ACTIVE' => 'Y',
			'NAME' => $name,
			'PROPERTY_VALUES' => [
				'PHONE' => $phone,
				'TIME' => $time,
			]
        );

        $element = new CIBlockElement;
        if ($feedbackId = $element->Add($arFields)) {
            $arFields = array(
				'LINK' => "/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=11&type=content&ID={$feedbackId}&lang=ru&find_section_section=0&WF=Y",
				'NAME' => $name,
				'PHONE' => $phone,
				'TIME' => $time,
            );
            CEvent::Send('FEEDBACK_FORM', 's1', $arFields);

            $arResult['STATUS'] = 'SUCCESS';
        } else {
            $arResult['STATUS'] = 'FAIL';
            $arResult['MESSAGE'] = "Произошла ошибка: {$element->LAST_ERROR}";
        }
        break;

    case 'addSubscribe':
		$email = htmlspecialchars($_REQUEST['email']);

        $arFields = Array(
            "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
            "FORMAT" => "html",
            "EMAIL" => $email,
            "ACTIVE" => "Y",
            "RUB_ID" => array(1, 2)
        );
        $subscr = new CSubscription;

        $ID = $subscr->Add($arFields);
        if ($ID > 0) {
            CSubscription::Authorize($ID);
            $arResult['STATUS'] = 'SUCCESS';
        } else {
            $arResult['STATUS'] = 'FAIL';
            $arResult['MESSAGE'] = "Произошла ошибка: {$subscr->LAST_ERROR}";
        }
        break;
    case 'setPayedBonuses':
    	$val = htmlspecialchars($_REQUEST['val']);
    	$_SESSION['payBonuses'] = $val;
    	break;
    case 'getFutureBouquet':
    	$APPLICATION->RestartBuffer();
		?>
<script>
function deleteProductCalc(itemId)
{
  BX.ajax({
	method: 'POST',
	dataType: 'json',
	url: '/ajax/',
	data: {'action': 'deleteFromBasket', 'id': itemId},
	onsuccess: function(result) {
		BX.ajax({
			url: '/ajax/?action=getFutureBouquet',
			onsuccess: function(basketResult) {
				$('#futureBouquet').html($(basketResult)[0].innerHTML);
			}
  		});
		$.get('/ajax/?action=getBasket')
			.done( function(basketResult) {
				$('#desktopBasket').html($(basketResult)[2].innerHTML);
				$('#mobileBasket').html($(basketResult)[0].innerHTML);
			});
	}
  });
}

function setQuantityCalc(itemId, action, currentQuantity)
{
  if (!currentQuantity) {
	  currentQuantity = $('#QUANTITY_INPUT_' + itemId).val();
  }

  var newQuantity = 0;
  if (action == 'set') {
	  newQuantity = parseInt(currentQuantity);
  } else if (action == 'up') {
	  newQuantity = parseInt(currentQuantity) + 1;
  } else {
	  newQuantity = parseInt(currentQuantity) - 1;
  }

  BX.ajax({
	method: 'POST',
	dataType: 'json',
	url: '/ajax/',
	data: {'action': 'setQuantity', 'id': itemId, 'quantity': newQuantity},
	onsuccess: function(result) {
		BX.ajax({
			url: '/ajax/?action=getFutureBouquet',
			onsuccess: function(basketResult) {
				$('#futureBouquet').html($(basketResult)[0].innerHTML);
			}
  		});
		$.get('/ajax/?action=getBasket')
			.done( function(basketResult) {
				$('#desktopBasket').html($(basketResult)[2].innerHTML);
				$('#mobileBasket').html($(basketResult)[0].innerHTML);
			});
	}
  });
}
</script>
<div class="futureBlock" id="basket_items">
    <div class="futureContent">
		<?
    	$flowers = [];
    	$herbal = [];
    	$pack = [];
    	$accessories = [];
    	$presents = [];
    	$flowersPack = [];
    	$useFloatQuantityJS = false;
		foreach ($_SESSION['futureBouquet'] as $itemId => $quantity) {
			$arItem = CIBlockElement::GetByID($itemId)->Fetch();
			$parentCategory = CIBlockSection::GetNavChain(false, $arItem['IBLOCK_SECTION_ID'])->Fetch();

			switch ($parentCategory['ID']) {
				case 2:
					$flowers[$itemId] = $quantity;
					break;
				case 5:
					$accessories[$itemId] = $quantity;
					break;
				case 6:
					$pack[$itemId] = $quantity;
					break;
				case 7:
					$herbal[$itemId] = $quantity;
					break;
				default:
					break;
			}

			if ($arItem['IBLOCK_ID'] == IB_OFFERS) {
				$flowersPack[$itemId] = $quantity;
			}
		}
		if (!empty($flowers) || !empty($flowersPack)) {
			foreach ($flowers as $itemId => $quantity) {
				$arItem = CIBlockElement::GetByID($itemId)->Fetch();
				$arProduct = CCatalogProductProvider::GetProductData(
					[
						'PRODUCT_ID' => $itemId,
						'QUANTITY' => $quantity,
					]
				);

				$totalSum += $arProduct['PRICE'] * $quantity;
				if (strlen($arItem["PREVIEW_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
				} elseif (strlen($arItem["DETAIL_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["DETAIL_PICTURE"]);
				} else {
					$url = SITE_TEMPLATE_PATH . "/img/no_photo.png";
				}
				?>
				<div id="<?=$itemId?>" class="futureItem futureContent-item select f-flowers">
				    <div class="futureTemplate">

				        <img src="<?=$url?>" alt="herbage1">
				        <span class="close-btn" onclick="deleteProductCalc(<?=$itemId?>)"><a href="javascript: void(0)" class="basket-remove"></a></span>
				    </div>
				    <div class="addItem-desc">
				        <h5><?=$arProduct["NAME"]?></h5>
						<div class="spinner-global" >
							<div class="spin-down" onclick="setQuantityCalc(<?=$itemId?>, 'down', 0);">-</div>
							<input type="text" onchange="setQuantityCalc(<?=$itemId?>, 'set', 0)" id="QUANTITY_INPUT_<?=$itemId?>" name="QUANTITY_INPUT_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
							<div class="spin-up" onclick="setQuantityCalc(<?=$itemId?>, 'up', 0);">+</div>
						</div>
						<input type="hidden" id="QUANTITY_<?=$itemId?>" name="QUANTITY_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
				    </div>
				</div>
			<?
			$flowersString .= "<span> {$arProduct['NAME']} - {$quantity} шт.</span>";
			}

			$itemSum = 0;
			foreach ($flowersPack as $itemId => $quantity) {
				$arFilter = ['ID' => $itemId, 'ACTIVE' => 'Y'];
				$arSelect = ['PREVIEW_PICTURE', 'DETAIL_PICTURE', 'PROPERTY_COUNT', 'PROPERTY_CML2_LINK', 'NAME', 'ID'];
				$arItem = CIBlockElement::GetList([], $arFilter, false, false, $arSelect)->Fetch();

				$arProduct = CCatalogProductProvider::GetProductData(
					[
						'PRODUCT_ID' => $itemId,
						'QUANTITY' => $quantity,
					]
				);

				$totalQuantity = CCatalogProduct::GetByID($itemId)['QUANTITY'];

				$totalSum += $arProduct['PRICE'] * $quantity;
				$itemSum = $arProduct['PRICE'] * $quantity;
				if (strlen($arItem["PREVIEW_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
				} elseif (strlen($arItem["DETAIL_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["DETAIL_PICTURE"]);
				} else {
					$url = SITE_TEMPLATE_PATH . "/img/no_photo.png";
				}

				if (strlen($arItem["PREVIEW_PICTURE"]) == 0 && strlen($arItem["DETAIL_PICTURE"]) == 0) {
					$arFilter = [
						'ID' => $itemId,
						'ACTIVE' => 'Y'
					];

					$arParentItemId = $arItem['PROPERTY_CML2_LINK_VALUE'];
					$arParentItem = CIBlockElement::GetByID($arParentItemId)->Fetch();
					if (strlen($arParentItem["PREVIEW_PICTURE"]) > 0) {
						$url = CFile::GetPath($arParentItem["PREVIEW_PICTURE"]);
					} elseif (strlen($arParentItem["DETAIL_PICTURE"]) > 0) {
						$url = CFile::GetPath($arParentItem["DETAIL_PICTURE"]);
					} else {
						$url = SITE_TEMPLATE_PATH . "/img/no_photo.png";
					}
				}

				?>
				<div id="<?=$itemId?>" class="futureItem futureContent-item select f-pack">
					<div class="addItem-top">
				        <div class="rotate<?if ($itemId == $_SESSION['futureBouquetActive']) {?> active<?}?>">
				        <?if ($itemId == $_SESSION['futureBouquetActive']) {
							//unset($_SESSION['futureBouquetActive']);
				        }?>

				          <div class="rotate-prev">
				            <div class="futureTemplate">
				              <img src="<?=$url?>" alt="herbage1">
				              <span class="close-btn" onclick="deleteProductCalc(<?=$itemId?>)"><a href="javascript: void(0)" onclick="deleteProductCalc(<?=$itemId?>)" class="basket-remove"></a></span>
				              <span class="future-edit"></span>
				            </div>
				            <div class="addItem-desc">
				              <h5><?=$arProduct["NAME"]?></h5>
				              <span class="addItem-price"><?=number_format($itemSum, 0, '.', ' ')?> руб.</span>
				              <span class="addItem-number"><?=$quantity?> <?=plural_form($quantity, ['пачка', 'пачки', 'пачек'])?></span>
				            </div>
				          </div>

				          <div class="rotate-next">
			          		<div class="buyFlowers">
				              <span class="close-btn"></span>
				              <div class="buyFlowers-item">
			              		<p>Количество пачек</p>
			              		<div class="b-range" data-itemid="<?=$arItem['ID']?>" data-current="<?=$quantity?>" data-max="<?=$totalQuantity?>"></div>
			              		<div class="number-range">
			              			<input class="range-value-n" type="number" name="quantity" value="<?=$quantity?>"><span><?=$totalQuantity?></span>
			              		</div>
				              </div>

				              <div class="buyFlowers-item">
				                <p>Итого:</p>
				                <div class="buyFlowers-total">
				                  <p><span class="flowers"><?=$arItem['PROPERTY_COUNT_VALUE']?></span> штук - <span class="packPrice" data-price="<?=$arProduct['PRICE']?>"> <?=number_format($arProduct['PRICE'], 0, '.', ' ')?> руб.</span></p>
				                  <p><span class="packs"><?=$quantity?></span> <span class="pluralForm"><?=plural_form($quantity, ['пачка', 'пачки', 'пачек'])?></span> - <span class="buyFlowers-total-b"><?=number_format($itemSum, 0, '.', ' ')?> руб.</span></p>
				                </div>
				              </div>
				            </div>
				          </div>

				        </div>
					</div>
				</div>
			<?
			$flowersString .= "<span> {$arProduct['NAME']} - {$quantity} шт.</span>";
			}
		} else {?>
            <div class="futureContent-item">
                <div class="futureTemplate f-flowers">
                    <p>Цветы</p>
                    <a href="#">+</a>
                </div>
            </div>
		<?}

		if (!empty($accessories)) {
			foreach ($accessories as $itemId => $quantity) {
				$arItem = CIBlockElement::GetByID($itemId)->Fetch();
				$arProduct = CCatalogProductProvider::GetProductData(
					[
						'PRODUCT_ID' => $itemId,
						'QUANTITY' => $quantity,
					]
				);

				$totalSum += $arProduct['PRICE'] * $quantity;
				if (strlen($arItem["PREVIEW_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
				} elseif (strlen($arItem["DETAIL_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["DETAIL_PICTURE"]);
				} else {
					$url = SITE_TEMPLATE_PATH . "/img/no_photo.png";
				}
				?>
				<div id="<?=$itemId?>" class="futureItem futureContent-item select f-access">
				    <div class="futureTemplate">

				        <img src="<?=$url?>" alt="herbage1">
				        <span class="close-btn" onclick="deleteProductCalc(<?=$itemId?>)"><a href="javascript: void(0)" onclick="deleteProductCalc(<?=$itemId?>)" class="basket-remove"></a></span>
				    </div>
				    <div class="addItem-desc">
				        <h5><?=$arProduct["NAME"]?></h5>
						<div class="spinner-global" >
							<div class="spin-down" onclick="setQuantityCalc(<?=$itemId?>, 'down', 0);">-</div>
							<input type="text" onchange="setQuantityCalc(<?=$itemId?>, 'set', 0)" id="QUANTITY_INPUT_<?=$itemId?>" name="QUANTITY_INPUT_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
							<div class="spin-up" onclick="setQuantityCalc(<?=$itemId?>, 'up', 0);">+</div>
						</div>
						<input type="hidden" id="QUANTITY_<?=$itemId?>" name="QUANTITY_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
				    </div>
				</div>
			<?
			$accessoriesString .= "<span> {$arProduct['NAME']} - {$quantity} шт.</span>";
			}
		} else {?>
            <div class="futureContent-item ">
                <div class="futureTemplate f-access">
                    <p>Аксессуары</p>
                    <a href="#">+</a>
                </div>
            </div>
		<?}

		if (!empty($pack)) {
			foreach ($pack as $itemId => $quantity) {
				$arItem = CIBlockElement::GetByID($itemId)->Fetch();
				$arProduct = CCatalogProductProvider::GetProductData(
					[
						'PRODUCT_ID' => $itemId,
						'QUANTITY' => $quantity,
					]
				);

				$totalSum += $arProduct['PRICE'] * $quantity;
				if (strlen($arItem["PREVIEW_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
				} elseif (strlen($arItem["DETAIL_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["DETAIL_PICTURE"]);
				} else {
					$url = SITE_TEMPLATE_PATH . "/img/no_photo.png";
				}
				?>
				<div id="<?=$itemId?>" class="futureItem futureContent-item select f-pack">
				    <div class="futureTemplate">

				        <img src="<?=$url?>" alt="herbage1">
				        <span class="close-btn" onclick="deleteProductCalc(<?=$itemId?>)"><a href="javascript: void(0)" onclick="deleteProductCalc(<?=$itemId?>)" class="basket-remove"></a></span>
				    </div>
				    <div class="addItem-desc">
				        <h5><?=$arProduct["NAME"]?></h5>
						<div class="spinner-global" >
							<div class="spin-down" onclick="setQuantityCalc(<?=$itemId?>, 'down',0);">-</div>
							<input type="text" onchange="setQuantityCalc(<?=$itemId?>, 'set', 0)" id="QUANTITY_INPUT_<?=$itemId?>" name="QUANTITY_INPUT_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
							<div class="spin-up" onclick="setQuantityCalc(<?=$itemId?>, 'up', 0);">+</div>
						</div>
						<input type="hidden" id="QUANTITY_<?=$itemId?>" name="QUANTITY_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
				    </div>
				</div>
			<?
			$packString .= "<span> {$arProduct['NAME']} - {$quantity} шт.</span>";
			}
		} else {?>
            <div class="futureContent-item ">
                <div class="futureTemplate f-pack">
                    <p>Упаковка</p>
                    <a href="#">+</a>
                </div>
            </div>
		<?}

		if (!empty($herbal)) {
			foreach ($herbal as $itemId => $quantity) {
				$arItem = CIBlockElement::GetByID($itemId)->Fetch();
				$arProduct = CCatalogProductProvider::GetProductData(
					[
						'PRODUCT_ID' => $itemId,
						'QUANTITY' => $quantity,
					]
				);

				$totalSum += $arProduct['PRICE'] * $quantity;
				if (strlen($arItem["PREVIEW_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["PREVIEW_PICTURE"]);
				} elseif (strlen($arItem["DETAIL_PICTURE"]) > 0) {
					$url = CFile::GetPath($arItem["DETAIL_PICTURE"]);
				} else {
					$url = SITE_TEMPLATE_PATH . "/img/no_photo.png";
				}
				?>
				<div id="<?=$itemId?>" class="futureItem futureContent-item select f-herb">
				    <div class="futureTemplate">

				        <img src="<?=$url?>" alt="herbage1">
				        <span class="close-btn" onclick="deleteProductCalc(<?=$itemId?>)"><a href="javascript: void(0)" onclick="deleteProductCalc(<?=$itemId?>)" class="basket-remove"></a></span>
				    </div>
				    <div class="addItem-desc">
				        <h5><?=$arProduct["NAME"]?></h5>
						<div class="spinner-global" >
							<div class="spin-down" onclick="setQuantityCalc(<?=$itemId?>, 'down', 0);">-</div>
							<input type="text" id="QUANTITY_INPUT_<?=$itemId?>" onchange="setQuantityCalc(<?=$itemId?>, 'set', 0)" name="QUANTITY_INPUT_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
							<div class="spin-up" onclick="setQuantityCalc(<?=$itemId?>, 'up', 0);">+</div>
						</div>
						<input type="hidden" id="QUANTITY_<?=$itemId?>" name="QUANTITY_<?=$itemId?>" value="<?=$arProduct["QUANTITY"]?>" />
				    </div>
				</div>
			<?
			$herbalString .= "<span> {$arProduct['NAME']} - {$quantity} шт.</span>";
			}
		} else {?>
            <div class="futureContent-item">
                <div class="futureTemplate f-herb">
                    <p>Зелень</p>
                    <a href="#">+</a>
                </div>
            </div>
		<?}?>

	    <div class="composition">
	    	<?if ($flowersString || $packString || $herbalString || $accessoriesString) {?>
	        <p class="composition-title">Состав букета</p>
	        <div class="composition-txt">
	        	<?if ($flowersString) {?>
		            <p class="composition-txt-flowers">Цветы: </p>
		            <?=$flowersString?>
	            <?}
	            if ($packString) {?>
	            	<p class="composition-txt-pack">Упаковка: </p>
	            	<?=$packString?>
	            <?}
	            if ($herbalString) {?>
		            <p class="composition-txt-herb">Зелень: </p>
		            <?=$herbalString?>
		        <?}
		        if ($accessoriesString) {?>
		            <p class="composition-txt-access">Аксессуары: </p>
		            <?=$accessoriesString?>
		        <?}?>
	        </div>
	        <?}?>
	        <div class="composition-price">
	            <p>Стоимость Вашего букета: <span id="allSum_FORMATED"><?=number_format($totalSum, 0, '.', ' ')?> руб.</span> </p>
	            <a href="javascript: void(0)" id="collectFutureBouquet">ЗАКАЗАТЬ БУКЕТ</a>
	        </div>
	    </div>
	</div>
</div>
		<?
		break;
	case 'add2Basket':
		$id = intval($_REQUEST['id']);
		$quantity = intval($_REQUEST['quantity']);
		$arElement = CIBlockElement::GetByID($id)->Fetch();
		if ($arElement['IBLOCK_SECTION_ID'] == 3) {
			$arSets = CCatalogProductSet::getAllSetsByProduct($id, CCatalogProductSet::TYPE_SET);
			if ($arSets) {
				foreach ($arSets as $arSet) {
					foreach ($arSet['ITEMS'] as $arItem) {
						/*$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();
						$parentCategory = CIBlockSection::GetNavChain(false, $arElement['IBLOCK_SECTION_ID'])->Fetch();
						if ($parentCategory['ID'] == 2) {
							$arResult['ITEM']['SET'][] = "<span>{$arElement['NAME']}</span> - <span>{$arItem['QUANTITY']} шт.</span>";
						}*/
						$_SESSION['futureBouquet'][$arItem['ITEM_ID']] = $quantity * $arItem['QUANTITY'];
					}
				}
			}
		} else {
			$_SESSION['futureBouquet'][$id] = $quantity;
		}

		$arResult['STATUS'] = 'OK';
		$arResult['session'] = $_SESSION;

		break;
	case 'collectFutureBouquet':
		if (!empty($_SESSION['futureBouquet'])) {
			$arFields = [
				'IBLOCK_ID' => IB_CATALOG,
				'IBLOCK_SECTION_ID' => 22,
				'ACTIVE' => 'Y',
				'NAME' => 'Цветочный набор',
				'CODE' => generateRandomString()
			];

			$obElement = new CIBlockElement;
			$complectId = $obElement->Add($arFields);

			if (!$complectId) {
				$arResult['STATUS'] = 'FAIL';
				$arResult['MESSAGE'] = 'Произошла ошибка: ' . $obElement->LAST_ERROR;
				break;
			}

			$arFields = [
				'ID' => $complectId,
			];
			if (!CCatalogProduct::Add($arFields)) {
				$arResult['STATUS'] = 'FAIL';
				$arResult['MESSAGE'] = 'Произошла ошибка: не добавлены параметры товару';
				break;
			}

			$arFields = [
				'TYPE' => CCatalogProductSet::TYPE_SET,
			    'ITEM_ID' => $complectId,
			    'ACTIVE' => 'Y',
			];
			$totalPrice = 0;
			foreach ($_SESSION['futureBouquet'] as $itemId => $quantity) {
				$arItem = CIBlockElement::GetByID($itemId)->Fetch();
				$arProduct = CCatalogProductProvider::GetProductData(
					[
						'PRODUCT_ID' => $itemId,
						'QUANTITY' => $quantity,
					]
				);

				$totalPrice += $arProduct['PRICE'] * $quantity;
				$arFields['ITEMS'][] = [
					'ACTIVE' => 'Y',
		            'ITEM_ID' => $itemId,
		            'QUANTITY' => $arProduct['QUANTITY'],
		            'DISCOUNT_PERCENT' => $arProduct['DISCOUNT_VALUE']
		        ];
			}

			$arFieldsPrice = [
				'PRODUCT_ID' => $complectId,
				'CATALOG_GROUP_ID' => 1,
				'PRICE' => $totalPrice,
				'CURRENCY' => 'RUB'
			];

			$obPrice = new CPrice();
            $result = $obPrice->Add($arFieldsPrice);

            if (!$result) {
	            $arResult['STATUS'] = 'FAIL';
	            $arResult['MESSAGE'] = 'Произошла ошибка: ' . $APPLICATION->GetException();
	            break;
			}

		    $setId = CCatalogProductSet::add($arFields);
			//CCatalogProductSet::recalculateSetsByProduct($complectId);

		    if ($setId) {
		    	$resultId = Add2BasketByProductID($complectId, 1);
		    	if ($ex = $APPLICATION->GetException())
					debugfile($ex->GetString());
				$arResult['STATUS'] = 'OK';
				$arResult['MESSAGE'] = 'Цветочный набор';
				unset($_SESSION['futureBouquet']);
		    } else {
				$arResult['STATUS'] = 'FAIL';
				$arResult['MESSAGE'] = 'Букет не собран';
		    }
		} else {
			$arResult['STATUS'] = 'FAIL';
			$arResult['MESSAGE'] = 'Ничего не добавлено';
		}

		break;
	case 'deleteFromBasket':
		$id = intval($_REQUEST['id']);
		unset($_SESSION['futureBouquet'][$id]);
		$arResult['STATUS'] = 'OK';
		$arResult['MESSAGE'] = 'Товар удален';
		break;
	case 'setQuantity':
		$id = intval($_REQUEST['id']);
		$quantity = intval($_REQUEST['quantity']);
		$_SESSION['futureBouquet'][$id] = $quantity;
		$_SESSION['futureBouquetActive'] = $id;
		$arResult['STATUS'] = 'OK';
		$arResult['MESSAGE'] = 'Колиество изменено';
		break;
    case 'getBasket':
    ?>
            <div class="mob-basket" id="mobileBasket">
				<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mobileBasket", Array(
					"HIDE_ON_BASKET_PAGES" => "N",	// Не показывать на страницах корзины и оформления заказа
						"PATH_TO_AUTHORIZE" => "",	// Страница авторизации
						"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
						"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
						"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
						"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
						"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
						"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
						"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
						"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
						"SHOW_PERSONAL_LINK" => "Y",	// Отображать персональный раздел
						"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
						"SHOW_REGISTRATION" => "N",	// Добавить возможность регистрации
						"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
					),
					false
				);?>
            </div>
			<li id="desktopBasket">
				<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "topBasket", Array(
					"HIDE_ON_BASKET_PAGES" => "N",	// Не показывать на страницах корзины и оформления заказа
						"PATH_TO_AUTHORIZE" => "",	// Страница авторизации
						"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
						"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
						"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
						"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
						"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
						"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
						"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
						"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
						"SHOW_PERSONAL_LINK" => "Y",	// Отображать персональный раздел
						"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
						"SHOW_REGISTRATION" => "N",	// Добавить возможность регистрации
						"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
					),
					false
				);?>
			</li>
			<?
		break;
	default:
			$arResult['STATUS'] = 'FAIL';
			$arResult['MESSAGE'] = "Неверное действие";
}

echo json_encode($arResult);
die();
?>