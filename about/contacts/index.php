<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты и адреса");
?>
    <section class="section about">
        <div class="container">
            <h2 class="section-title">
                <span>Контакты и адреса</span>
            </h2>
            <p class="section-desc">Работаем для Вас</p>
                <ul class="ourNavAbout">
                    <li><a href="/about/">О КОМПАНИИ</a></li>
                    <li><a href="/about/delivery/">Оплата И ДОСТАВКА</a></li>
                    <li class="active"><a href="/about/contacts/">Контакты и адреса</a></li>
                </ul>
            <div class="address">
                <div class="address-content">
                    <p>Адреса магазинов</p>
                    <div class="address-town">
                        <div class="address-town-name">г. Сургут</div>
                        <p><a href="https://www.google.com.ua/maps?q=%D0%B3.+%D0%A1%D1%83%D1%80%D0%B3%D1%83%D1%82+%D1%83%D0%BB.+%D0%9F%D1%80%D0%BE%D1%84%D1%81%D0%BE%D1%8E%D0%B7%D0%BE%D0%B2+31/2&um=1&ie=UTF-8&sa=X&ved=0ahUKEwj8iLTm9NvWAhWhF5oKHdwCCfEQ_AUICigB">ул. Профсоюзов 31/2 </a><span>............</span><a href="tel:+834627490890">8 (3462) 490-890 </a>(самовывоз)</p>
                        <p><a href="https://www.google.com.ua/maps/place/%D0%BF%D1%80.+%D0%9B%D0%B5%D0%BD%D0%B8%D0%BD%D0%B0,+62,+%D0%A1%D1%83%D1%80%D0%B3%D1%83%D1%82,+%D0%A5%D0%B0%D0%BD%D1%82%D1%8B-%D0%9C%D0%B0%D0%BD%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9+%D0%B0%D0%B2%D1%82%D0%BE%D0%BD%D0%BE%D0%BC%D0%BD%D1%8B%D0%B9+%D0%BE%D0%BA%D1%80%D1%83%D0%B3,+%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F,+628415/@61.2632226,73.3737713,17z/data=!3m1!4b1!4m5!3m4!1s0x43739748f1f2b2d3:0xf6f15d42e2326411!8m2!3d61.26322!4d73.37596">Пр-т Ленина 62 </a><span>..................................</span><a href="tel:+83462470570">8 (3462) 470-570</a></p>
                        <p><a href="https://www.google.com.ua/maps/place/%D0%A1%D0%B5%D0%BC%D1%8C+%D1%86%D0%B2%D0%B5%D1%82%D0%BE%D0%B2/@61.2467651,73.4058291,17z/data=!4m13!1m7!3m6!1s0x4373975f80a103c9:0x228b6e3d5f80e0fd!2z0L_RgC4g0JvQtdC90LjQvdCwLCAxNi8xLCDQodGD0YDQs9GD0YIsINCl0LDQvdGC0Yst0JzQsNC90YHQuNC50YHQutC40Lkg0LDQstGC0L7QvdC-0LzQvdGL0Lkg0L7QutGA0YPQsywg0KDQvtGB0YHQuNGPLCA2Mjg0MDM!3b1!8m2!3d61.246858!4d73.4076369!3m4!1s0x0:0xbe2929482c07bdf5!8m2!3d61.2466091!4d73.4065837">Пр-т Ленина 16 </a><span>..................................</span><a href="tel:+83462660550">8 (3462) 660-550</a></p>
                        <p><a href="#">Ул. Комсомольская 15 </a><span>..........................</span><a href="tel:+83462490990">8 (3462) 490-990</a></p>
                    </div>
                    <div class="address-town">
                        <div class="address-town-name">г. Нефтеюганск</div>
                        <p><a href="https://www.google.com.ua/maps/place/16-%D0%B9+%D0%BC%D0%B8%D0%BA%D1%80%D0%BE%D1%80%D0%B0%D0%B9%D0%BE%D0%BD,+86,+%D0%9D%D0%B5%D1%84%D1%82%D0%B5%D1%8E%D0%B3%D0%B0%D0%BD%D1%81%D0%BA,+%D0%A5%D0%B0%D0%BD%D1%82%D1%8B-%D0%9C%D0%B0%D0%BD%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9+%D0%B0%D0%B2%D1%82%D0%BE%D0%BD%D0%BE%D0%BC%D0%BD%D1%8B%D0%B9+%D0%BE%D0%BA%D1%80%D1%83%D0%B3,+%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F,+628310/@61.0808293,72.6149903,18.75z/data=!4m5!3m4!1s0x437464ce8145ab9f:0xa9095d495ac27c91!8m2!3d61.0809964!4d72.6152413">Мкр. 16А, д. 86 </a><span>...................</span><a href="tel:+83463512512">8 (3463) 512-512 </a>(самовывоз)</p>
                    </div>
                </div>
            </div>
            <div class="address-bot">
                <p>ООО "ЦВЕТОЧНАЯ КОМПАНИЯ"</p>
                <p>ИНН 8602265908</p>
                <p>ОГРН 1168617054418</p>
            </div>
            <div class="address-subscribe">
				<?
				$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH . '/include/subscribeForm.php',
					[],
					['mode' => 'html']);
				?>
            </div>
        </div>
    </section>
    <div class="map">
        <iframe src="https://snazzymaps.com/embed/18253" style="border:none;"></iframe>
        <div class="map-info">
            <p>Наши контакты</p>
            <ul class="map-list">
                <li><a href="tel:+73462490890">7 (3462) 490-890</a></li>
                <li><a href="mailto:zakaz@semcvetov.ru">zakaz@semcvetov.ru</a></li>
                <li><a href="https://www.google.com.ua/maps/place/%D1%83%D0%BB.+%D0%9F%D1%80%D0%BE%D1%84%D1%81%D0%BE%D1%8E%D0%B7%D0%BE%D0%B2,+31%2F2,+%D0%A1%D1%83%D1%80%D0%B3%D1%83%D1%82,+%D0%A5%D0%B0%D0%BD%D1%82%D1%8B-%D0%9C%D0%B0%D0%BD%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9+%D0%B0%D0%B2%D1%82%D0%BE%D0%BD%D0%BE%D0%BC%D0%BD%D1%8B%D0%B9+%D0%BE%D0%BA%D1%80%D1%83%D0%B3,+%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F,+628418/@61.2671626,73.4073193,17.25z/data=!4m5!3m4!1s0x4373976b8898c745:0x2de46eacb5ecfaf9!8m2!3d61.267176!4d73.408389" class="footer-right-address">г. Сургут ул. Профсоюзов 31/2</a></li>
                <li>8:00 - 21:00</li>
            </ul>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>