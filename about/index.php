<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");
$APPLICATION->AddChainItem('О компании', '/about/');
?>    <section class="section about">
        <div class="container">
            <h2 class="section-title">
                <span>О нас</span>
            </h2>
            <p class="section-desc">Работаем для Вас</p>
                <ul class="ourNavAbout">
                    <li class="active"><a href="/about/">О КОМПАНИИ</a></li>
                    <li><a href="/about/delivery/">Оплата И ДОСТАВКА</a></li>
                    <li><a href="/about/contacts/">Контакты и адреса</a></li>
                </ul>
            <div class="company clearfix">
                <div class="company-photo" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/info-company.jpg');"></div>
                <div class="company-txt">
                    <p>«Семь цветов»</p>
                    <span>- это молодая динамичная компания, которая находится на рынке более пяти лет. За эти пять лет мы стали опытнее, интереснее, современнее и каждый день мы стараемся совершенствоваться для Вас. В наших магазинах Вы найдете не только свежий цветок по приятной цене, но и получите качественное обслуживание от мастеров своего дела.
Наши букеты - это особая любовь к цветам и частичка нашей души, мы всегда подскажем, проконсультируем и сотворим для Вас незабываемую красоту.</span>
                </div>
            </div>
			<?
			$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH . '/include/features.php',
				[],
				['mode' => 'html']);
			?>
            <div class="certificates">
                <p class="certificates-title">Сертификаты</p>
				<?$APPLICATION->IncludeComponent("bitrix:news.list", "certificates", Array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
						"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
						"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
						"DISPLAY_DATE" => "N",	// Выводить дату элемента
						"DISPLAY_NAME" => "N",	// Выводить название элемента
						"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
						"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"FIELD_CODE" => array(	// Поля
							0 => "",
							1 => "",
						),
						"FILTER_NAME" => "",	// Фильтр
						"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
						"IBLOCK_ID" => "6",	// Код информационного блока
						"IBLOCK_TYPE" => "-",	// Тип информационного блока (используется только для проверки)
						"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
						"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"NEWS_COUNT" => "100",	// Количество новостей на странице
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Новости",	// Название категорий
						"PARENT_SECTION" => "",	// ID раздела
						"PARENT_SECTION_CODE" => "",	// Код раздела
						"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
						"PROPERTY_CODE" => array(	// Свойства
							0 => "",
							1 => "",
						),
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
						"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
						"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
						"SORT_ORDER2" => "DESC",	// Направление для второй сортировки новостей
						"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
					),
					false
				);?>
            </div>
			<?
			$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH . '/include/ourFloristsSays.php',
				[],
				['mode' => 'html']);
			?>
        </div>
		<?
		$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH . '/include/floristGallery.php',
			[],
			['mode' => 'html']);
		?>
    </section>
	<?
	$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH . '/include/subscribeForm.php',
		[],
		['mode' => 'html']);
	?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>