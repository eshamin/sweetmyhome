<?
CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');
CModule::IncludeModule('subscribe');

require_once('include/debug.php');
require_once('include/constants.php');
require_once('include/functions.php');
require_once('include/events.php');

global $arrFilter, $searchFilter;
$arrFilter['!PROPERTY_DESIGN_VALUE'] = 'Y';
$searchFilter['!PROPERTY_DESIGN_VALUE'] = 'Y';
?>