<?
AddEventHandler("sale", "OnOrderUpdate", "OnOrderUpdateHandler");

function OnOrderUpdateHandler($id, $arFields)
{
	if ($arFields['STATUS_ID'] == 'F') {
		$userId = $arFields['USER_ID'];
		$totalAmount = 0;
		$needUpdate = true;
		$resTransact = CSaleUserTransact::GetList([], ['USER_ID' => $userId]);
		while ($arTransact = $resTransact->Fetch()) {
			if ($arTransact['ORDER_ID'] == $id) {
				$needUpdate = false;
				break;
			}
		}

		if ($needUpdate) {
			$dbItemsInOrder = CSaleBasket::GetList(['ID' => 'ASC'], ['ORDER_ID' => $id]);
			while ($arItem = $dbItemsInOrder->Fetch()) {
				$arSort = [];
				$arFilter = ['ID' => $arItem['PRODUCT_ID']];
				$arProperties = ['PROPERTY_BONUS'];
				$arBonuses = CIBlockElement::GetList($arSort, $arFilter, false, false, $arProperties)->Fetch();
				$totalAmount += $arBonuses['PROPERTY_BONUS_VALUE'];
			}
			CSaleUserAccount::UpdateAccount($userId, $totalAmount, 'RUB', $id, $id);
		}
	}
}


AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");

function BeforeIndexHandler($arFields)
{
	if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == IB_CATALOG) {
		$arSets = CCatalogProductSet::getAllSetsByProduct($arFields["ITEM_ID"], CCatalogProductSet::TYPE_SET);

		foreach ($arSets as $arSet) {
			foreach ($arSet['ITEMS'] as $arItem) {
				$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();
				$arFields["BODY"] .= " {$arElement['NAME']}";
			}
		}
	} else {
		$arFields["BODY"] = '';
		$arFields["TITLE"] = '';
	}
	return $arFields;
}

?>