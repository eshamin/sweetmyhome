<?
function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function plural_form($number, $after)
{
	$cases = array (2, 0, 1, 1, 1, 2);
	echo $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
}

function getProfileInfo($id)
{
	$arUser = CUser::GetByID($id)->Fetch();
	$arUser['PHOTO'] = CFile::GetPath($arUser['PERSONAL_PHOTO']);
	?>
    <div class="private-user-img" style="background-image: url('<?=$arUser['PHOTO']?>');"></div>
    <p class="private-user-name"><?=$arUser['NAME']?></p>
    <p class="private-user-balance">Баланс: <span><?=number_format(CSaleUserAccount::GetByUserID($id, 'RUB')['CURRENT_BUDGET'], 0, '.', ' ')?></span> баллов</p>
    <a href="?logout=yes"></a>
<?}?>