<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
    <footer class="footer">
        <div class="container">
            <div class="footer-content">
                <div class="footer-left">
                    <a href="/" class="footer-logo">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="logo">
                    </a>
                    <p>© <?=date('Y')?> “7 Цветов”.<br>
                        All rigths reserved 2016.</p>
                    <a href="#policy" class="policyLink">Политика конфиденциальности</a>
                </div>
                <div class="footer-center">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottomMenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					);?>
                    <div class="Cbot">
                        <p>
						<?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH . '/include/footerSlogan.php',
							[],
							['mode' => 'html']);
						?>
						</p>
                    </div>
                </div>
                <div class="footer-right">
                    <a href="https://www.google.com.ua/maps/place/%D1%83%D0%BB.+%D0%9F%D1%80%D0%BE%D1%84%D1%81%D0%BE%D1%8E%D0%B7%D0%BE%D0%B2,+31%2F2,+%D0%A1%D1%83%D1%80%D0%B3%D1%83%D1%82,+%D0%A5%D0%B0%D0%BD%D1%82%D1%8B-%D0%9C%D0%B0%D0%BD%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%B8%D0%B9+%D0%B0%D0%B2%D1%82%D0%BE%D0%BD%D0%BE%D0%BC%D0%BD%D1%8B%D0%B9+%D0%BE%D0%BA%D1%80%D1%83%D0%B3,+%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F,+628418/@61.2671626,73.4073193,17.25z/data=!4m5!3m4!1s0x4373976b8898c745:0x2de46eacb5ecfaf9!8m2!3d61.267176!4d73.408389" class="footer-right-address">г. Сургут ул. Профсоюзов 31/2</a>
                    <a href="tel:+73462490890" class="footer-right-phone">7 (3462) 490-890</a>
                    <a href="#callback" class="footer-right-callback">Заказать обратный звонок</a>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
                    </ul>
                    <div class="footer-info">
                        <span>Сделано с любовью</span>
                        <span>5 углов</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="Lside ">
        <p>Меню</p>
		<?$APPLICATION->IncludeComponent("bitrix:menu", "mobileMenu", Array(
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"MAX_LEVEL" => "1",	// Уровень вложенности меню
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
				"MENU_CACHE_TYPE" => "A",	// Тип кеширования
				"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
				"ROOT_MENU_TYPE" => "mobile",	// Тип меню для первого уровня
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			),
			false
		);?>
    </div>
	<?
	$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH . '/include/footerForms.php',
		[],
		['mode' => 'html']);
	?>
</div>


  <div class="b_collect_metadata">
    <div class="close_metadata"></div>
    <div class="_metadata_title">
      Мы собираем метаданные
    </div>
    <div class="_metadata_law">
      <a href="#">152-Федеральный Закон</a> обязывает нас уведомить вас о том, что в целях корректного функционирования сайта, мы собираем метаданные, такие как cookie, данные об IP-адресе и местоположении
    </div>
    <div class="_metadata_understanding">
      Если вы не хотите, чтобы эти данные обрабатывались, то должны покинуть сайт.
      <br>
      Спасибо за понимание.
    </div>
  </div>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/common.js"></script>
</body>
</html>