<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
 $totalQuantity = 0;
 foreach ($arResult['CATEGORIES']['READY'] as $arItem) {
	 $totalQuantity += $arItem['QUANTITY'];
 }
 ?>
<a href="<?=$arParams['PATH_TO_BASKET']?>">
	<img src="<?=SITE_TEMPLATE_PATH?>/img/svg/basket.svg" alt="basket">
</a>
<span><?=$totalQuantity?></span>
