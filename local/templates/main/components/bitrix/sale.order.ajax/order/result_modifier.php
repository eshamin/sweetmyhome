<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($_SESSION['payBonuses'] == 'Y') {
	$arResult['BONUSES'] = CSaleUserAccount::GetByUserID($USER->GetID(), 'RUB')['CURRENT_BUDGET'];
	$arResult['BONUSES'] = number_format($arResult['BONUSES'], 0, '.', '');

	if ($arResult['ORDER_DATA']['PRICE'] >= $arResult['BONUSES']) {
		$arResult['ORDER_DATA']['PRICE'] -= $arResult['BONUSES'];
	} else {
		$arResult['ORDER_DATA']['PRICE'] = 0;
	}
	$arResult['ORDER_DATA']['SUM_PAID'] = $arResult['BONUSES'];

	$arResult['ORDER_TOTAL_PRICE_FORMATED'] = number_format($arResult['ORDER_DATA']['PRICE'], 0, '.', ' ') . ' руб.';
}