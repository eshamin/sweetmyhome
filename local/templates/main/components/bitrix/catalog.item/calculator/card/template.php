<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="addItem-top">
	<?if ($haveOffers) {?>
        <div class="rotate">
            <div class="rotate-prev">
                <div class="addItem-img" id="<?=$itemIds['PICT']?>">
                    <img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="flowers-img-1">
                </div>
                <div class="addItem-desc">
                    <h5><?=$item['NAME']?></h5>
                    <p><span><?=$price['RATIO_PRICE']?></span> руб./шт.</p>
                    <div class="addItem-btn"></div>
                </div>
            </div>
            <div class="rotate-next">
            	<div class="buyFlowers">
	                <span class="close-btn"></span>
	                <div class="buyFlowers-item">
	                    <p>Количество пачек</p>
	                    <div class="b-range" data-current="<?=$measureRatio?>" data-max="<?=$actualItem['CATALOG_QUANTITY']?>"></div>
	                    <div class="number-range">
							<input class="range-value-n" id="<?=$itemIds['QUANTITY']?>" type="number"
								name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>"
								value="<?=$measureRatio?>">
	                        <span><?=$actualItem['CATALOG_QUANTITY']?></span>
	                    </div>
	                </div>

	                <div class="buyFlowers-item" id="<?=$itemIds['PROP_DIV']?>">
	                    <p>Количество штук в пачке</p>
							<?foreach ($arParams['SKU_PROPS'] as $skuProperty) {
								$propertyId = $skuProperty['ID'];
								$skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
								if (!isset($item['SKU_TREE_VALUES'][$propertyId])) {
									continue;
								}

								$name = generateRandomString(5);
								$first = true;
								foreach ($skuProperty['VALUES'] as $value) {
									if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
										continue;

										$checked = '';
									$value['NAME'] = htmlspecialcharsbx($value['NAME']);
									if ($first) {
										$checked = 'checked';
										$firstValue = $value['NAME'];
										$first = false;
									}
									?>

									<label data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
										<input type="radio" <?=$checked?> name="<?=$name?>" title="<?=$value['NAME']?>"><span><?=$value['NAME']?></span>
									</label>
									<?

								}?>
	                        <?}?>
	                </div>
	                <div class="buyFlowers-item">
	                    <p>Итого:</p>
	                    <div class="buyFlowers-total">
							<p><span class="flowers"><?=$firstValue?></span> штук - <span class="packPrice" id="<?=$itemIds['PRICE']?>" data-price="<?=$price['RATIO_PRICE']?>"> <?=$price['RATIO_PRICE']?> руб.</span></p>
	                        <p><span class="packs">1</span> <span class="pluralForm"><?=plural_form($measureRatio, ['пачка', 'пачки', 'пачек'])?></span> - <span class="buyFlowers-total-b" id="totalPrice_<?=$item['ID']?>"><?=$price['RATIO_PRICE']?> руб.</span></p>
	                    </div>
	                </div>
	            </div>
            </div>
        </div>
<?
	foreach ($arParams['SKU_PROPS'] as $skuProperty) {
		if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']])) {
			continue;
		}

		$skuProps[] = [
			'ID' => $skuProperty['ID'],
			'SHOW_MODE' => $skuProperty['SHOW_MODE'],
			'VALUES' => $skuProperty['VALUES'],
			'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
		];
	}

	unset($skuProperty, $value);
?>
	<?} else {?>
		<div class="addItem-img" id="<?=$itemIds['PICT']?>">
		    <img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="">
		</div>
		<div class="addItem-desc">
		    <h5><?=$item['NAME']?></h5>
		    <p><span><?=$price['RATIO_PRICE']?></span> руб./шт.</p>
			<div class="spinner-global" data-entity="quantity-block">
			    <p class="spin-down" id="<?=$itemIds['QUANTITY_DOWN']?>">-</p>
			    <input type="text" data-quantityFor="<?=$item['ID']?>" value="<?=$price['MIN_QUANTITY']?>" id="<?=$itemIds['QUANTITY']?>">
			    <p class="spin-up" id="<?=$itemIds['QUANTITY_UP']?>">+</p>
			</div>
		</div>
	<?}?>
</div>
<div id="<?=$itemIds['BASKET_ACTIONS']?>">
	<a href="javascript:void(0);" data-productId="<?=$item['ID']?>" class="addFutureBouquet" id="<?=$itemIds['BUY_LINK']?>">добавить в букет</a>
</div>
