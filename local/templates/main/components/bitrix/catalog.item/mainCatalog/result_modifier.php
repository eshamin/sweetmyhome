<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ($arResult['ITEM']) {
	$arResult['ITEM']['SET'] = false;
	$arSets = CCatalogProductSet::getAllSetsByProduct($arResult['ITEM']['ID'], CCatalogProductSet::TYPE_SET);

	foreach ($arSets as $arSet) {
		foreach ($arSet['ITEMS'] as $arItem) {
			$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();

			if ($arElement['IBLOCK_SECTION_ID'] == 9) {
				$arResult['ITEM']['SET'][] = "<span>{$arElement['NAME']}</span> - <span>{$arItem['QUANTITY']} шт.</span>";
			}
		}
	}
}
?>