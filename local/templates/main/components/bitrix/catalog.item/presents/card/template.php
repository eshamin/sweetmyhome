<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="ourHover">
    <ul class="ourSelect">
        <li>
            <a href="<?=$item['DETAIL_PAGE_URL']?>">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/svg/fl-search.svg" alt="fl-search">
            </a>
        </li>
        <li id="<?=$itemIds['BASKET_ACTIONS']?>">
            <a href="javascript:void(0)" rel="nofollow" class="addBasket" data-productName="<?=$item['NAME']?>" id="<?=$itemIds['BUY_LINK']?>">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/svg/fl-basket.svg" alt="fl-basket">
            </a>
        </li>
        <li>
            <a href="#buy"> <span>в 1 клик</span></a>
        </li>
    </ul>
</div>
<div class="ourContent-img" id="<?=$itemIds['PICT']?>">
    <img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="accessories-1">
	<?if ($item['PROPERTIES']['NOVELTY']['VALUE'] == 'Да') {?><div class="ourLabel">новинка</div><?}?>
	<?if ($item['PROPERTIES']['HIT']['VALUE'] == 'Да') {?><div class="ourLabel">популярное</div><?}?>
	<?if ($price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE']) {?><div class="ourLabel">скидка</div><?}?>
</div>
<div class="ourDesc">
    <h5><?=$item['NAME']?></h5>
    <p></p>
</div>
<?
if ($arParams['SHOW_OLD_PRICE'] === 'Y')
{
?>
<p class="ourPrice <?if ($price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE']) {?>newPrice<?}?>">
    <?if ($price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE']) {?><span id="<?=$itemIds['OLD_PRICE_ID']?>"><?=$price['PRINT_RATIO_BASE_PRICE']?></span><?}?>
    <?=$price['PRINT_RATIO_PRICE']?>
</p>
<?} else {?>
<p class="ourPrice">
    <?=$price['PRINT_RATIO_PRICE']?>
</p>
<?}?>