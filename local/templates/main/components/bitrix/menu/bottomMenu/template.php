<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$totalCount = 0;
?>
<div class="Ctop">
<?if (!empty($arResult)) {?>
	<ul class="Fmenu">
	<?
	foreach($arResult as $arItem) {
		if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;

		if ($totalCount == 3) {?>
            </ul>
            <ul class="Fmenu">
		<?
			$totalCount = 0;
		}
		$totalCount++;
	?>
		<?if($arItem["SELECTED"]) {?>
			<li><a href="<?=$arItem["LINK"]?>" class="active"><?=$arItem["TEXT"]?></a></li>
		<?} else {?>
			<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
		<?}?>

	<?}?>
	</ul>
<?}?>
</div>