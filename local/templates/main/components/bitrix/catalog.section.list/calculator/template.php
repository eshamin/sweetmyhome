<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>


<div class="constructor-block">
    <div class="constructor-toggle">
        <div class="burger__constructor">
            <span class="burger__line_constructor"></span>
            <span class="burger__line_constructor"></span>
            <span class="burger__line_constructor"></span>
            <span class="burger__line_constructor"></span>
        </div>
        <p>Меню</p>
    </div>
    <div class="ourFlowers tabs">
	    <ul class="constructorNav">
	        <li class="tabs__tab active">
	            <a href="#all" class="n-all">все цветы</a>
	        </li>
	        <?foreach ($arResult['SECTIONS'] as &$arSection) {?>
	        <li class="tabs__tab">
	            <a href="#<?=$arSection['UF_CSS_COLOR_CLASS']?>" class="n-<?=$arSection['UF_CSS_COLOR_CLASS']?>"><?=$arSection['NAME']?></a>
	        </li>
	        <?}?>
	        <li class="tabs__tab">
	            <a href="#herb" class="n-herb">зелень</a>
	        </li>
	        <li class="tabs__tab">
	            <a href="#pack" class="n-pack">упаковка</a>
	        </li>
	        <li class="tabs__tab">
	            <a href="#accessories" class="n-access">аксессуары</a>
	        </li>
	    </ul>

		<div class="tabs_content">
		<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "calculator",
			Array(
				"ACTIVE" => "Y",
				"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
				"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
				"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
				"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
				"CUSTOM_FILTER" => "",
				"DETAIL_URL" => "/flowers/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
				"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
				"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
				"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
				"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
				"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
				"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
				"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
				"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
				"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"LABEL_PROP" => array(	// Свойства меток товара
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
				"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
				"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
				"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
				"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
				"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"OFFERS_FIELD_CODE" => array(	// Поля предложений
					0 => "",
					1 => "",
				),
				"OFFERS_PROPERTY_CODE" => array(	// Поля предложений
					0 => "COUNT",
					1 => "",
				),
				"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
				"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
				"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
				"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
				"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "mainBouquets",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGE_ELEMENT_COUNT" => "400",	// Количество элементов на странице
				"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
				"PRODUCT_DISPLAY_MODE" => "Y",	// Схема отображения
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
				"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
				"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
				"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
				"RCM_TYPE" => "personal",	// Тип рекомендации
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
				"SECTION_ID" => $arParams['SECTION_ID'],	// ID раздела
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"SECTION_URL" => "/bouquets/",	// URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => array(	// Свойства раздела
					0 => "",
					1 => "",
				),
				"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
				"SEF_RULE" => "",	// Правило для обработки
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
				"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
				"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
				"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
				"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
				"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
				"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
				"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
				"TEMPLATE_THEME" => "blue",	// Цветовая тема
				"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
				"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
			),
			false
		);?>
		<?foreach ($arResult['SECTIONS'] as $arrSection) {
			$APPLICATION->IncludeComponent("bitrix:catalog.section", "calculator", Array(
				"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
					"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
					"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
					"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
					"AJAX_MODE" => "N",	// Включить режим AJAX
					"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
					"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
					"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
					"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
					"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
					"CACHE_GROUPS" => "Y",	// Учитывать права доступа
					"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
					"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
					"CUSTOM_FILTER" => "",
					"DETAIL_URL" => "/flowers/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
					"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
					"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
					"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
					"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
					"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
					"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
					"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
					"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
					"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
					"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
					"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
					"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
					"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
					"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
					"LABEL_PROP" => array(	// Свойства меток товара
						0 => "NOVELTY",
						1 => "HIT",
					),
					"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
						0 => "NOVELTY",
						1 => "HIT",
					),
					"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
					"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
					"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
					"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
					"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
					"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
					"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
					"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
					"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
					"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
					"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
					"OFFERS_FIELD_CODE" => array(	// Поля предложений
						0 => "",
						1 => "",
					),
					"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
					"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
					"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
					"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
					"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
					"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
					"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
					"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
					"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
					"PAGER_TEMPLATE" => "mainBouquets",	// Шаблон постраничной навигации
					"PAGER_TITLE" => "Товары",	// Название категорий
					"PAGE_ELEMENT_COUNT" => "400",	// Количество элементов на странице
					"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
					"PRICE_CODE" => array(	// Тип цены
						0 => "BASE",
					),
					"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
					"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
					"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
					"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
					"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
					"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
					"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
					"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
					"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
					"RCM_TYPE" => "personal",	// Тип рекомендации
					"SECTION_CODE" => "",	// Код раздела
					"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
					"SECTION_ID" => $arrSection['ID'],	// ID раздела
					"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
					"SECTION_URL" => "/bouquets/",	// URL, ведущий на страницу с содержимым раздела
					"SECTION_USER_FIELDS" => array(	// Свойства раздела
						0 => "",
						1 => "",
					),
					"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
					"SEF_RULE" => "",	// Правило для обработки
					"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
					"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
					"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
					"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
					"SET_STATUS_404" => "N",	// Устанавливать статус 404
					"SET_TITLE" => "N",	// Устанавливать заголовок страницы
					"SHOW_404" => "N",	// Показ специальной страницы
					"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
					"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
					"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
					"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
					"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
					"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
					"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
					"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
					"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
					"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
					"TEMPLATE_THEME" => "blue",	// Цветовая тема
					"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
					"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
					"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
					"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
				),
				false
			);
		}?>
		<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "calculator", Array(
			"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
				"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
				"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
				"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
				"CUSTOM_FILTER" => "",
				"DETAIL_URL" => "/flowers/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
				"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
				"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
				"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
				"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
				"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
				"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
				"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
				"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
				"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"LABEL_PROP" => array(	// Свойства меток товара
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
				"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
				"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
				"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
				"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
				"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"OFFERS_FIELD_CODE" => array(	// Поля предложений
					0 => "",
					1 => "",
				),
				"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
				"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
				"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
				"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
				"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "mainBouquets",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGE_ELEMENT_COUNT" => "400",	// Количество элементов на странице
				"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
				"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
				"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
				"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
				"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
				"RCM_TYPE" => "personal",	// Тип рекомендации
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
				"SECTION_ID" => "7",	// ID раздела
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"SECTION_URL" => "/bouquets/",	// URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => array(	// Свойства раздела
					0 => "",
					1 => "",
				),
				"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
				"SEF_RULE" => "",	// Правило для обработки
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
				"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
				"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
				"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
				"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
				"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
				"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
				"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
				"TEMPLATE_THEME" => "blue",	// Цветовая тема
				"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
				"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
			),
			false
		);?>
		<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "calculator", Array(
			"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
				"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
				"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
				"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
				"CUSTOM_FILTER" => "",
				"DETAIL_URL" => "/flowers/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
				"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
				"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
				"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
				"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
				"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
				"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
				"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
				"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
				"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"LABEL_PROP" => array(	// Свойства меток товара
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
				"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
				"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
				"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
				"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
				"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"OFFERS_FIELD_CODE" => array(	// Поля предложений
					0 => "",
					1 => "",
				),
				"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
				"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
				"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
				"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
				"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "mainBouquets",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGE_ELEMENT_COUNT" => "400",	// Количество элементов на странице
				"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
				"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
				"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
				"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
				"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
				"RCM_TYPE" => "personal",	// Тип рекомендации
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
				"SECTION_ID" => "6",	// ID раздела
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"SECTION_URL" => "/bouquets/",	// URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => array(	// Свойства раздела
					0 => "",
					1 => "",
				),
				"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
				"SEF_RULE" => "",	// Правило для обработки
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
				"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
				"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
				"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
				"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
				"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
				"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
				"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
				"TEMPLATE_THEME" => "blue",	// Цветовая тема
				"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
				"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
			),
			false
		);?>
		<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "calculator", Array(
			"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
				"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
				"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
				"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
				"AJAX_MODE" => "N",	// Включить режим AJAX
				"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
				"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
				"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
				"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
				"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
				"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
				"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
				"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
				"CACHE_GROUPS" => "Y",	// Учитывать права доступа
				"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
				"CACHE_TYPE" => "A",	// Тип кеширования
				"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
				"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
				"CUSTOM_FILTER" => "",
				"DETAIL_URL" => "/accessories/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
				"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
				"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
				"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
				"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
				"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
				"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
				"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
				"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
				"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
				"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
				"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
				"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
				"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
				"LABEL_PROP" => array(	// Свойства меток товара
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
					0 => "NOVELTY",
					1 => "HIT",
				),
				"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
				"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
				"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
				"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
				"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
				"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
				"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
				"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
				"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
				"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
				"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
				"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
				"OFFERS_FIELD_CODE" => array(	// Поля предложений
					0 => "",
					1 => "",
				),
				"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
				"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
				"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
				"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
				"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
				"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
				"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
				"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
				"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
				"PAGER_TEMPLATE" => "mainBouquets",	// Шаблон постраничной навигации
				"PAGER_TITLE" => "Товары",	// Название категорий
				"PAGE_ELEMENT_COUNT" => "400",	// Количество элементов на странице
				"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
				"PRICE_CODE" => array(	// Тип цены
					0 => "BASE",
				),
				"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
				"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
				"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
				"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
				"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
				"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
				"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
				"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
				"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
				"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
				"RCM_TYPE" => "personal",	// Тип рекомендации
				"SECTION_CODE" => "",	// Код раздела
				"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
				"SECTION_ID" => "5",	// ID раздела
				"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
				"SECTION_URL" => "/accessories/",	// URL, ведущий на страницу с содержимым раздела
				"SECTION_USER_FIELDS" => array(	// Свойства раздела
					0 => "",
					1 => "",
				),
				"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
				"SEF_RULE" => "",	// Правило для обработки
				"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
				"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
				"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
				"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
				"SET_STATUS_404" => "N",	// Устанавливать статус 404
				"SET_TITLE" => "N",	// Устанавливать заголовок страницы
				"SHOW_404" => "N",	// Показ специальной страницы
				"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
				"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
				"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
				"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
				"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
				"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
				"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
				"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
				"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
				"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
				"TEMPLATE_THEME" => "blue",	// Цветовая тема
				"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
				"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
				"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
				"USE_PRODUCT_QUANTITY" => "Y",	// Разрешить указание количества товара
			),
			false
		);?>
		</div>
	</div>
</div>
