<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0) {
?>

<div class="futureBlock" id="basket_items">
    <div class="futureContent">

<!--        <div class="futureContent-item select f-flowers">
            <div class="futureTemplate">
                <p>Цветы</p>
                <a href="#">+</a>
                <img src="img/flowers/rose-1.jpg" alt="rose-1">
                <span class="close-btn"></span>
                <span class="future-edit"></span>
            </div>
            <div class="addItem-desc">
                <h5>Роза Pink</h5>
                <span class="addItem-price">36 000 руб.</span>
                <span class="addItem-number">20 пачек</span>
            </div>
        </div>


        <div class="futureContent-item select f-flowers">
            <div class="futureTemplate">
                <p>Цветы</p>
                <a href="#">+</a>
                <img src="img/flowers/rose-1.jpg" alt="rose-1">
                <span class="close-btn"></span>
                <span class="future-edit"></span>
            </div>
            <div class="addItem-desc">
                <h5>Роза Pink</h5>
                <span class="addItem-price">36 000 руб.</span>
                <span class="addItem-number">20 пачек</span>
            </div>
        </div>


        <div class="futureContent-item select f-herb">
            <div class="futureTemplate">
                <p>Зелень</p>
                <a href="#">+</a>
                <img src="img/herbage/img1.jpg" alt="herbage1">
                <span class="close-btn"></span>
            </div>
            <div class="addItem-desc">
                <h5>Рускус</h5>
                <div class="spinner-global" >
                    <p class="spin-down">-</p>
                    <input type="text" value="0">
                    <p class="spin-up">+</p>
                </div>
            </div>
        </div>


        <div class="futureContent-item select f-pack">
            <div class="futureTemplate f-pack">
                <p>Упаковка</p>
                <a href="#">+</a>
                <img src="img/packaging/img1.jpg" alt="packaging1">
                <span class="close-btn"></span>
            </div>
            <div class="addItem-desc">
                <h5>Крафтовая бумага</h5>
                <div class="spinner-global" >
                    <p class="spin-down">-</p>
                    <input type="text" value="0">
                    <p class="spin-up">+</p>
                </div>
            </div>
        </div>


        <div class="futureContent-item select f-access">
            <div class="futureTemplate f-access">
                <p>Аксессуары</p>
                <a href="#">+</a>
                <img src="img/gifts/img1.jpg" alt="gifts1">
                <span class="close-btn"></span>
            </div>
            <div class="addItem-desc">
                <h5>Медведь Love</h5>
                <div class="spinner-global" >
                    <p class="spin-down">-</p>
                    <input type="text" value="0">
                    <p class="spin-up">+</p>
                </div>
            </div>
        </div>
    </div>


    <div class="futureContent-item select f-herb">
        <div class="futureTemplate">
            <p>Зелень</p>
            <a href="#">+</a>
            <img src="img/herbage/img1.jpg" alt="herbage1">
            <span class="close-btn"></span>
        </div>
        <div class="addItem-desc">
            <h5>Рускус</h5>
            <div class="spinner-global" >
                <p class="spin-down">-</p>
                <input type="text" value="0">
                <p class="spin-up">+</p>
            </div>


        </div>
    </div>


    <div class="futureContent-item select f-flowers">
        <div class="futureTemplate">
            <p>Цветы</p>
            <a href="#">+</a>
            <img src="img/flowers/rose-1.jpg" alt="rose-1">
            <span class="close-btn"></span>
            <span class="future-edit"></span>
        </div>
        <div class="addItem-desc">
            <h5>Роза Pink</h5>
            <span class="addItem-price">36 000 руб.</span>
            <span class="addItem-number">20 пачек</span>
        </div>
    </div>


-->

<?foreach ($arResult["GRID"]["ROWS"] as $k => $arItem) {

	if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y") {
	?>

    <?$class = 'flowers';

	if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
		$url = $arItem["PREVIEW_PICTURE_SRC"];
	elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
		$url = $arItem["DETAIL_PICTURE_SRC"];
	else:
		$url = $templateFolder."/images/no_photo.png";
	endif;
    ?>
    <div id="<?=$arItem["ID"]?>" class="futureItem futureContent-item select f-<?=$class?>">
        <div class="futureTemplate">

            <img src="<?=$url?>" alt="herbage1">
            <span class="close-btn"><a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" onclick="return deleteProductRow(this)" class="basket-remove"></a></span>
        </div>
        <div class="addItem-desc">
            <h5><?=$arItem["NAME"]?></h5>
			<div class="spinner-global" >
				<div class="spin-down" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);">-</div>
				<input type="text" id="QUANTITY_INPUT_<?=$arItem["ID"]?>" name="QUANTITY_INPUT_<?=$arItem["ID"]?>" value="<?=$arItem["QUANTITY"]?>" />
				<div class="spin-up" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);">+</div>
			</div>
			<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />


        </div>
    </div>

	<?}
}?>

    <div class="composition">
        <!--<p class="composition-title">Состав букета</p>
        <div class="composition-txt">
            <p class="composition-txt-flowers">Цветы: </p>
            <span> Пион розовый - 1 шт,</span>
            <span> Пион розовый - 2 шт.</span>
            <p class="composition-txt-pack">Упаковка: </p>
            <span>бумага крафтовая - 1шт.</span>
            <p class="composition-txt-herb">Зелень: </p>
            <span>рускус - 1шт.</span>
            <p class="composition-txt-access">Аксессуары: </p>
            <span>Мишка Love - 1шт.</span>
        </div>-->
        <div class="composition-price">
            <p>Стоимость Вашего букета: <span id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span> </p>
            <a href="/personal/cart/">ЗАКАЗАТЬ БУКЕТ</a>
        </div>
    </div>
	</div>
</div>
	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
<?
} else {
?>
<div class="shopping-cart-empty" id="basket_items_list"><?=GetMessage("SALE_NO_ITEMS");?></div>

<?
}
