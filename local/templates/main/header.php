<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-114x114.png">
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/libs.min.js"></script>
    <?$APPLICATION->ShowHead();?>
    <?if (!$USER->IsAuthorized()) {
        CJSCore::Init(array('ajax', 'json', 'ls', 'session', 'popup', 'pull'));
    }?>

	<style>
	.mobile-header {
		position: absolute;
		top: -100%;
		right: 0;
		left: 0;
		background: #fff;
		width: 100%;
		max-width: 980px;
		margin: 0 auto;
		z-index: 10;
		padding-left: 15px;
		padding-right: 15px;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
			-webkit-flex-wrap: wrap;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			-webkit-box-pack: justify;
			-webkit-justify-content: space-between;
			-ms-flex-pack: justify;
			justify-content: space-between;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;
		height: 70px;
	}
	</style>
	<link href="<?=SITE_TEMPLATE_PATH?>/css/libs.min.css" rel="stylesheet" property=stylesheet type="text/css">
	<link href="<?=SITE_TEMPLATE_PATH?>/css/main.css" rel="stylesheet" property=stylesheet type="text/css">
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<div class="wrapper">
    <section class="section-header">
        <div class="mobile-header">
            <div class="burger">
                <span class="burger__line"></span>
                <span class="burger__line"></span>
                <span class="burger__line"></span>
                <span class="burger__line"></span>
            </div>
            <a <?if (!$isMain) {?>href="/"<?}?> class="mobile-logo">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/svg/mobile-logo.svg" alt="mobile-logo">
            </a>
            <div class="mob-basket" id="mobileBasket">
				<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "mobileBasket", Array(
					"HIDE_ON_BASKET_PAGES" => "N",	// Не показывать на страницах корзины и оформления заказа
						"PATH_TO_AUTHORIZE" => "",	// Страница авторизации
						"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
						"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
						"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
						"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
						"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
						"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
						"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
						"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
						"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
						"SHOW_PERSONAL_LINK" => "Y",	// Отображать персональный раздел
						"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
						"SHOW_REGISTRATION" => "N",	// Добавить возможность регистрации
						"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
					),
					false
				);?>
            </div>
        </div>
        <header class="header">
            <div class="header-top">
                <div class="header-left">
                	<?
					$APPLICATION->IncludeFile(
						SITE_TEMPLATE_PATH . '/include/phone.php',
						[],
						['mode' => 'html']);
					?>
					<?
					$APPLICATION->IncludeFile(
						SITE_TEMPLATE_PATH . '/include/mail.php',
						[],
						['mode' => 'html']);
					?>
                </div>
                <a <?if (!$isMain) {?>href="/"<?}?> class="header-logo">
                    <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="logo">
                </a>
                <div class="header-right">
                    <div class="header-sing">
                        <a href="/about/delivery/">доставка и оплата</a>
                        <?if (!$USER->IsAuthorized()) {?>
                        	<a class='login' href="#sing">Вход и регистрация</a>
                        <?} else {?>
                        	<a href="/personal/?back_url=<?=urlencode($APPLICATION->GetCurPageParam());?>"><?=$USER->GetFullName()?></a> <a class="header-logout" href="?logout=yes"></a>
                        <?}?>
                    </div>
                    <div class="header-callback">
						<?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH . '/include/shedule.php',
							[],
							['mode' => 'html']);
						?>
                    </div>
                </div>
            </div>
            <div class="header-bot">
				<nav>
					<ul class="menu">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "topMenu", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "A",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
								"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							),
							false
						);?>
				        <li>
				            <a href="/search/">
				                <img src="<?=SITE_TEMPLATE_PATH?>/img/svg/header-search.svg" alt="header-search">
				            </a>
				        </li>
				        <li id="desktopBasket">
							<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "topBasket", Array(
								"HIDE_ON_BASKET_PAGES" => "N",	// Не показывать на страницах корзины и оформления заказа
									"PATH_TO_AUTHORIZE" => "",	// Страница авторизации
									"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
									"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
									"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
									"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
									"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
									"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
									"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
									"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
									"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
									"SHOW_PERSONAL_LINK" => "Y",	// Отображать персональный раздел
									"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
									"SHOW_REGISTRATION" => "N",	// Добавить возможность регистрации
									"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
								),
								false
							);?>
				        </li>
				    </ul>
				</nav>
            </div>
        </header>
        <?if ($isMain) {?>
        <div class="intro">
			<?$APPLICATION->IncludeComponent(
				"bitrix:news.list",
				"mainSlider",
				array(
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"FILTER_NAME" => "",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"IBLOCK_ID" => IB_SLIDER,
					"IBLOCK_TYPE" => "content",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"INCLUDE_SUBSECTIONS" => "Y",
					"MESSAGE_404" => "",
					"NEWS_COUNT" => "20",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "semcvetov",
					"PAGER_TITLE" => "Баннеры",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"PREVIEW_TRUNCATE_LEN" => "",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "BUTTON",
						2 => "LINK",
						3 => "",
					),
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "N",
					"SET_META_KEYWORDS" => "N",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "N",
					"SHOW_404" => "N",
					"SORT_BY1" => "ACTIVE_FROM",
					"SORT_BY2" => "SORT",
					"SORT_ORDER1" => "DESC",
					"SORT_ORDER2" => "ASC",
					"STRICT_SECTION_CHECK" => "N",
					"COMPONENT_TEMPLATE" => "mainBanners"
				),
				false
			);?>

        </div>
        <?}?>
    </section>

    <?if (!$isMain && !defined('ERROR_404')) {?>
		<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", Array(
			"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
				"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
				"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
			),
			false
		);?>
    <?}?>
