<section class="section section-subscribe">
    <div class="container">
        <h2 class="section-title">
            <span>Подписка</span>
        </h2>
        <p class="section-desc">Подпишитесь на самые лучшие акции и новости</p>
        <form action="/" class="subscribe" id="subscribe" method="post">
            <label for="subscribe-email">
                <input type="email" name="email" placeholder="Ваша почта" class="subscribe-email" required id="subscribe-email">
            </label>
            <input type="hidden" name="action" value="addSubscribe">
            <input type="submit" name="subscribe" value="подписаться" class="subscribe-btn">
        </form>
    </div>
</section>