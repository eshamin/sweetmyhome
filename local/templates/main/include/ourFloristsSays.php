<div class="floristSay">
    <h6>Наши флористы говорят</h6>
    <p>Для меня создание букета - это удивительный мир эмоций и <a href="#">открытий</a>! В процессе подбора цветов я ориентируюсь на
        <a href="#">современные тренды</a> во флористике, и, конечно же отзывы наших дорогих клиентов. Благодаря студии
        <a href="#">“7 цветов”</a> я могу реализовать свой творческий потенциал и дарить клиентам <a
                href="#">радость</a>! </p>
    <div class="floristSay-people">
        <div class="floristSay-people-img" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/img/florist-people-photo.jpg');"></div>
        <p>Анна Иванова</p>
        <span>Главный флорист</span>
    </div>
</div>