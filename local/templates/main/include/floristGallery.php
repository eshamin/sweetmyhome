<div class="floristGallery">
    <a href="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo1.jpg" class="floristGallery-item"  data-fancybox="image"
       data-caption="Здесь может быть описание...">
        <img src="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo1.jpg" alt="florist-galery-photo1">
    </a>
    <a href="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo2.jpg" class="floristGallery-item"  data-fancybox="image"
       data-caption="Здесь может быть описание...">
        <img src="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo2.jpg" alt="florist-galery-photo2">
    </a>
    <a href="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo3.jpg" class="floristGallery-item"  data-fancybox="image"
       data-caption="Здесь может быть описание...">
        <img src="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo3.jpg" alt="florist-galery-photo3">
    </a>
    <a href="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo4.jpg" class="floristGallery-item"  data-fancybox="image"
       data-caption="Здесь может быть описание...">
        <img src="<?=SITE_TEMPLATE_PATH?>/img/florist-galery/photo4.jpg" alt="florist-galery-photo4">
    </a>
</div>