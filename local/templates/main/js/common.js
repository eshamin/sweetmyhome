$(document).ready(function () {


function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deletePersonalPhoto()
{
	$('#PERSONAL_PHOTO_del').val('Y');
	$('#saveForm').val('Y');
	$('#profileForm').submit();
}

function deleteProductCalc(itemId)
{
  BX.ajax({
	method: 'POST',
	dataType: 'json',
	url: '/ajax/',
	data: {'action': 'deleteFromBasket', 'id': itemId},
	onsuccess: function(result) {
		BX.ajax({
			url: '/ajax/?action=getFutureBouquet',
			onsuccess: function(basketResult) {
				$('#futureBouquet').html($(basketResult)[0].innerHTML);
			}
  		});
		$.get('/ajax/?action=getBasket')
			.done( function(basketResult) {
				$('#desktopBasket').html($(basketResult)[2].innerHTML);
				$('#mobileBasket').html($(basketResult)[0].innerHTML);
			});
	}
  });
}

function setQuantityCalc(itemId, action, currentQuantity)
{
  if (!currentQuantity) {
	  currentQuantity = $('#QUANTITY_INPUT_' + itemId).val();
  }

  var newQuantity = 0;
  if (action == 'set') {
	  newQuantity = parseInt(currentQuantity);
  } else if (action == 'up') {
	  newQuantity = parseInt(currentQuantity) + 1;
  } else {
	  newQuantity = parseInt(currentQuantity) - 1;
  }

  BX.ajax({
	method: 'POST',
	dataType: 'json',
	url: '/ajax/',
	data: {'action': 'setQuantity', 'id': itemId, 'quantity': newQuantity},
	onsuccess: function(result) {
		BX.ajax({
			url: '/ajax/?action=getFutureBouquet',
			onsuccess: function(basketResult) {
				$('#futureBouquet').html($(basketResult)[0].innerHTML);
			}
  		});
		$.get('/ajax/?action=getBasket')
			.done( function(basketResult) {
				$('#desktopBasket').html($(basketResult)[2].innerHTML);
				$('#mobileBasket').html($(basketResult)[0].innerHTML);
			});
	}
  });
}

function onBxAjaxSuccess()
{
	//mask
	$('input[name="phone-callback"], input[name="phone-sing"], input[name="phone"], input[name="phone-buy"], #ordering-phone, #edit-phone, #ORDER_PROP_3, #ORDER_PROP_8').mask("+7(999) 999-99-99");
	$('input[name="time-callback"], input[name="time-buy"], input[name="time"]').mask("99:99");
	$('.ourDesc p').equivalent();
	$('.addItem-desc h5').equivalent();
	$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
		removalDelay: 250,
		mainClass: 'mfp-fade'
	});
	$(".b-range").each(function(){
    var $this = $(this);
    var $thisItem = $this.parents('.addItem-top');
		var $thismax = $(this).attr('data-max'),
		    $thisCurrent = $(this).attr('data-current'),
		    $itemId = $(this).attr('data-itemid');

		$this.slider({
		  range: "min",
		  min: 1,
		  max: $thismax,
		  value: $thisCurrent,
		  stop: function (event, ui) {
		    $thisItem.find(".range-value-n").val(ui.value);
		    $this.parents('.futureItem').find(".range-value-n").val(ui.value);

		    $thisItem.find(".packs").text(ui.value);
		    $thisItem.find(".pluralForm").text(plural_form(ui.value, ['пачка', 'пачки', 'пачек']));
		    var totalPackPrice = ui.value * $thisItem.find(".packPrice").attr('data-price');
		    $thisItem.find(".buyFlowers-total-b").text(BX.Currency.currencyFormat(totalPackPrice, 'RUB', true));

		    $this.parents('.futureItem').find(".packs").text(ui.value);
		    $this.parents('.futureItem').find(".pluralForm").text(plural_form(ui.value, ['пачка', 'пачки', 'пачек']));

		    if ($itemId) {
		    	setQuantityCalc($itemId, 'set', ui.value);
			  }
		  },
    });

		$thisItem.find(".range-value-n").val($(".b-range").slider("value"));
		$this.parents('.futureItem').find(".range-value-n").val($(".b-range").slider("value"));

		$thisItem.find(".packs").text($(".b-range").slider("value"));
		$thisItem.find(".pluralForm").text(plural_form($(".b-range").slider("value"), ['пачка', 'пачки', 'пачек']));
		var totalPackPrice = $(".b-range").slider("value") * $thisItem.find(".packPrice").attr('data-price');
		//$thisItem.find(".buyFlowers-total-b").text(BX.Currency.currencyFormat(totalPackPrice, 'RUB', true));

		$this.parents('.futureItem').find(".packs").text($thisCurrent);
		$this.parents('.futureItem').find(".pluralForm").text(plural_form($thisCurrent, ['пачка', 'пачки', 'пачек']));
  });




	  $('.buyFlowers-item input[type="radio"]').on('change', function() {
      var $thisItem = $(this).parents('.addItem-top');
      var item = $(this).attr('title');

      $thisItem.find('.buyFlowers-total .flowers').text(item);

      setTimeout(function() {
        var totalPackPrice = $thisItem.find(".b-range").slider("value") * $thisItem.find(".buyFlowers-total .packPrice").attr('data-price');
        $thisItem.find('.buyFlowers-total .buyFlowers-total-b').text(BX.Currency.currencyFormat(totalPackPrice, 'RUB', true));
      }, 100);
	  });
}

  $.fn.equivalent = function (){
    var $blocks = $(this), maxH = $blocks.eq(0).height();

    $blocks.each(function(){
      maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
    });
    $blocks.height(maxH);
  }

  	$('body').on('click', '.addFutureBouquet', function (e) {
		e.preventDefault();

		var data = {'id': $(this).data('productId')};
  	});

	$('body').on('click', '.btn_load_more', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more').remove();

			$('.ajaxResult').append($(result).find('.ajaxResult').html());

			if ($(result).find('._load_more').length > 0) {
				$('.ajaxResult').after($(result).find('._load_more'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '.btn_load_more_bouquets', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more_bouquets').remove();

			$('.ajaxResultBouquets').append($(result).find('.ajaxResultBouquets').html());

			if ($(result).find('._load_more_bouquets').length > 0) {
				$('.ajaxResultBouquets').after($(result).find('._load_more_bouquets'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '.btn_load_more_presents', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more_presents').remove();

			$('.ajaxResultPresents').append($(result).find('.ajaxResultPresents').html());

			if ($(result).find('._load_more_presents').length > 0) {
				$('.ajaxResultPresents').after($(result).find('._load_more_presents'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '.btn_load_more_accessories', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more_accessories').remove();

			$('.ajaxResultAccessories').append($(result).find('.ajaxResultAccessories').html());

			if ($(result).find('._load_more_accessories').length > 0) {
				$('.ajaxResultAccessories').after($(result).find('._load_more_accessories'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '.btn_load_more_novelties', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more_novelties').remove();

			$('.ajaxResultNovelties').append($(result).find('.ajaxResultNovelties').html());

			if ($(result).find('._load_more_novelties').length > 0) {
				$('.ajaxResultNovelties').after($(result).find('._load_more_novelties'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '.btn_load_more_hits', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more_hits').remove();

			$('.ajaxResultHits').append($(result).find('.ajaxResultHits').html());

			if ($(result).find('._load_more_hits').length > 0) {
				$('.ajaxResultHits').after($(result).find('._load_more_hits'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '.btn_load_more_packs', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more_packs').remove();

			$('.ajaxResultPacks').append($(result).find('.ajaxResultPacks').html());

			if ($(result).find('._load_more_packs').length > 0) {
				$('.ajaxResultPacks').after($(result).find('._load_more_packs'));
			}
			$('.ourDesc p').equivalent();
			$('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
				removalDelay: 250,
				mainClass: 'mfp-fade'
			});
		});
	});

	$('body').on('click', '#bonus', function (e) {

		var val = 'N';
		if ($(this).prop('checked')) {
			val = 'Y';
		}

		var data = {'action': 'setPayedBonuses', 'val': val};

	    $.ajax({
	      type: "POST",
	      url: "/ajax/",
	      data: data,
	    });
	});
  //sliders
  $('.intro-slider').slick({
    speed: 1000,
    prevArrow: '<div class="intro-prev"><p>Пред</p></div>',
    nextArrow: '<div class="intro-next"><p>След</p></div>',
    responsive: [{
      breakpoint: 769,
      settings: {
        slidesToScroll: 1,
        arrows: false
      }
    }]

  });

  $('.reviews-slider').slick({
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<div class="reviews-prev"></div>',
    nextArrow: '<div class="reviews-next"></div>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 460,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      }
    ]
  });

  $('.sharesSlider-content').slick({
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<div class="reviews-prev"></div>',
    nextArrow: '<div class="reviews-next"></div>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 3,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      }
    ]
  });


  $('.addItem').slick({
    speed: 500,
    infinite: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    draggable: false,
    prevArrow: '<div class="reviews-prev"></div>',
    nextArrow: '<div class="reviews-next"></div>',
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          centerMode: true,
          slidesToScroll: 1,
          initialSlide: 2,
          arrows: false
        }
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          slidesToScroll: 1,
          initialSlide: 2,
          arrows: false
        }
      },
      {
        breakpoint: 345,
        settings: {
          slidesToShow: 1,
          centerMode: false,
          slidesToScroll: 1,
          arrows: true
        }
      }
    ]
  });

  $('.ourInfo').slick({
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: true,
    infinite: false,
    arrows: false,
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          initialSlide: 2,
          centerMode: true,
          slidesToScroll: 1

        }
      },
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 2,
          initialSlide: 1,
          centerMode: true,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          initialSlide: 1,
          centerMode: true,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.card-fotorama').fotorama({
    thumbsPreview: true,
    width: '515',
    transition: 'slide',
    arrows: false,
    nav: 'thumbs',
    fit: 'cover',
    margin: '10'
  });


  $('.certificates-content').slick({
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<div class="reviews-prev"></div>',
    nextArrow: '<div class="reviews-next"></div>',
    infinity: false,
    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          initialSlide: 1,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false

        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          initialSlide: 1,
          centerMode: true,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          initialSlide: 1,
          centerMode: true,
          slidesToScroll: 1,
          arrows: false
        }
      }
    ]
  });
  //florist gallery fancy
  $('.floristGallery a').fancybox({
    loop: true,
    showNavArrows: true
  });

  //header menu active
  /*$('.constructorNav li').click(function (even) {
    even.preventDefault();
    if (!$(this).hasClass('active')) {
      $('.constructorNav li').removeClass('active');
      $(this).addClass('active');
    }
  });*/

  $('body').on('click', '.ourNav li a', function (event) {
    event.preventDefault();
  });

  $('body').on('click', '.menu li', function () {
    if (!$(this).hasClass('active')) {
      $('.menu li').removeClass('active');
      $(this).addClass('active');
    }
  });

  //ordering form (add and remove inputs)
  $('.for-entity').hide();
  $('body').on('change', '.ordering-type', function () {
    var fei = $('.for-entity input');
    if ($('#entity').prop('checked')) {
      $('.for-entity').show();
      fei.attr('type', 'text');
      fei.attr('required', 'required');
    } else {
      fei.attr('type', 'hidden');
      fei.removeAttr('required');
      $('.for-entity').hide();
    }
  });


  //ordering form (add and remove input address)
  $('body').on('change', '.ordering-delivery', function () {
    var ai = $('#address input');
    if ($('#ordering-pickup').prop('checked')) {
      ai.attr('type', 'hidden');
      ai.removeAttr('required');
      $('#address').hide();
    } else {
      ai.attr('type', 'text');
      ai.attr('required', 'required');
      $('#address').show();
    }
  });


  //for subscribe
  $('.subscribe').on('submit', function (event) {
    event.preventDefault();
  });

  //for Callback
  $('.formCallback').on('submit', function (event) {
    event.preventDefault();
  });

  // products tabs
  $('body').on('click', '.card-bot .ourNav li', function () {
    if (!$('.card-bot .ourNav li:first-child').hasClass('active')) {
      $('.card-desc').show();
      $('.card-reviews').hide();
    } else {
      $('.card-reviews').show();
      $('.card-desc').hide();
    }
  });

  //spiner (value = 1)
  /*$(".spin-up").click(function() {
      var value = parseInt($(this).closest(".spinner-card").find("input").val());
      if (value != 99){
          $(this).closest(".spinner-card").find("input").val(value + 1);
      }
  });

  $(".spin-down").click(function() {
      var value = parseInt($(this).closest(".spinner-card").find("input").val());
      if (value != 1) {
          $(this).closest(".spinner-card").find("input").val(value - 1);
      }
  });

  //spiner global (value = 0)
  $(".spin-up").click(function() {
      var value = parseInt($(this).closest(".spinner-global").find("input").val());
      if (value != 99){
          $(this).closest(".spinner-global").find("input").val(value + 1);
      }
  });

  $(".spin-down").click(function() {
      var value = parseInt($(this).closest(".spinner-global").find("input").val());
      if (value != 0) {
          $(this).closest(".spinner-global").find("input").val(value - 1);
      }
  });*/

  //tabs active
  $('body').on('click', '.ourNav li', function () {
    if (!$(this).hasClass('active')) {
      $('.ourNav li').removeClass('active');
      $(this).addClass('active');
    }
  });

  //sort tab
  $('body').on('click', '.sort li', function () {
    $('.sort li').not(this).removeClass();
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
    } else {
      $(this).toggleClass('sorts');
    }
  });

  //item select
  $('body').on('click', '.addItem-item a', function (event) {
    event.preventDefault();
    $(this).parent().addClass('select');
  });

  //accordion filter
  $('body').on('click', '.filter-category:not(:first-child) p', function () {
    $(this).toggleClass('active');
    $(this).next().slideToggle();
  });


function plural_form(number, after)
{
	var cases = [2, 0, 1, 1, 1, 2],
		result;
	if (number % 100 > 4 && number % 100 < 20) {
		result = after[2];
	} else {
		result = after[cases[Math.min(number % 10, 5)]];
	}
	return result;
}

  //rotate card
  $('body').on('click', '.addItem-btn, .future-edit', function () {
    $(this).parents('.rotate').addClass('active');
    $(this).parents('.addItem-item').siblings().find('.rotate').removeClass('active');

  });
  $('body').on('click', '.buyFlowers .close-btn', function () {
    $(this).parents('.rotate').removeClass('active');
  });
  $('body').on('click', '.rotate-next .close-bth', function () {
    $('.addItem-top').removeClass('active');
  });

  //add Basket
  /*$('.addBasket').click(function (event) {
    event.preventDefault();
    $('.basket-alert').fadeIn(500);
    $('.alert-close').click(function () {
      $('.basket-alert').fadeOut(500);
    });

  });*/

  //burger animation
  $('body').on('click', '.burger', function () {
    $('.Lside').addClass('active');
    $('.wrapper').addClass('fon');


  });
  $('body').on('click', '.Lside-close', function () {
    $('.Lside').removeClass('active');
    $('.wrapper').removeClass('fon');
  });

  //
  $('body').on('click', '.Lmenu-about', function () {
    $(this).toggleClass('active');
    $('.sub-about').slideToggle();
  });

  if ($('#futureBouquet')) {
	BX.ajax({
		url: '/ajax/?action=getFutureBouquet',
		onsuccess: function(basketResult) {
			$('#futureBouquet').html($(basketResult)[0].innerHTML);
		}
  	});
  }

  $('body').on('click', '#collectFutureBouquet', function () {
	  BX.ajax({
		method: 'POST',
		dataType: 'json',
		url: '/ajax/',
		data: {'action': 'collectFutureBouquet'},
		onsuccess: function(result) {
			console.log(result);
		    $('#productName').html(result.MESSAGE);
		    $('.basket-alert').fadeIn(500);
		    $('.alert-close').click(function () {
		        $('.basket-alert').fadeOut(500);
		    });

			BX.ajax({
				url: '/ajax/?action=getFutureBouquet',
				onsuccess: function(basketResult) {
					//console.log(basketResult);
					$('#futureBouquet').html($(basketResult)[0].innerHTML);
				}
  			});
			$.get('/ajax/?action=getBasket')
				.done( function(basketResult) {
					$('#desktopBasket').html($(basketResult)[2].innerHTML);
					$('#mobileBasket').html($(basketResult)[0].innerHTML);
				});
		}
	  });
  });

  // filter
  var filterCount = 0;

  $('#filter-reset').on('click', function () {
    setFilterCount(0);
    var range = $('.f-range');
    range.slider('values', 0, $('#arrFilter_P1_MIN').val());
    range.slider('values', 1, $('#arrFilter_P1_MAX').val());
    $('.range-value').text("От " + $('#arrFilter_P1_MIN').val() + " до " + $('#arrFilter_P1_MAX').val() + " руб");
    $('.form-filter').submit();
  });

  $('.buyFlowers-item input[type="radio"]').on('change', function() {
    var item = $(this).attr('title');
    $(this).parents('.buyFlowers').find('.buyFlowers-total .flowers').text(item);
  });


  setFilterCount($('input[type="checkbox"]:checked').length);

  $('body').on('change', 'input[type="checkbox"]', function () {
    var filterValue = $(this).prop('checked') ? (filterCount + 1) : (filterCount - 1);
    setFilterCount(filterValue);
  });

  function setFilterCount(count) {
    filterCount = count;
    $('#filterCount').text(filterCount);
  }


  //accordion filter table (basket, private-ordering)
  $('body').on('click', '.tableOrder span', function () {
    $(this).parents('.tableOrder').next().slideToggle(10);
  });
  $('body').on('click', '.tableOrder-name span', function () {
    $(this).toggleClass('active');
  });


  //burger animation for tabs (mobile version)
  $('body').on('click', '.constructor-toggle', function () {
    $('.burger__line_constructor').toggleClass('active');
    $('.constructorNav').slideToggle();

  });


  var tabs = $('.tabs');
  tabs.each(function () {
    var tabs = $(this),
      tab = tabs.find('.tabs__tab'),
      content = tabs.find('.tabs__item');
    tab.each(function (index, element) {
      $(this).attr('data-tab', index);
    });

    function showContent(i) {
      tab.removeClass('active');
      content.removeClass('active').removeClass('fade');
      tab.eq(i).addClass('active');
      content.eq(i).addClass('active');
      setTimeout(function () {
        content.eq(i).addClass('fade');
      }, 1);
    }

    tab.on('click', function (e) {
      e.preventDefault();
      showContent(parseInt($(this).attr('data-tab')));
    });
  });



  $(window).on('load', function () {
    $('.jsSliderForNav').each(function () {
      var $for = $(this).find('.jsSliderFor'),
          $nav = $(this).find('.jsSliderNav');
      $for.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        speed: 600,
        fade: true,
        asNavFor: $nav
      });
      $nav.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: $for,
        dots: false,
        speed: 600,
        focusOnSelect: true,
        responsive: [{
          breakpoint: 1200,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 3
          }
        },
        {
          breakpoint: 460,
          settings: {
            slidesToShow: 2
          }
        }
      ]
      });
    });
  });

  //slider
  if (document.documentElement.clientWidth < 1200) {
    function initSlides(){
      $('.mainSlider').each(function(){
        if (!$(this).hasClass('inited')){
          $(this).addClass('inited');
          $(this).slick({
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: true,
            variableWidth: true,
            arrows: false,
            responsive: [{
                breakpoint: 1200,
                settings: {
                  slidesToShow: 3,
                  initialSlide: 2,
                  centerMode: true,
                  slidesToScroll: 1

                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  initialSlide: 1,
                  centerMode: true,
                  slidesToScroll: 1
                }
              },

              {
                breakpoint: 480,
                settings: {
                  slidesToShow: 1,
                  initialSlide: 1,
                  centerMode: true,
                  slidesToScroll: 1
                }
              }
            ]
          });
        }
      });

    }

    initSlides();

    $('.ourFlowers .tabs__tab').on('click', function (e) {
      initSlides();
    });

  }

/*
  if (document.documentElement.clientWidth < 992) {
    $('.goods-content').slick({
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: false,
      prevArrow: '<div class="reviews-prev"></div>',
      nextArrow: '<div class="reviews-next"></div>',
      responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            initialSlide: 1,
            centerMode: false,
            slidesToScroll: 1,
            arrows: true,
          }
        },

        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            initialSlide: 1,
            centerMode: false,
            slidesToScroll: 1,
          }
        }
      ]
    });
  }*/
  //popUp
  $('.header-callback p a, .footer-right-callback, .Lmenu>li:last-child a, .ourSelect li:last-child a, .login, .policyLink, .card-top a, .ordering-table td p a').magnificPopup({
    removalDelay: 250,
    mainClass: 'mfp-fade'
  });

  onBxAjaxSuccess();
  BX.addCustomEvent('onAjaxSuccess', onBxAjaxSuccess);

  //for Callback
  $('#formCallback').on('submit', function (event) {
    $.ajax({
      type: "POST",
      url: "/ajax/",
      data: $(this).serialize()
    }).done(function () {
      $(this).find("input").val("");
      $('.wrapper').addClass('fon');
      $('.mfp-close-btn-in .mfp-close').click();
      $('.alert-application').fadeIn(200);
      $('.alert-close').click(function () {
        $('.alert-application').fadeOut(200);
        $('.wrapper').removeClass('fon');
      });
      $("#formBuy").trigger("reset");
    });
    return false;
  });

  $("#subscribe").submit(function () {
    $.ajax({
      type: "POST",
      url: "/ajax/",
      data: $(this).serialize()
    }).done(function () {
      $(this).find("input").val("");
      $('.subscribe-alert').fadeIn(200);
      $('.wrapper').addClass('fon');
      $('.alert-close').click(function () {
        $('.subscribe-alert').fadeOut(200);
        $('.wrapper').removeClass('fon');
      });
      $("#subscribe").trigger("reset");
    });
    return false;
  });

  $("#formBuy").submit(function () {
    $.ajax({
      type: "POST",
      url: "/ajax/",
      data: $(this).serialize()
    }).done(function () {
      $(this).find("input").val("");
      $('.wrapper').addClass('fon');
      $('.mfp-close-btn-in .mfp-close').click();
      $('.alert-application').fadeIn(200);
      $('.alert-close').click(function () {
        $('.alert-application').fadeOut(200);
        $('.wrapper').removeClass('fon');
      });
      $("#formBuy").trigger("reset");
    });
    return false;
  });

  $('body').on('click', '.ourSelect li:last-child a', function () {
    $('#formBuy #productId').val($(this).data('productid'));
  });

  $('.xzoom, .xzoom-gallery').xzoom({
    title: true,
    tint: '#333',
    position: 'inside',
    Xoffset: 0,
    Yoffset: 0,
    fadeIn: true,
    fadeTrans: true,
    loadingClass: 'xzoom-loading',
    lensClass: 'xzoom-lens',
    zoomClass: 'xzoom-preview',
    activeClass: 'xactive',
    lensOpacity: 0.5,
    lensShape: 'box',
    zoomHeight: 'auto',
    zoomWidth: 'auto',
  });

    /*$('body').on('click', '.addBasket, .addPresent', function (event) {
        event.preventDefault();
        $('#productName').html($(this).data('productname'));
        $('.basket-alert').fadeIn(500);
        $('.alert-close').click(function () {
            $('.basket-alert').fadeOut(500);
        });

		$.get('/ajax/?action=getBasket')
			.done( function(basketResult) {
				$('#desktopBasket').html($(basketResult)[2].innerHTML);
				$('#mobileBasket').html($(basketResult)[0].innerHTML);
			});

    });*/

  $( window ).on('load', function() {
    $('.ourDesc p').equivalent();
    $('.addItem-desc h5').equivalent();
  });
  $( window ).on('resize', function() {
    $('.ourDesc p').equivalent();
    $('.addItem-desc h5').equivalent();
  });

	if (getCookie('metadataCollect') != 'Y') {
		$('.b_collect_metadata').fadeIn();
	}
	$('.close_metadata').click(function () {
		$(this).parents('.b_collect_metadata').fadeOut(300);
		var date = new Date(new Date().getTime() + 365 * 24 * 3600 * 1000);
		document.cookie = "metadataCollect=Y; path=/; expires=" + date.toUTCString();
	});

  $('.file-upload input[type="file"]').change(function(){
    if ($(this).val()!==''){
      $('#file_title').text($(this)[0].files[0].name);
      }
      else {
        $('#file_title').text('Файл не выбран')
      }
  });
});