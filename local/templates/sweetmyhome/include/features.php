<div class="row ourInfo">
    <div class="ourInfo-item">
        <div class="ourInfo-desc">
            <p>Бесплатная доставка</p>
            <span>Круглосуточная бесплатная доставка</span>
        </div>
    </div>
    <div class="ourInfo-item ">
        <div class="ourInfo-desc">
            <p>Фото перед доставкой</p>
            <span>Отправляем на WhatsApp/ Telegram/Viber или почту</span>
        </div>
    </div>
    <div class="ourInfo-item ">
        <div class="ourInfo-desc">
            <p>Подарочная упаковка</p>
            <span>Эксклюзивная подарочная коробка</span>
        </div>
    </div>
    <div class="ourInfo-item ">
        <div class="ourInfo-desc">
            <p>Удобная оплата</p>
            <span>Круглосуточная бесплатная доставка</span>
        </div>
    </div>
</div>