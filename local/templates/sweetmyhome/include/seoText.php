<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$arFilter = [
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => IB_SEO,
	'PROPERTY_URL' => $APPLICATION->GetCurPage(),
];

$arSelect = ['NAME', 'PREVIEW_TEXT', 'PROPERTY_URL'];

$arSeo = CIBlockElement::GetList([], $arFilter, false, false, $arSelect)->Fetch();
if ($arSeo) {
?>

<div class="desc-category">
    <span><?=$arSeo['NAME']?></span>
    <div class="desc-txt">
		<?=$arSeo['PREVIEW_TEXT'];?>
	</div>
</div>
<?}?>