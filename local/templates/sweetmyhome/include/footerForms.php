<div class="hidden">
    <div class="callback" id="callback">
        <section class="section">
            <div class="container">
                <h2 class="section-title">
                    <span>Обратный звонок</span>
                </h2>
                <p class="section-desc">Оставьте свои котактные данные и мы свяжемся с вами в указанное время</p>
                <form action="/" class="formCallback" id="formCallback" method="post">
                    <input type="text" name="name" placeholder="Ваше имя" required>
                    <input type="text" name="phone" placeholder="Ваш номер телефона" required>
                    <input type="text" name="time" placeholder="В какое время вам позвонить?">
                    <span class="policyText"><input type="checkbox" name="policyAgree" value="Y" required>Даю согласие на обработку <a href="#policy" class="policyLink">персональных данных</a></span>
                    <input type="hidden" name="action" value="addFeedback">
                    <input type="submit" name="send-callback" value="Отправить" class="form-btn">
                </form>
            </div>
        </section>
    </div>
    <div class="buy" id="buy">
        <section class="section">
            <div class="container">
                <h2 class="section-title">
                    <span>Купить в один клик</span>
                </h2>
                <p class="section-desc">Оставьте свои котактные данные и мы свяжемся с вами в указанное время</p>
                <form class="formBuy" id="formBuy" method="post">
                    <input type="text" name="name" placeholder="Ваше имя" required>
                    <input type="text" name="phone" placeholder="Ваш номер телефона" required>
                    <input type="text" name="time" placeholder="В какое время вам позвонить?">
                    <span class="policyText"><input type="checkbox" name="policyAgree" value="Y" required>Даю согласие на обработку <a href="#policy" class="policyLink">персональных данных</a></span>
                    <input type="hidden" name="productId" id="productId">
                    <input type="hidden" name="action" value="addOneClickOrder">
                    <input type="submit" name="send-buy" value="Отправить" class="form-btn">
                </form>
            </div>
        </section>
    </div>
    <div class="sing" id="sing">
        <section class="section">
            <div class="container">
                <h2 class="section-title">
                    <span>Вход и регистрация</span>
                </h2>
                <p class="section-desc">Введите свой номер телефона и мы отправим вам СМС с кодом для авторизации</p>
                <form action="/" class="formSing" id="formSing" method="post">
                    <input type="text" name="phone-sing" placeholder="Ваш номер телефона" required>
                    <button name="confirm">Подтвердить</button>
                    <input type="text" name="code-sms" placeholder="Введите код из СМС" required>
                    <span class="policyText"><input type="checkbox" name="policyAgree" value="Y" required>Даю согласие на обработку <a href="#policy" class="policyLink">персональных данных</a></span>
                    <input type="submit" name="send-callback" value="Войти" class="form-btn">
                </form>
            </div>
        </section>
    </div>

    <div class="policy" id="policy">
        <section class="section">
            <div class="container">
                <h2 class="section-title">
                    <span>Политика конфиденциальности</span>
                </h2>
                <p class="section-desc">Тут скоро появится текст политики конфиденциальности</p>
            </div>
        </section>
    </div>
</div>
<div class="alerts">
    <div class="subscribe-alert">
        <p class="basket-alert-txt">Ваша заявка на подписку успешно отправлена!</p>
        <div class="alert-close"></div>
    </div>
    <div class="alert-application">
        <section class="section">
            <div class="container">
                <h2 class="section-title">
                    <span>Спасибо за заявку!</span>
                </h2>
                <p class="section-desc">Ваше сообщение успешно отправлено,<br> мы свяжемся с вами в указанное время</p>
                <div class="alert-img"></div>
            </div>
        </section>
        <span class="alert-close"></span>
    </div>
    <div class="basket-alert">
        <p class="basket-alert-title">Товар добавлен в корзину</p>
        <p class="basket-alert-txt">Товар <span class="basket-alert-product" id="productName">Букет "Свадьба"</span> успешно добавлен в корзину!</p>
        <div class="alert-close"></div>
    </div>
</div>