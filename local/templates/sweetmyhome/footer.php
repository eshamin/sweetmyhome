<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
    <footer class="footer">
        <div class="container">
            <div class="footer-content">
                <div class="footer-left">
                    <a href="/" class="footer-logo">
                        <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="logo">
                    </a>
                    <p>© <?=date('Y')?> “Sweet My Home”.<br>
                        All rigths reserved <?=date('Y')?>.</p>
                    <a href="#policy" class="policyLink">Политика конфиденциальности</a>
                </div>
                <div class="footer-center">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottomMenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					);?>
                    <div class="Cbot">
                        <p>
						<?
						$APPLICATION->IncludeFile(
							SITE_TEMPLATE_PATH . '/include/footerSlogan.php',
							[],
							['mode' => 'html']);
						?>
						</p>
                    </div>
                </div>
                <div class="footer-right">
                    <a href="tel:+79857709926" class="footer-right-phone">7 (985) 770-9926</a>
                    <a href="#callback" class="footer-right-callback">Заказать обратный звонок</a>
                    <ul class="social">
                        <li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
                    </ul>
                    <div class="footer-info">
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="Lside ">
        <p>Меню</p>
		<?$APPLICATION->IncludeComponent("bitrix:menu", "mobileMenu", Array(
			"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"MAX_LEVEL" => "1",	// Уровень вложенности меню
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MENU_CACHE_TIME" => "3600000",	// Время кеширования (сек.)
				"MENU_CACHE_TYPE" => "A",	// Тип кеширования
				"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
				"ROOT_MENU_TYPE" => "mobile",	// Тип меню для первого уровня
				"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			),
			false
		);?>
    </div>
	<?
	$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH . '/include/footerForms.php',
		[],
		['mode' => 'html']);
	?>
</div>


  <div class="b_collect_metadata">
    <div class="close_metadata"></div>
    <div class="_metadata_title">
      Мы собираем метаданные
    </div>
    <div class="_metadata_law">
      <a href="#">152-Федеральный Закон</a> обязывает нас уведомить вас о том, что в целях корректного функционирования сайта, мы собираем метаданные, такие как cookie, данные об IP-адресе и местоположении
    </div>
    <div class="_metadata_understanding">
      Если вы не хотите, чтобы эти данные обрабатывались, то должны покинуть сайт.
      <br>
      Спасибо за понимание.
    </div>
  </div>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/common.js"></script>
</body>
</html>