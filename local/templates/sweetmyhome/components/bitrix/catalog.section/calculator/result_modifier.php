<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

foreach ($arResult['ITEMS'] as $arItemSet) {
	$arSets = CCatalogProductSet::getAllSetsByProduct($arItemSet['ID'], CCatalogProductSet::TYPE_SET);

	$bExist = false;
	if ($arSets) {
		foreach ($arSets as $arSet) {
			foreach ($arSet['ITEMS'] as $arItem) {
				/*$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();
				$parentCategory = CIBlockSection::GetNavChain(false, $arElement['IBLOCK_SECTION_ID'])->Fetch();
				if ($parentCategory['ID'] == 2) {
					$arResult['ITEM']['SET'][] = "<span>{$arElement['NAME']}</span> - <span>{$arItem['QUANTITY']} шт.</span>";
				}*/

				if (array_key_exists($arItem['ITEM_ID'], $_SESSION['futureBouquet'])) {
					$bExist = true;
					break;
				}
			}
		}
	} else {
		$bExist = true;
	}

	if ($bExist) {
		$arResultItems[] = $arItemSet;
	}
}

$arResult['ITEMS'] = $arResultItems;
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();