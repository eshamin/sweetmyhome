<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>


<section class="section-private section-private-dec">
    <div class="container">
        <div class="private">
            <div class="private-user">
                <?=getProfileInfo($arResult['ID'])?>
            </div>
            <div class="ournav-scr">
                <ul class="ourNavAbout">
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/') {?>class="active"<?}?>><a href="/personal/">редактирование профиля</a></li>
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/history/') {?>class="active"<?}?>><a href="/personal/history/">история заказов</a></li>
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/orders/') {?>class="active"<?}?>><a href="/personal/orders/">Мои заказы</a></li>
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/alerts/') {?>class="active"<?}?>><a href="/personal/alerts/">настройка оповещений</a></li>
                </ul>
            </div>
			<?ShowError($arResult["strProfileError"]);?>
			<?
			if ($arResult['DATA_SAVED'] == 'Y')
				ShowNote(GetMessage('PROFILE_DATA_SAVED'));
			?>
            <div class="private-alerts alerts">
                <p>Настройка оповещений</p>
                <form class="form-edit" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
					<?=$arResult["BX_SESSION_CHECK"]?>
					<input type="hidden" name="lang" value="<?=LANG?>" />
					<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                    <table class="settingTable">
                        <thead>
                            <tr><th>Тип оповещений</th><th>E-mail</th><th>SMS</th></tr>
                        </thead>
                        <tbody>

						<?foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField) {?>
						<?//debug($arUserField)?>
						<tr><td>
							<?=$arUserField["EDIT_FORM_LABEL"]?>:</td>
								<?$APPLICATION->IncludeComponent(
									"bitrix:system.field.edit",
									'checkbox',
									array(
										"bVarsFromForm" => $arResult["bVarsFromForm"],
										"arUserField" => $arUserField),
									null,
									array("HIDE_ICONS"=>"N"));?></tr>
						<?}?>
                        </tbody>
                    </table>
                    <div class="edit-btn">
                        <a href="/personal/"><span></span>вернуться</a>
                        <input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>