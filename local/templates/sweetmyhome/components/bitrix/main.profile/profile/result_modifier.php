<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
if ($arResult["ID"] > 0) {
	$arUser = CUser::GetByID($arResult["ID"])->Fetch();
	$arResult['PHOTO'] = CFile::GetPath($arUser['PERSONAL_PHOTO']);
}

?>