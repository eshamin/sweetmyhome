<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<section class="section-private">
    <div class="container">
        <div class="private">
            <div class="private-user">
				<?=getProfileInfo($arResult['ID'])?>
            </div>
            <div class="ournav-scr">
                <ul class="ourNavAbout">
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/') {?>class="active"<?}?>><a href="/personal/">редактирование профиля</a></li>
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/history/') {?>class="active"<?}?>><a href="/personal/history/">история заказов</a></li>
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/orders/') {?>class="active"<?}?>><a href="/personal/orders/">Мои заказы</a></li>
	                <li <?if ($APPLICATION->GetCurPage() == '/personal/alerts/') {?>class="active"<?}?>><a href="/personal/alerts/">настройка оповещений</a></li>
                </ul>
            </div>
			<?ShowError($arResult["strProfileError"]);?>
			<?
			if ($arResult['DATA_SAVED'] == 'Y') {
				ShowNote(GetMessage('PROFILE_DATA_SAVED'));
			}
			?>
            <div class="private-edit">
                <p>Редактирование профиля</p>
				<form id="profileForm" class="form-edit" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
					<?=$arResult["BX_SESSION_CHECK"]?>
					<input type="hidden" name="lang" value="<?=LANG?>" />
					<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
                    <table class="tableEdit">
                        <tr>
                            <td>Фото</td>
                            <td>
                                <div class="edit-photo">
                                <?if ($arResult['PHOTO']) {?>
	                                <img src="<?=$arResult['PHOTO']?>" alt="user-img">
	                                <a href="#" onclick="deletePersonalPhoto()" class="close-btn"></a>
	                                <input type="hidden" name="PERSONAL_PHOTO_del" value="N" id="PERSONAL_PHOTO_del" />
	                                <input type="hidden" name="save" value="" id="saveForm" />
	                            <?}?>
                                </div>
                                <label class="file-upload">
                                    <span class="photo-button">Изменить</span>
                                    <input type="file" name="PERSONAL_PHOTO">
                                    <div id="file_title">

                                    </div>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>Имя<span class="star-c">*</span></td>
                            <td><input type="text" name="NAME" required value="<?=$arResult["arUser"]["NAME"]?>"></td>
                        </tr>
                        <tr>
                            <td>E-mail<span class="star-c">*</span></td>
                            <td><input type="email" name="EMAIL" value="<?=$arResult["arUser"]["EMAIL"]?>" required></td>
                        </tr>
                        <tr class="table-phone">
                            <td>Телефон<span class="star-c">*</span></td>
                            <td>
                                <input type="text" name="PERSONAL_PHONE" id="edit-phone" value="<?=$arResult["arUser"]["PERSONAL_PHONE"]?>" required>
                                <label for="edit-phone">Для подтверждения нового номера вам будет выслан на него SMS-код</label>
                            </td>
                        </tr>
                        <tr>
                            <td>Дата рождения</td>
                            <td>
							<?
								$APPLICATION->IncludeComponent(
									'bitrix:main.calendar',
									'',
									array(
										'SHOW_INPUT' => 'Y',
										'FORM_NAME' => 'form1',
										'INPUT_NAME' => 'PERSONAL_BIRTHDAY',
										'INPUT_VALUE' => $arResult["arUser"]["PERSONAL_BIRTHDAY"],
										'SHOW_TIME' => 'N'
									),
									null,
									array('HIDE_ICONS' => 'N')
								);
							?>
                            </td>
                        </tr>
                        <tr class="edit-radio">
                            <td>Пол</td>
                            <td>
                                <input type="radio" name="PERSONAL_GENDER" id="man" value="M"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "M" ? " checked" : ""?> >
                                <label for="man">Мужской</label>
                            </td>

                            <td>
                                <input type="radio" name="PERSONAL_GENDER" id="woman" value="F"<?=$arResult["arUser"]["PERSONAL_GENDER"] == "F" ? " checked" : ""?>>
                                <label for="woman">Женский</label>
                            </td>
                        </tr>
                    </table>
                    <div class="edit-btn">
                        <a href="<?=$_REQUEST['back_url']?>"><span></span>вернуться</a>
                        <input type="submit" name="save" value="<?=(($arResult["ID"]>0) ? GetMessage("MAIN_SAVE") : GetMessage("MAIN_ADD"))?>">
                    </div>
                </form>
                <a href="#" class="edit-license">* Отправляя заявку, Вы соглашается на обработку персональных данных</a>
            </div>
        </div>
    </div>
</section>
