<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0) {
?>

<div id="basket_items_list" class="private-order">
	<p>Ваш заказ</p>
	<div class="bx_ordercart_order_table_container">
		<table id="basket_items" class="tableOrder tableBasket">
			<thead>
				<tr>
					<?
					foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
						$arHeaders[] = $arHeader["id"];

						// remember which values should be shown not in the separate columns, but inside other columns
						if (in_array($arHeader["id"], array("TYPE")))
						{
							$bPriceType = true;
							continue;
						}
						elseif ($arHeader["id"] == "PROPS")
						{
							$bPropsColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "DELAY")
						{
							$bDelayColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "DELETE")
						{
							$bDeleteColumn = true;
							continue;
						}
						elseif ($arHeader["id"] == "WEIGHT")
						{
							$bWeightColumn = true;
						}

						if ($arHeader["id"] == "NAME") {
						?>
							<th id="col_<?=$arHeader["id"];?>">Наименование</th>
						<?
						} elseif ($arHeader["id"] == "PRICE") {
						?>
							<th id="col_<?=$arHeader["id"];?>">Цена</th>
						<?
						} elseif ($arHeader["id"] == "QUANTITY") {
						?>
							<th class="col-quantity" id="col_<?=$arHeader["id"];?>">Кол-во</th>
						<?
						} elseif ($arHeader["id"] == "DISCOUNT") {
						?>
							<th class="col-amount" id="col_<?=$arHeader["id"];?>">Скидка</th>
						<?
						} elseif ($arHeader["id"] == "SUM") {
						?>
							<th id="col_<?=$arHeader["id"];?>">Итого</th>
							<th></th>
						<?
						}
						?>
					<?
					endforeach;
					?>

				</tr>
			</thead>

			<tbody>
				<?
				$skipHeaders = array('PROPS', 'DELAY', 'TYPE');

				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem) {

					$intCountInSet = 0;
					$arComposition = [];
					$arCatalogItem = CIBlockElement::GetByID($arItem['PRODUCT_ID'])->Fetch();
					if ($arCatalogItem['IBLOCK_SECTION_ID'] == 22) {
						$arSets = CCatalogProductSet::getAllSetsByProduct($arItem['PRODUCT_ID'], CCatalogProductSet::TYPE_SET);
						foreach ($arSets as $arSet) {
							foreach ($arSet['ITEMS'] as $arItemSet) {
								$arFilter = ['ID' => $arItemSet['ITEM_ID']];
								$arSelect = ['ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'PROPERTY_CML2_LINK'];
								$arElement = CIBlockElement::GetList([], $arFilter, false, false, $arSelect)->Fetch();

								if ($arElement['IBLOCK_ID'] == 9) {
									$arParentElement = CIBlockElement::GetByID($arElement['PROPERTY_CML2_LINK_VALUE'])->Fetch();
									$arElement['PREVIEW_PICTURE'] = $arParentElement['PREVIEW_PICTURE'];
								}
								$arElement['QUANTITY'] = $arItemSet['QUANTITY'];
								$arComposition[] = $arElement;
							}
						}
					}

					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y") {
					?>
					<tr id="<?=$arItem["ID"]?>"
						 data-item-name="<?=$arItem["NAME"]?>"
						 data-item-brand="<?=$arItem[$arParams['BRAND_PROPERTY']."_VALUE"]?>"
						 data-item-price="<?=$arItem["PRICE"]?>"
						 data-item-currency="<?=$arItem["CURRENCY"]?>"
					>
						<?
						foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {

							if (in_array($arHeader["id"], $skipHeaders)) // some values are not shown in the columns in this template
								continue;

							if ($arHeader["name"] == '') {
								$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
							}

							if ($arHeader["id"] == "NAME") {
							?>
								<td data-title="Наименование">
									<?
									if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
										$url = $arItem["PREVIEW_PICTURE_SRC"];
									elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
										$url = $arItem["DETAIL_PICTURE_SRC"];
									else:
										$url = $templateFolder."/images/no_photo.png";
									endif;
									?>
					                <div class="tableOrder-img">
					                	<?if (!empty($arComposition)) {
					                		foreach ($arComposition as $arCompositionItem) {?>
			                                    <div class="tableOrder-groupItem">
			                                        <img src="<?=($arCompositionItem['PREVIEW_PICTURE']) ? CFile::GetPath($arCompositionItem['PREVIEW_PICTURE']) : $templateFolder."/images/no_photo.png"?>" alt="flowers3">
			                                    </div>
		                                    <?}?>
					                	<?} else {?>
					                	<img src="<?=$url?>" alt="<?=$arItem["NAME"]?>" />
					                	<?}?>
					                </div>
									<p class="tableOrder-name">
										<?=$arItem["NAME"]?>
										<?if (!empty($arComposition)) {?>
											<span class="active">Комплект из <?=count($arComposition)?> <?=plural_form(count($arComposition), ['товар', 'товара', 'товаров'])?></span>
										<?}?>
									</p>
								</td>
							<?
							} elseif ($arHeader["id"] == "QUANTITY") {
							?>
								<td data-title="Кол-во">

									<?
									$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
									$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
									$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
									?>

									<?
									if (!isset($arItem["MEASURE_RATIO"])) {
										$arItem["MEASURE_RATIO"] = 1;
									}

									?>
									<div class="spinner-card" >
										<div class="spin-down" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);">-</div>
										<input
											type="text"
											id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
											name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
											value="<?=$arItem["QUANTITY"]?>"
											onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
										/>
										<div class="spin-up" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);">+</div>
									</div>
									<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
								</td>
							<?
							} elseif ($arHeader["id"] == "PRICE") {
							?>
								<td data-title="Цена">
									<label class="price" data-price="" id="current_price_<?=$arItem["ID"]?>">
										<?=$arItem["FULL_PRICE_FORMATED"]?>
									</label>

									<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
										<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
										<div class="type_price_value"><?=$arItem["NOTES"]?></div>
									<?endif;?>
								</td>
							<?
							} elseif ($arHeader["id"] == "DISCOUNT") {
							?>
								<td class="col-amount">
									<span class="price" id="discount_value_<?=$arItem["ID"]?>"><?=$arItem["DISCOUNT_PRICE"]?> руб.</span>
								</td>
							<?
							} elseif ($arHeader["id"] == "DELETE") {
							?>
			                    <td>
			                        <!--<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" onclick="return deleteProductRow(this)" class="basket-remove"></a>-->
			                        <div data-href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" onclick="return deleteProductRow(this)" class="basket-remove"></div>
			                    </td>
							<?
							} elseif ($arHeader["id"] == "SUM") {
							?>
			                    <td data-title="Итого">
			                    	<label id="sum_<?=$arItem["ID"]?>" class="price" data-price=""><?=str_replace(" ", "&nbsp;", $arItem["SUM"])?></label>
			                    </td>
							<?
							} elseif ($arHeader["id"] == "WEIGHT") {
							?>
								<td class="custom">
									<span><?=$arHeader["name"]; ?>:</span>
									<?=$arItem["WEIGHT_FORMATED"]?>
								</td>
							<?
							}
						}?>
					</tr>

					<?
					if (!empty($arComposition)) {?>
						<tr>
							<td colspan="5" style="border: none">
			                    <table class="subBasket subTable">
			                    <?foreach ($arComposition as $arCompositionItem) {
									$arProduct = CCatalogProductProvider::GetProductData(
										[
											'PRODUCT_ID' => $arCompositionItem['ID'],
											'QUANTITY' => $arCompositionItem['QUANTITY'],
										]
									);
			                    	?>
			                        <tr>
			                            <td data-title="Наименование">
			                                <div class="tableOrder-img">
			                                    <img src="<?=($arCompositionItem['PREVIEW_PICTURE']) ? CFile::GetPath($arCompositionItem['PREVIEW_PICTURE']) : $templateFolder."/images/no_photo.png"?>" alt="flowers3">
			                                </div>
			                                <p class="tableOrder-name"><?=$arCompositionItem['NAME']?></p>
			                            </td>
			                            <td data-title="Цена">
			                                <?=number_format($arProduct['PRICE'], 0, '.', ' ')?> руб./шт
			                            </td>
			                            <td>
			                            	<?=$arProduct['QUANTITY']?> шт.
			                            </td>
			                            <td data-title="Итого">
			                                <?=number_format($arProduct['PRICE'] * $arProduct['QUANTITY'], 0, '.', ' ')?> руб
			                            </td>
			                            <td>
			                            </td>
			                        </tr>
			                    <?}?>
			                    </table>
							</td>
						</tr>
					<?}
					?>
					<?
					}
				}
				?>
			</tbody>
		</table>
		</div>

        <div class="ordering-bottom">
            <div class="ordering-total">
                <p>Итого: <span id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></p>
            </div>
            <div class="coupon-code" id="coupons_block_not_need">
                <label for="coupon-code">
                  <input value="<?=$arResult['COUPON']?>" type="text" onchange="enterCoupon();" name="COUPON" id="coupon" placeholder="Введите код купона">
                </label>
				<?if (!empty($arResult['COUPON_LIST']))
				{
					foreach ($arResult['COUPON_LIST'] as $oneCoupon)
					{
						$couponClass = 'disabled';
						switch ($oneCoupon['STATUS'])
						{
							case DiscountCouponsManager::STATUS_NOT_FOUND:
							case DiscountCouponsManager::STATUS_FREEZE:
								$couponClass = 'bad';
								break;
							case DiscountCouponsManager::STATUS_APPLYED:
								$couponClass = 'good';
								break;
						}
						?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
						if (isset($oneCoupon['CHECK_CODE_TEXT']))
						{
							echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
						}
						?></div></div><?
					}
					unset($couponClass, $oneCoupon);
				}?>
            </div>

            <div class="basket-bot clearfix">
            	<?
            	if (!$arResult['COUPON_LIST'] && $arResult['BONUSES'] > 0) {?>
                <div class="basket-bonus filter" id="bonusBlock">
                    <label for="bonus"><input type="checkbox" name="bonus" id="bonus" value="Y"><span>Потратить <?=number_format($arResult['BONUSES'], 0, '.', '')?></span> бонусов</label>
                </div>
                <?}?>
                <ul class="issued-btn">
                    <li>
                        <a href="javascript:void(0)" onclick="updateBasket();">пересчитать</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="checkOut();">оформить заказ</a>
                    </li>
                </ul>
            </div>
        </div>

	</div>
	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
<?
} else {
?>
<div class="shopping-cart-empty" id="basket_items_list"><?=GetMessage("SALE_NO_ITEMS");?></div>

<?
}
