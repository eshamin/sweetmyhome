<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult['ORDER'])) { ?>

<?

if ($_SESSION['payBonuses'] == 'Y') {
	$arFields = [];
	if ($arResult['ORDER']['PRICE'] <= $arResult['BONUSES']) {
		$arFields['SUM_PAID'] = $arResult['ORDER']['PRICE'];
		$arFields['PAYED'] = 'Y';
		$amount = $arResult['ORDER']['PRICE'];
	} else {
		$arFields['SUM_PAID'] = $arResult['BONUSES'];
		$amount = $arResult['BONUSES'];
	}

	CSaleOrder::Update($arResult['ORDER']['ID'], $arFields);
	CSaleUserAccount::Pay($arResult['ORDER']['USER_ID'], $amount, 'RUB', $arResult['ORDER']['ID'], false);
	$_SESSION['payBonuses'] = 'N';
}
?>

<section class="section">
    <div class="container">
        <h2 class="section-title">
            <span>Спасибо за заказ</span>
        </h2>
        <p class="section-desc">Работаем для Вас</p>
        <ul class="issued">
            <li>Ваш заказ успешно оформлен</li>
            <li>Ваш заказ <p>№<?=$arResult['ORDER']['ID']?></p><br> от <span><?=$arResult["ORDER"]["DATE_INSERT"]->toUserTime()->format('d.m.Y H:i')?></span> успешно создан.</li>
            <li>Наш менеджер свяжется с вами в ближайшее время для уточнения деталей заказа, способа оплаты и доставки.</li>
            <li>Спасибо, что выбрали нас!</li>
        </ul>
        <ul class="issued-btn">
            <li><a href="/personal/orders/">мои заказы</a></li>
            <li><a href="/">главная</a></li>
        </ul>
    </div>
</section>

<!--
<section class="shopping-box">
	<div class="wrapper">
        <h1>Спасибо за заказ!</h1>
        <div class="ordering_thanks__details">
            <p>Номер вашего заказа № <?=$arResult['ORDER']['ACCOUNT_NUMBER']?>.</p>
            <p>Вам отправлен e-mail и SMS с номером заказа.</p>
            <?if ($arResult['PAY_SYSTEM']['ID'] == 20) {?>
                <p>В ближайшее время наш менеджер свяжется с вами для уточнения деталей заказа.</p>
            <?}?>
        </div>
        <? if (!empty($arResult['PAY_SYSTEM'])) { ?>
            <div class="ordering_thanks__process">
                <div>Способ оплаты заказа: <b><?= $arResult["PAY_SYSTEM"]["NAME"] ?></b></div>
                <? if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0) { ?>
                    <div class="payment-info payButton2">
                        <? if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") { ?>
                            <script language="JavaScript">
                                window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ID"]))?>');
                            </script>
                            <?= GetMessage("SOA_TEMPL_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ID"])))) ?>
                            <? if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])) { ?><br/>
                                <?= GetMessage("SOA_TEMPL_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ID"])) . "&pdf=1&DOWNLOAD=Y")) ?>
                            <? }
                        } else {
                            if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
                                include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                            }
                        } ?>
                    </div>
                <? } ?>
            </div>
        <? } ?>

    </div>
</section>-->

<? } else { ?>
    <div class="order-confirm">
        <h3><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></h3>
    </div>
    <div class="order-confirm pay-system">
        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", array("#ORDER_ID#" => intval($_REQUEST['ORDER_ID']))) ?><br />
        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
    </div>
<? }?>

<?\Bitrix\Sale\DiscountCouponsManager::clear(true);?>
