<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="ordering-total">
    <p>Итого: <span><?=$arResult['ORDER_TOTAL_PRICE_FORMATED']?></span></p>
</div>
<ul class="issued-btn">
    <li><a href="<?=$arParams['PATH_TO_BASKET']?>">Назад в корзину</a></li>
    <li>
        <a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" name="ordering-submit" style="margin-left:0 !important;">завершить покупку</a>
    </li>
</ul>