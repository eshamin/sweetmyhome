<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem) {?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<?

	$arFileTmp = CFile::ResizeImageGet(
			$arItem['DETAIL_PICTURE']['ID'],
			array("width" => 80, "height" => 80),
			BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
			true, array(array("name" => "sharpen", "precision" => 30))
	);?>
	<div class="card-item clearfix" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	    <div class="card-photo" style="background-image: url('<?=$arFileTmp['src']?>');"></div>
	    <div class="card-txt">
	        <span><?=$arItem['ACTIVE_FROM']?></span>
	        <p class="card-txt-user"><?=$arItem['NAME']?></p>
	        <p class="card-txt-rew"><?=($arItem['DETAIL_TEXT']) ? $arItem['DETAIL_TEXT'] : $arItem['PREVIEW_TEXT']?></p>
	    </div>
	</div>
<?}?>



