<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$count = 0;
foreach($arResult["ITEMS"] as &$arItem) {
	if ($count == 0) {
		$arResult['DEFAULT'] = $arItem;
		$count++;
		unset($arItem);
	}
}