<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem) {?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	<?
		$date = "<span>до {$arItem['ACTIVE_TO']}</span>";

		if ($arItem['ACTIVE_TO'] < date('d.m.Y')) {
			$date = '<span class="shares-desc-complete">Акция завершена</span>';
		}

	?>

    <div class="sharesSlider-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="sharesSlider-img">
            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="sharesSlider-photo2">
        </a>
        <p><?=$arItem['NAME']?></p>
        <?=$date?>
    </div>
<?}?>



