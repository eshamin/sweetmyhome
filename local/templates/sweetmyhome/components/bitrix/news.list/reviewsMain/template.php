<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row reviews">

    
    <div class="col-md-4">
        <div class="reviews-img">
            <img src="<?=$arResult['DEFAULT']['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['DEFAULT']['NAME']?>">
        </div>
    </div>


    <div class="col-md-8 reviews-content">

<!--  
        <div class="reviews-txt">
            <p><span>“</span> <?=$arResult['DEFAULT']['PREVIEW_TEXT']?></p>
            <span><?=$arResult['DEFAULT']['NAME']?></span>
        </div>

        <div class="reviews-others">
            <p>Другие отзывы:</p>
            <div class="reviews-slider">
			<?foreach($arResult["ITEMS"] as $arItem) {?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>

				<?

				$arBouquet = CIBlockElement::GetByID($arItem['PROPERTIES']['ELEMENT']['VALUE'])->GetNext();
				$arFileTmp = CFile::ResizeImageGet(
						$arItem['PREVIEW_PICTURE']['ID'],
						array("width" => 135, "height" => 162),
						BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
						true, array(array("name" => "sharpen", "precision" => 30))
				);?>
                <div class="reviews-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <a href="<?=$arBouquet['DETAIL_PAGE_URL']?>" class="reviews-item-img" style="background-image: url('<?=$arFileTmp['src']?>');"></a>
                    <p><?=$arBouquet['NAME']?></p>
                </div>
				<?}?>
            </div>
        </div>
-->

        <div class="slider_wrapper jsSliderForNav">
            <div class="slider_for jsSliderFor">

                <div class="slider_item">
                    <div class="reviews-txt">
                        <p><span>“</span> Букет “Нежность” вызывает восторг и изумление!!!<br> Спасибо большое “Семи цветам” !</p>
                        <span>Анна Иванова, Сургут</span>
                    </div>
                </div>

                <div class="slider_item">
                    <div class="reviews-txt">
                        <p><span>“</span> Букет “Нежность” вызывает восторг и изумление!!!<br> Спасибо большое “Семи цветам” 1111111111!</p>
                        <span>Анна Иванова, Сургут</span>
                    </div>
                </div>

                <div class="slider_item">
                    <div class="reviews-txt">
                        <p><span>“</span> Букет “Нежность” вызывает восторг и изумление!!!<br> Спасибо большое “Семи цветам” 222222222222!</p>
                        <span>Анна Иванова, Сургут</span>
                    </div>
                </div>

                <div class="slider_item">
                    <div class="reviews-txt">
                        <p><span>“</span> Букет “Нежность” вызывает восторг и изумление!!!<br> Спасибо большое “Семи цветам” 333333333!</p>
                        <span>Анна Иванова, Сургут</span>
                    </div>
                </div>

                <div class="slider_item">
                    <div class="reviews-txt">
                        <p><span>“</span> Букет “Нежность” вызывает восторг и изумление!!!<br> Спасибо большое “Семи цветам” 444444444444!</p>
                        <span>Анна Иванова, Сургут</span>
                    </div>
                </div>

            </div>
            
            <div class="reviews-others">
                <div class="slider_nav jsSliderNav">

                    <div class="slider_item">
                        <div class="item_wrapper">
                            <div class="img_wrapper">
                                <div class="img_inner">
                                <img src="/local/templates/main/img/florist-galery/photo2.jpg" alt="">
                                </div>
                            </div>
                            <p>Букет “Свадьба”</p>
                        </div>
                    </div>

                    <div class="slider_item">
                        <div class="item_wrapper">
                            <div class="img_wrapper">
                                <div class="img_inner">
                                <img src="/local/templates/main/img/florist-galery/photo2.jpg" alt="">
                                </div>
                            </div>
                            <p>Букет “Свадьба1”</p>
                        </div>
                    </div>

                    <div class="slider_item">
                        <div class="item_wrapper">
                            <div class="img_wrapper">
                                <div class="img_inner">
                                <img src="/local/templates/main/img/florist-galery/photo2.jpg" alt="">
                                </div>
                            </div>
                            <p>Букет “Свадьба2”</p>
                        </div>
                    </div>

                    <div class="slider_item">
                        <div class="item_wrapper">
                            <div class="img_wrapper">
                                <div class="img_inner">
                                <img src="/local/templates/main/img/florist-galery/photo2.jpg" alt="">
                                </div>
                            </div>
                            <p>Букет “Свадьба3”</p>
                        </div>
                    </div>

                    <div class="slider_item">
                        <div class="item_wrapper">
                            <div class="img_wrapper">
                                <div class="img_inner">
                                <img src="/local/templates/main/img/florist-galery/photo2.jpg" alt="">
                                </div>
                            </div>
                            <p>Букет “Свадьба4”</p>
                        </div>
                    </div>

                </div>
            </div>
            
        </div>

        
    </div>



</div>


