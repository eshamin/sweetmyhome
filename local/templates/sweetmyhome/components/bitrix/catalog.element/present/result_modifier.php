<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

if ($arResult) {
	$arSets = CCatalogProductSet::getAllSetsByProduct($arResult['ID'], CCatalogProductSet::TYPE_SET);

	foreach ($arSets as $arSet) {
		foreach ($arSet['ITEMS'] as $arItem) {
			$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();

			if (!$arElement['IBLOCK_SECTION_ID']) {
				$arResult['SET']['FLOWERS'][] = "{$arElement['NAME']} - {$arItem['QUANTITY']} шт.";
			} elseif ($arElement['IBLOCK_SECTION_ID'] == 7) {
				$arResult['SET']['HERBAGE'][] = "{$arElement['NAME']} - {$arItem['QUANTITY']} шт.";
			} elseif($arElement['IBLOCK_SECTION_ID'] == 6) {
				$arResult['SET']['PACK'][] = "{$arElement['NAME']} - {$arItem['QUANTITY']} шт.";
			} elseif ($arElement['IBLOCK_SECTION_ID'] == 5) {
				$arResult['SET']['ACCESSORIES'][] = "{$arElement['NAME']} - {$arItem['QUANTITY']} шт.";
			}
		}
	}
}

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();