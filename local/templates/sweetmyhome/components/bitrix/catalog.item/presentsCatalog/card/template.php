<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="addItem-item">
	<div class="addItem-top">
	    <div class="addItem-img" id="<?=$itemIds['PICT']?>">
	        <img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="">
	    </div>
	    <div class="addItem-desc">
	        <h5><?=$item['NAME']?></h5>
	        <p><span id="<?=$itemIds['PRICE']?>"><?=$price['RATIO_PRICE']?></span> руб./шт.</p>
	        <div class="spinner-global" data-entity="quantity-block">
	            <p class="spin-down" id="<?=$itemIds['QUANTITY_DOWN']?>">-</p>
	            <input type="text" value="<?=$price['MIN_QUANTITY']?>" id="<?=$itemIds['QUANTITY']?>">
	            <p class="spin-up" id="<?=$itemIds['QUANTITY_UP']?>">+</p>
	        </div>
	    </div>
	</div>
	<div id="<?=$itemIds['BASKET_ACTIONS']?>">
	<a href="javascript:void(0);" data-productName="<?=$item['NAME']?>" class="addPresent" id="<?=$itemIds['BUY_LINK']?>">добавить в букет</a>
	</div>
</div>