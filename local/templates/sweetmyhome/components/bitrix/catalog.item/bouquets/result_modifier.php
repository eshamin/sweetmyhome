<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if ($arResult['ITEM']) {
	$arSets = CCatalogProductSet::getAllSetsByProduct($arResult['ITEM']['ID'], CCatalogProductSet::TYPE_SET);

	foreach ($arSets as $arSet) {
		foreach ($arSet['ITEMS'] as $arItem) {
			$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();
			$parentCategory = CIBlockSection::GetNavChain(false, $arElement['IBLOCK_SECTION_ID'])->Fetch();
			if ($parentCategory['ID'] == 2) {
				$arResult['ITEM']['SET'][] = "<span>{$arElement['NAME']}</span> - <span>{$arItem['QUANTITY']} шт.</span>";
			}
		}
	}
}
?>