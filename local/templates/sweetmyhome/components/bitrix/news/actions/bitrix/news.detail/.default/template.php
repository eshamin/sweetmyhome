<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
	$date = "<span>до {$arItem['ACTIVE_TO']}</span>";

	if ($arItem['ACTIVE_TO'] < date('d.m.Y')) {
		$date = '<span class="shares-desc-complete">Акция завершена</span>';
	}

?>
<div class="shares-item clearfix select">
    <a  href="javascript:void(0)" class="shares-img">
        <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="shares-photo1">
    </a>
    <div class="shares-desc">
        <h6><?=$arResult['NAME']?></h6>
        <?=$date?>
        <?=$arResult['DETAIL_TEXT']?>
    </div>
</div>
		<?/*
		$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
				"HANDLERS" => $arParams["SHARE_HANDLERS"],
				"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
				"PAGE_TITLE" => $arResult["~NAME"],
				"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
				"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
				"HIDE" => $arParams["SHARE_HIDE"],
			),
			$component,
			array("HIDE_ICONS" => "N")
		);*/
		?>
<div class="shares-bot">
	<a  href="/actions/" class="shares-bot-back">вернуться</a>
	<div class="shares-left">
		<p>ПОДЕЛИТЬСЯ С ДРУЗЬЯМИ:</p>
		<ul class="sharesSocial">
			<li><a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
		</ul>
	</div>
</div>