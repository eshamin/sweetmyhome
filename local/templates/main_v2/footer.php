    <div class="fixed-panel" id="fixed-panel">
        <div class="panel-wrapper">
<!--            <div class="controls-left">-->
<!--                <div class="controls-list">-->
<!--                    <li>-->
<!--                        <a class="btn btn-primary"><span class="ico ico-letter"></span>Подписаться на новости</a>-->
<!--                    </li>-->
<!--                </div>-->
<!--            </div>-->
            <div class="controls-right">
                <ul class="controls-list">

                    <li class="active">
                        <a href="/favorites">
                            <span class="ico ico-favorite"></span>
                            <span class="underline">Избранное</span>
                            <span class="counter" id="quantityFavorites"><?=(is_array(getFavorites())) ? count(getFavorites()) : 0?></span>
                        </a>
                    </li>

                    <li class="active2">
                        <?$APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "footerMenu", Array(
                            "POSITION_FIXED" => "N",	// Отображать список сравнения поверх страницы
                            "AJAX_MODE" => "N",	// Включить режим AJAX
                            "IBLOCK_TYPE" => "catalog",	// Тип инфоблока
                            "IBLOCK_ID" => "5",	// Инфоблок
                            "DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
                            "COMPARE_URL" => "/compare/",	// URL страницы с таблицей сравнения
                            "NAME" => "CATALOG_COMPARE_LIST",	// Уникальное имя для списка сравнения
                            "ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
                            "PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                            "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                        ),
                            false
                        );?>
                    </li>

                    <li class="active3">
                        <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "smallCart", Array(
                            "PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
                            "PATH_TO_ORDER" => "/personal/order/",	// Страница оформления заказа
                            "SHOW_DELAY" => "N",	// Показывать отложенные товары
                            "SHOW_NOTAVAIL" => "N",	// Показывать товары, недоступные для покупки
                            "SHOW_SUBSCRIBE" => "N",	// Показывать товары, на которые подписан покупатель
                        ),
                            false
                        );?>
                    </li>
                </ul>

                <a href="/personal/cart" class="btn btn-y">ОФОРМИТЬ ЗАКАЗ</a>
            </div>
        </div>
    </div>

    </div>
    <div class="footer">
        <div class="clear"></div>
        <div class="footer-wrapper">
            <div class="footer-block">
                <?$APPLICATION->IncludeComponent("bitrix:menu", "bottomMenu", Array(
                    "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                    false
                );?>
                <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/social.php')?>
            </div>
            <div class="clear"></div>
            <div class="footer-block">
                <div class="copyright">
                    <?=date('Y')?> © SweetMyHome. Все права защищены. Все данные, представленные на сайте, носят сугубо информационный характер и не являются исчерпывающими. Для более подробной информации следует обращаться к менеджерам компании по указанным на сайте телефонам. Вся представленная на сайте информация, касающаяся комплектации, технических характеристик, цветовых сочетаний, а также стоимости продукции, носит информационный характер и ни при каких условиях не является публичной офертой, определяемой положениями пункта 2 статьи 437 Гражданского Кодекса Российской Федерации. Указанные цены являются рекомендованными и могут отличаться от действительных цен.
                </div>
            </div>
        </div>
    </div>
    <script type="text/x-template" id="your-city-list">

    </script>
    </body>
</html>