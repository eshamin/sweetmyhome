<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
    ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0) {
    ?>
    <div id="basket_items_list">
    <div class="bx_ordercart_order_table_container">
        <div class="b-basket__inner-wrapper">
            <table class="b-basket" id="basket_items">
                <thead>
                <tr>
                    <th class="b-basket__header" colspan="2"><h1>Корзина</h1></th>
                    <th class="b-basket__price"><p>Цена</p></th>
                    <th class="b-basket__qty"><p>Количество</p></th>
                    <th class="b-basket__summ"><p>Сумма</p></th>
                </tr>
                </thead>

                <tbody>
                <?
                $totalCount = 0;
                foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $k => $arItem) {
                    $totalCount += $arItem['QUANTITY'];
                    ?>
                    <tr id="<?= $arItem["ID"] ?>">
                        <td class="b-basket__img">
                            <?
                            if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0) {
                                $url = $arItem["PREVIEW_PICTURE_SRC"];
                            } elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0) {
                                $url = $arItem["DETAIL_PICTURE_SRC"];
                            } else {
                                $url = $templateFolder . "/images/no_photo.png";
                            }
                            ?>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$url?>" alt=""/></a>
                        </td>
                        <td class="b-basket__name">
                            <h4><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h4>
                            <p>
                                <?if ($arItem['PROPERTY_SIZE1_VALUE'] || $arItem['PROPERTY_SIZE3_VALUE'] || $arItem['PROPERTY_SIZE3_VALUE']) {?>
                                    Длина: <?=$arItem['PROPERTY_SIZE1_VALUE']?>, Глубина: <?=$arItem['PROPERTY_SIZE3_VALUE']?>, Высота: <?=$arItem['PROPERTY_SIZE2_VALUE']?><img src="<?=SITE_TEMPLATE_PATH?>/img/basket-delimiter.png" alt=""/>
                                <?}?>
                                <? if ($arItem['PROPERTY_COLORTEXT_VALUE']) {?>
                                    Цвет: <?=$arItem['PROPERTY_COLORTEXT_VALUE']?><img src="<?=SITE_TEMPLATE_PATH?>/img/basket-delimiter.png" alt=""/>
                                <?}?>
                                <? if ($arItem['PROPERTY_SER_VALUE']) {?>
                                    Серия: <?=$arItem['PROPERTY_SER_VALUE']?><img src="<?=SITE_TEMPLATE_PATH?>/img/basket-delimiter.png" alt=""/>
                                <?}?>
                                <? if ($arItem['PROPERTY_SLEEPSIZE_VALUE']) {?>
                                    Спальное место: <?=$arItem['PROPERTY_SLEEPSIZE_VALUE']?> см<img src="<?=SITE_TEMPLATE_PATH?>/img/basket-delimiter.png" alt=""/>
                                <?}?>
                                <? if ($arItem['PROPERTY_MATERIAL_VALUE']) {?>
                                    Материал: <?=$arItem['PROPERTY_MATERIAL_VALUE']?><img src="<?=SITE_TEMPLATE_PATH?>/img/basket-delimiter.png" alt=""/>
                                <?}?>
                                <? if ($arItem['PROPERTY_WEIGHT_VALUE']) {?>
                                    Вес: <?=$arItem['PROPERTY_WEIGHT_VALUE']?> кг
                                <?}?>
                            </p>
                            <p class="b-basket__name-artikul">
                                <span>Артикул: <?=$arItem['ID']?></span>
                                <?
                                $resComments = CIBlockSection::GetList(
                                    array('DATE_CREATED' => 'DESC'),
                                    array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arItem['ID']),
                                    false,
                                    false,
                                    array('PROPERTY_RATING'));
                                $totalRating = 0;
                                $count = 0;
                                while ($comment = $resComments->Fetch()) {
                                    $totalRating += $comment['PROPERTY_RATING_VALUE'];
                                    $count++;
                                }

                                if ($count > 0) {
                                    $totalRating = floor($totalRating / $count);
                                }
                                ?>
                                <?if ($totalRating > 0) {?>
                                    <span class="b-top-panel__feedback-stars">
                                    <?for ($i = 0; $i < $totalRating; $i++) {?>
                                        <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                    <?}?>
                                        <?for ($i = $totalRating; $i < 5; $i++) {?>
                                            <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                        <?}?>
                                    </span>
                                <?}?>
                            </p>
                        </td>
                        <td class="b-basket__price"><span><?=$arItem['PRICE_FORMATED']?></span></td>
                        <td class="b-basket__qty">
                            <div class="js-digital-spinner">
                                <?
                                $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"" . $arItem["AVAILABLE_QUANTITY"] . "\"" : "";
                                $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                                ?>
                                <span class="js-digital-spinner-down" onclick="setQuantity(<?= $arItem["ID"] ?>, '1', 'down', <?=$useFloatQuantityJS ?>);">-</span>
                                <input
                                    type="text"
                                    size="3"
                                    class="onlyDigits"
                                    id="QUANTITY_INPUT_<?=$arItem["ID"] ?>"
                                    name="QUANTITY_INPUT_<?=$arItem["ID"] ?>"
                                    size="2"
                                    maxlength="18"
                                    min="0"
                                    <?= $max ?>
                                    step="1"
                                    style="max-width: 50px"
                                    value="<?=$arItem["QUANTITY"] ?>"
                                    onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"] ?>', '<?=$arItem["ID"] ?>', '1', <?=$useFloatQuantityJS ?>)"
                                    >
                                <span class="js-digital-spinner-up" onclick="setQuantity(<?=$arItem["ID"] ?>, '1', 'up', <?=$useFloatQuantityJS ?>);">+</span>
                            </div>
                            <input type="hidden" id="QUANTITY_<?=$arItem['ID'] ?>" name="QUANTITY_<?=$arItem['ID'] ?>" value="<?=$arItem["QUANTITY"]?>"/>
                        </td>
                        <td class="b-basket__summ">
                            <span id="current_price_<?=$arItem["ID"]?>"><?=$arItem['SUM']?></span>
                            <p class="b-basket__summ-more">
                                <?if ($arItem['PROPERTY_STOCK_VALUE'] == 'Y') {?>
                                    <span>в наличии</span><br />
                                <?}?>
                                <span>Доставка: <?=$arItem['PROPERTY_YMSTOIMOSTDOSTAVKI_VALUE']?> руб.,</span><br />
                                <span>возможен самовывоз</span>
                            </p>
                            <p class="b-basket__summ-remove">
                                <a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" class="deleteFromBasket" item-id="<?=$arItem["ID"]?>"><span>Удалить</span></a>
                            </p>
                        </td>
                    </tr>
                <?
                }
                ?>
                <tr class="b-basket__summary">
                    <td colspan="2" class="b-basket__summary-summary">Итого: <b class="b-basket__summary-qty" id="totalCount"><?=getNumEnding($totalCount, Array('товар', 'товара', 'товаров'))?></b> на сумму <b class="b-basket__summary-summ" id="allSum_FORMATED"><?=$arResult['allSum_FORMATED']?></b></td>
                    <td colspan="3" class="b-basket__summary-order">
                        <a href="javascript:void(0)" class="b-basket__summary-promo">У меня есть промокод | Дисконтная карта</a>
                        <a class="js-button js-button-yellow" onclick="checkOut();" href="/personal/order/make">Оформить заказ</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <p class="b-basket-post-info">
                После оформления заказа с вами в ближайшее время свяжется менеджер для уточнения деталей заказа
            </p>
        </div>
        <input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
        <input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
        <input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
        <input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
        <input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />

        <div class="bx_ordercart_order_pay">

            <div class="bx_ordercart_order_pay_left" id="coupons_block">
                <?
                if ($arParams["HIDE_COUPON"] != "Y")
                {
                    ?>
                    <div class="bx_ordercart_coupon">
                    <span><?=GetMessage("STB_COUPON_PROMT")?></span><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">
                    </div><?
                    if (!empty($arResult['COUPON_LIST']))
                    {
                        foreach ($arResult['COUPON_LIST'] as $oneCoupon)
                        {
                            $couponClass = 'disabled';
                            switch ($oneCoupon['STATUS'])
                            {
                                case DiscountCouponsManager::STATUS_NOT_FOUND:
                                case DiscountCouponsManager::STATUS_FREEZE:
                                    $couponClass = 'bad';
                                    break;
                                case DiscountCouponsManager::STATUS_APPLYED:
                                    $couponClass = 'good';
                                    break;
                            }
                            ?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
                                if (isset($oneCoupon['CHECK_CODE_TEXT']))
                                {
                                    echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
                                }
                                ?></div></div><?
                        }
                        unset($couponClass, $oneCoupon);
                    }
                }
                else
                {
                    ?>&nbsp;<?
                }
                ?>
            </div>
        </div>
    </div>
<?
} else {
    ?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td colspan="<?= $numCells ?>" style="text-align:center">
					<div class=""><?= GetMessage("SALE_NO_ITEMS"); ?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
}
?>
<script type="text/x-template" id="remove-from-basket">
    <div class="feedback-thank">
        <h2>Удалить товар из корзины?</h2>
        <p class="muted">Вот те раз! Все ведь шло так хорошо, вы же почти его купили, как же так((</p>
        <a class="fl_l js-button js-button-white js-basket-back" href="javascript:void(0)">Вернуться в корзину</a>
        <a class="fl_r js-button js-button-yellow js-basket-remove" href="javascript:void(0)">Удалить товар</a>
    </div>
</script>