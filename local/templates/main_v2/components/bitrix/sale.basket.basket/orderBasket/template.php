<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<table class="b-order__created">
    <tbody>
    <?foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $k => $arItem) {?>
    <tr>
        <?
        if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0) {
            $url = $arItem["PREVIEW_PICTURE_SRC"];
        } elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0) {
            $url = $arItem["DETAIL_PICTURE_SRC"];
        } else {
            $url = $templateFolder . "/images/no_photo.png";
        }
        ?>
        <td class="b-order__created-img"><img src="<?=$url?>" alt=""/></td>
        <td class="b-order__created-name" colspan="2">
            <h4><?=$arItem['NAME']?></h4>
            <span><?=$arItem['SUM']?></span> <?=$arItem["QUANTITY"]?> шт. × <?=$arItem['PRICE_FORMATED']?>
        </td>
    </tr>
    <?}?>
    </tbody>
</table>
