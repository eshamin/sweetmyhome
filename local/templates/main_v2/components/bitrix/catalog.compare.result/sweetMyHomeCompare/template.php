<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");

$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME']
);

?><div class="bx_compare <? echo $templateData['TEMPLATE_CLASS']; ?>" id="bx_catalog_compare_block"><?
if ($isAjax)
{
	$APPLICATION->RestartBuffer();
}

$arResult["ITEMS"] = array_slice($arResult["ITEMS"], 0, 4);
?>

<!--<ul class="b-products__list" id="products-compare">-->
<div class="bx_content_section">
    <li class="b-products__item"></li>
<?
foreach($arResult["ITEMS"] as &$arItem) {?>
    <div class="catalogGoodsBlock active blocks b-products__list l-lines-container_box" data-item-minwidth="230">
<!--    <li class="b-product b-product_tale b-box b-page_theme_normal b-box_w240 b-box_h415 b-box_hoverable double-hover-wrap l-line-item js-fly">-->
<!--    <li class="b-products__item">-->
        <section class="b-product b-product_tale b-box b-page_theme_normal b-box_w240 b-box_h415 b-box_hoverable double-hover-wrap l-line-item js-fly class">
            <div class="b-box__h">
                <?
                $arFileTmp = CFile::ResizeImageGet(
                    $arItem['DETAIL_PICTURE'],
                    array("width" => 235, "height" => 148),
                    BX_RESIZE_IMAGE_EXACT,
                    true,
                    false
                );
                ?>
                <a target="_blank" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"
                   class="must_be_href double-hover js-gtm-product-click">
                        <span class="b-img2 b-img2_size_lg b-product__img center-block">
                            <img src="<?= $arFileTmp['src'] ?>" class="b-img2__img">
                        </span>
                </a>

                <?
                $resComments = CIBlockSection::GetList(
                    array('DATE_CREATED' => 'DESC'),
                    array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arItem['ID']),
                    false,
                    false,
                    array('PROPERTY_RATING'));
                $totalRating = 0;
                $totalCount = 0;
                while ($comment = $resComments->Fetch()) {
                    $totalRating += $comment['PROPERTY_RATING_VALUE'];
                    $totalCount++;
                }

                if ($totalCount > 0) {
                    $totalRating = floor($totalRating / $totalCount);
                }
                ?>
                <div class="b-stars-wrap b-product__stars-wrap g-clickable pull-left _small">
                    <?if ($totalRating > 0) { ?>
                        <div class="b-small-stars _s<?= $totalRating ?> g-clickable js-qtip-click-handle">
                            <i class="s1"></i>
                            <i class="s2"></i>
                            <i class="s3"></i>
                            <i class="s4"></i>
                            <i class="s5"></i>
                        </div>
                        <span class="pseudolink count"><?= $totalCount; ?></span>
                    <?
                    } ?>
                    <a href="javascript: void(0)" class="btn add-to-favorites" item-id="<?=$arItem['ID']?>" title="Добавить в избранное">
                        <span class="ico ico-favorite"></span>
                    </a>
                </div>
                <div class="b-product__title">
                    <a target="_blank" href="<?=$arItem['DETAIL_PAGE_URL']?>"
                       class="must_be_href js-gtm-product-click">
                        <?= (strlen($arItem['NAME']) > 90) ? substr($arItem['NAME'], 0, 90) : $arItem['NAME'] ?>
                    </a>
                    <?if ($arItem['PROPERTIES']['SIZE1']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE'] || $arItem['PROPERTIES']['SIZE3']['VALUE']) { ?>
                        <p class="muted">Длина <?= $arItem['PROPERTIES']['SIZE1']['VALUE'] ?>    •    Глубина <?= $arItem['PROPERTIES']['SIZE3']['VALUE'] ?>    •    Высота <?= $arItem['PROPERTIES']['SIZE2']['VALUE'] ?></p>
                    <?
                    }
                    ?>
                </div>
            </div>
            <div class="b-box__b">
                <?if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                    <div class="b-product__price">
                        <span class="b-price b-price_theme_normal b-price_size4">
                            <span class="b-price__num"><?=$arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></span>
                            <span class="b-price__sign"></span>
                        </span>
                        <del class="b-price b-price_theme_normal b-price_old">
                            <span class="b-price__num"><?=$arItem['MIN_PRICE']['PRINT_VALUE'] ?></span>
                            <span class="b-price__sign"></span>
                        </del>
                    </div>
                <?} else {?>
                    <div class="b-product__price">
                        <span class="b-price b-price_theme_normal b-price_size4">
                            <span class="b-price__num"><?= $arItem['MIN_PRICE']['PRINT_VALUE'] ?></span>
                            <span class="b-price__sign"></span>
                        </span>
                    </div>
                <?}?>
                <div
                    class="btn-group  btn-group_theme_normal btn-group-btnleft b-product__actions js-rutarget-actions">
                    <a class="btn btn-y add2basket" item-id="<?= $arItem['ID'] ?>" href="<?= $arItem['ADD_URL'] ?>">Купить</a>
                </div>
                <div class="b-product__group">
                    <div
                        class="b-product-status b-product-status_theme_normal b-product-status_instock b-product__status pull-right">
                                <span class="b-pseudolink js-qtip-click-handle js-avail-click">
                                    <?if ($arItem['CAN_BUY']) { ?>В наличии<?
                                    } ?>
                                </span>
                    </div>
                </div>
            </div>
            <div class="b-label-group b-label-group_size_xs b-product__label2-group">
                <span class="b-label b-label_theme_normal b-label_size_xs b-label_info">Выгодная покупка</span>
                <span class="b-label b-label_theme_normal b-label_size_xs b-label_info">Акция!</span>
                <?if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']) { ?>
                    <span class="b-label b-label_theme_normal b-label_size_xs b-label_info">Суперцена</span>
                <?
                } ?>
            </div>
        </section>

        <!--<a onclick="CatalogCompareObj.MakeAjaxAction('<?=CUtil::JSEscape($arElement['~DELETE_URL'])?>');" href="javascript:void(0)"><?=GetMessage("CATALOG_REMOVE_PRODUCT")?></a>-->
    </div>
<?}?>
</div>

    <?
    if (!empty($arResult["SHOW_PROPERTIES"])) {
        foreach ($arResult["SHOW_PROPERTIES"] as $code => $arProperty)
        {
            $showRow = true;
            if ($arResult['DIFFERENT'])
            {
                $arCompare = array();
                foreach($arResult["ITEMS"] as &$arElement)
                {
                    $arPropertyValue = $arElement["DISPLAY_PROPERTIES"][$code]["VALUE"];
                    if (is_array($arPropertyValue))
                    {
                        sort($arPropertyValue);
                        $arPropertyValue = implode(" / ", $arPropertyValue);
                    }
                    $arCompare[] = $arPropertyValue;
                }
                unset($arElement);
                $showRow = (count(array_unique($arCompare)) > 1);
            }

            if ($showRow)
            {
                $arProperty["NAME"];
                echo (is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]);
            }
        }
    }
    ?>
<?
if ($isAjax)
{
	die();
}
?>
</div>
<script type="text/javascript">
	var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
</script>

<div id="good-added" style="display: none">
    <div class="good-added">
        <h2>Товар добавлен в корзину</h2>
        <table class="b-basket">
            <tbody>
            <tr>
                <td class="b-basket__img" id="item_img"><img src="" alt="" /></td>
                <td class="b-basket__name">
                    <h4 id="item_name"></h4>
                </td>
                <td class="b-basket__price"><span id="item_discount_price"></span></td>
                <td class="b-basket__summ" style="text-align: center">
                    <span id="item_count"></span> <span> шт.</span>
                </td>
                <td class="b-basket__summ">
                    <span id="item_price"></span>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="good-added__elems">
            <a  class="js-button js-button-yellow left" href="/personal/cart">Перейти в корзину</a>
            <a class="left" href="javascript:void(0)">или продолжить покупки</a>
            <p>Всего <span class="quantityTotalBasket"></span> на сумму <span class="priceTotalBasket"></span></p>
            <div class="clear"></div>
        </div>
    </div>
</div>