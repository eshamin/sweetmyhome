<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arElements = $APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "",
    Array(
        "RESTART" => $arParams["RESTART"],
        "NO_WORD_LOGIC" => $arParams["NO_WORD_LOGIC"],
        "USE_LANGUAGE_GUESS" => $arParams["USE_LANGUAGE_GUESS"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "arrFILTER" => array("iblock_".$arParams["IBLOCK_TYPE"]),
        "arrFILTER_iblock_".$arParams["IBLOCK_TYPE"] => array($arParams["IBLOCK_ID"]),
        "USE_TITLE_RANK" => "N",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "arrFilter",
        "SHOW_WHERE" => "N",
        "arrWHERE" => array(),
        "SHOW_WHEN" => "N",
        "PAGE_RESULT_COUNT" => 5000000,
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "N",
    ),
    $component,
    array('HIDE_ICONS' => 'Y')
);

if (!empty($arElements) && is_array($arElements))
{
    global $arrFilter;
    $arrFilter = array(
        "=ID" => array_values($arElements),
    );
?>

<div class="workarea b-catalog">
    <div class="bx_sitemap">
        <h1 class="bx_sitemap_title">Результаты поиска (<?=count($arElements)?>)</h1>
    </div>
    <div class="b-catalog__sidebar g-left">
        <nav class="b-catalog__sidebar-list">
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "searchPage",
                array(
                    "IBLOCK_TYPE" => 'catalog',
                    "IBLOCK_ID" => CATALOG_IB,
                    "SECTION_ID" => $_REQUEST['where'],
                    "CACHE_TYPE" => 'A',
                    "CACHE_TIME" => 3600,
                    "CACHE_GROUPS" => 'N',
                    "COUNT_ELEMENTS" => 'Y',
                    "TOP_DEPTH" => 1,
                    "SECTION_URL" => '/catalog/#SECTION_CODE#/',
                    "VIEW_MODE" => 'LIST',
                    "SHOW_PARENT_NAME" => 'Y',
                    "HIDE_SECTION_NAME" => 'N',
                    "ADD_SECTIONS_CHAIN" => ''
                ),
                $component,
                array("HIDE_ICONS" => "Y")
            );?>
        </nav>
        <div class="b-catalog__sidebar-filters">
            <div class="bx_filter_section">
                <?
                    $APPLICATION->IncludeComponent(
                        "bitrix:catalog.smart.filter",
                        "artopt",
                        array(
                            "IBLOCK_TYPE" => 'catalog',
                            "IBLOCK_ID" => CATALOG_IB,
                            "SECTION_ID" => $_REQUEST['where'],
                            "FILTER_NAME" => 'arrFilter',
                            "PRICE_CODE" => array('BASE'),
                            "CACHE_TYPE" => 'A',
                            "CACHE_TIME" => 3600,
                            "CACHE_GROUPS" => 'N',
                            "SAVE_IN_SESSION" => "N",
                            "FILTER_VIEW_MODE" => 'VERTICAL',
                            "XML_EXPORT" => "Y",
                            "SECTION_TITLE" => "NAME",
                            "SECTION_DESCRIPTION" => "DESCRIPTION",
                            'HIDE_NOT_AVAILABLE' => 'Y',
                            "TEMPLATE_THEME" => 'blue'
                        ),
                        $component,
                        array('HIDE_ICONS' => 'Y')
                    );
                ?>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>


    <div class="b-catalog__section">
        <nav class="b-catalog__section-navbar b-content-switchers">
            <div class="b-content-switchers--left"></div>
            <div class="b-content-switchers--right">
                <span class="b-content-switchers__control-label">Вид каталога</span>
                <div class="b-content-switchers__btn-group">
                    <button class="btn switch_view blocks active" title="Плиткой">
                        <div class="icon cells">
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                        </div>
                    </button>
                    <button class="btn switch_view lines" title="Список">
                        <div class="icon photo-list">
                            <div class="dot"></div>
                            <div class="line"></div>
                            <div class="dot"></div>
                            <div class="line"></div>
                            <div class="dot"></div>
                            <div class="line"></div>
                        </div>
                    </button>
                </div>
            </div>
        </nav>
<?

	$APPLICATION->IncludeComponent(
		"bitrix:catalog.section",
		"searchPage",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
			"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
			"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
			"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
			"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
			"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
			"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
			"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
			"OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
			"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
			"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
			"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
			"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
			"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
			"OFFERS_LIMIT" => $arParams["OFFERS_LIMIT"],
			"SECTION_URL" => $arParams["SECTION_URL"],
			"DETAIL_URL" => $arParams["DETAIL_URL"],
			"BASKET_URL" => $arParams["BASKET_URL"],
			"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
			"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
			"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => $arParams["CACHE_TIME"],
			"DISPLAY_COMPARE" => $arParams["DISPLAY_COMPARE"],
			"PRICE_CODE" => $arParams["PRICE_CODE"],
			"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
			"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
			"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
			"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
			"USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
			"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
			"CURRENCY_ID" => $arParams["CURRENCY_ID"],
			"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
			"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
			"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
			"PAGER_TITLE" => $arParams["PAGER_TITLE"],
			"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
			"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
			"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
			"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
			"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
			"FILTER_NAME" => "arrFilter",
			"SECTION_ID" => "",
			"SECTION_CODE" => "",
			"SECTION_USER_FIELDS" => array(),
			"INCLUDE_SUBSECTIONS" => "Y",
			"SHOW_ALL_WO_SECTION" => "Y",
			"META_KEYWORDS" => "",
			"META_DESCRIPTION" => "",
			"BROWSER_TITLE" => "",
			"ADD_SECTIONS_CHAIN" => "N",
			"SET_TITLE" => "N",
			"SET_STATUS_404" => "N",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "N",
		),
		$arResult["THEME_COMPONENT"],
		array('HIDE_ICONS' => 'Y')
	);
    ?>
    <div class="g-clear"></div>
    <?
}
else
{
	echo GetMessage("CT_BCSE_NOT_FOUND");
}
?>