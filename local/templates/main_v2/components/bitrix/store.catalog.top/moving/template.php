<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="list_carousel">

<?
foreach($arResult["ROWS"] as $arItems):
?>

<ul id="foo2">

<?

	foreach($arItems as $key => $arElement):
?>
			
<?
      
		if(is_array($arElement)):
			$bPicture = is_array($arElement["PREVIEW_IMG"]); ?>
					<li>
					 <a href="<? echo $arElement["DETAIL_PAGE_URL"]; ?>">	
			<? if ($bPicture):
?>
					<div class="images">
           <span class="img" style="background: url('<?=$arElement["DETAIL_PICTURE"]["SRC"]?>') center center no-repeat;"></span>
           <span class="img_overlay"></span>
					</div>
<?
			endif;
?>                        
<?
			if(count($arElement["PRICES"])>0 && $arElement['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] == $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE']):
				foreach($arElement["PRICES"] as $code=>$arPrice):
					if($arPrice["CAN_ACCESS"]):
?>
						
<?
								if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):
?>
                                <span class="price"><?= $arPrice["PRINT_DISCOUNT_VALUE"] ?></span>
                                <span class="old_price"><?= $arPrice["PRINT_VALUE"] ?></span>
<?
								else:
?>
								<span class="price"><?= $arPrice["PRINT_VALUE"] ?></span>
<?
							endif;
?>							
<?
					endif;
				endforeach;
			else:
				$price_from = '';
				if($arElement['PROPERTIES']['MAXIMUM_PRICE']['VALUE'] > $arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'])
				{
					$price_from = GetMessage("CR_PRICE_OT");	
				}
				CModule::IncludeModule("sale")
?>
				<p class="item-price"><span><?=$price_from?><?=FormatCurrency($arElement['PROPERTIES']['MINIMUM_PRICE']['VALUE'], CSaleLang::GetLangCurrency(SITE_ID))?></span></p>
<?
			endif;
?>
                            <span class="tittle">
                            <?
                            $name = $arElement["NAME"];
                            echo $name;
                            ?>
                            </span>
            </a>
					</li>
	
<?
		endif;
?>
<?
	endforeach;
?>

</ul>
                    <div class="clearfix"></div>
                    <a id="prev2" class="prev" href="#"></a>
                    <a id="next2" class="next" href="#"></a>
<?
endforeach;
?>
</div>