<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<ul class="slides">
<?foreach($arResult["ITEMS"] as $arItem) {
	if (!$arItem['PREVIEW_PICTURE']['SRC']) {
        continue;
    }
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
		    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
		    <span class="flex-caption">
			    <p>
				    <?=$arItem['PREVIEW_TEXT']?>
			    </p>
		    </span>
        </a>
	</li>
<?}?>
</ul>
