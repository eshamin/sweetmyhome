<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="grid">
    <div class="grid-sizer"></div>
    <div class="grid-item min-height">
        <a href="<?=$arResult['ITEMS'][0]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][0]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][0]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
        <a href="<?=$arResult['ITEMS'][1]['PROPERTIES']['LINK']['VALUE']?>" class="sub first">
            <img class="image" src="<?=$arResult['ITEMS'][1]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
            <span><?=$arResult['ITEMS'][1]['NAME']?></span>
        </a>
        <a href="<?=$arResult['ITEMS'][2]['PROPERTIES']['LINK']['VALUE']?>" class="sub">
            <img class="image" src="<?=$arResult['ITEMS'][2]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
            <span><?=$arResult['ITEMS'][2]['NAME']?></span>
        </a>
    </div>
    <div class="grid-item">
        <a href="<?=$arResult['ITEMS'][3]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][3]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][3]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
    </div>
    <div class="grid-item grid-item--width2">
        <a href="<?=$arResult['ITEMS'][4]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][4]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][4]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
    </div>
    <div class="grid-item">
        <a href="<?=$arResult['ITEMS'][5]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][5]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][5]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
    </div>

    <div class="grid-item">
        <a href="<?=$arResult['ITEMS'][6]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][6]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][6]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
    </div>
    <div class="grid-item max-height">
        <a href="<?=$arResult['ITEMS'][7]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][7]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][7]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
    </div>
    <div class="grid-item">
        <a href="<?=$arResult['ITEMS'][8]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][8]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][8]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
        <a href="<?=$arResult['ITEMS'][9]['PROPERTIES']['LINK']['VALUE']?>" class="sub first">
            <img class="image" src="<?=$arResult['ITEMS'][9]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
            <span><?=$arResult['ITEMS'][9]['NAME']?></span>
        </a>
        <a href="<?=$arResult['ITEMS'][10]['PROPERTIES']['LINK']['VALUE']?>" class="sub">
            <img class="image" src="<?=$arResult['ITEMS'][10]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
            <span><?=$arResult['ITEMS'][10]['NAME']?></span>
        </a>
    </div>
    <div class="grid-item">
        <a href="<?=$arResult['ITEMS'][11]['PROPERTIES']['LINK']['VALUE']?>">
            <span><?=$arResult['ITEMS'][11]['NAME']?></span>
            <img class="image" src="<?=$arResult['ITEMS'][11]['PREVIEW_PICTURE']['SRC']?>" alt="" title="">
        </a>
    </div>
</div>
