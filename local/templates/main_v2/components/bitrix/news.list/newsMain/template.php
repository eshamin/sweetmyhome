<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<ul class="b-news">
<?foreach($arResult["ITEMS"] as $arItem) {

    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <li class="b-news__item g-left" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="b-news__item-int">
            <p class="b-news__item-date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></p>
            <h3 class="b-news__item-h2"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="b-news__item-link"><?=$arItem['NAME']?></a></h3>
            <p class="b-news__item-text">
                <?=$arItem['PREVIEW_TEXT']?>
            </p>
        </div>
    </li>
<?}?>
    <li class="b-news__subscribe g-left">
        <div class="b-news__subscribe-int">
            <div class="g-left b-news__subscribe-img"></div>
            <div class="b-news__subscribe-header">
                <h4>Подписывайтесь</h4>
                <p>Узнавайте свежую информацию о скидках и акциях первым</p>
            </div>
            <div class="b-news__subscribe-form g-clear">
                <form action="#">
                    <input type="text" name="user-subscribe-mail" class="b-subscribe-form__mail" placceholder="Оставьте свой e-mail">
                    <input type="submit" class="b-subscribe-form__submit" value="Подписаться">
                </form>
            </div>
        </div>
    </li>
    <li class="g-clear"></li>
</ul>