<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <ul class="footer-menu">

    <?foreach($arResult as $arItem) {?>
        <?if($arItem["SELECTED"]) {?>
            <li><a href="<?=$arItem["LINK"]?>" class="selected"><span class="item"><?=$arItem["TEXT"]?></span></a></li>
        <?} else {?>
            <li><a href="<?=$arItem["LINK"]?>"><span class="item"><?=$arItem["TEXT"]?></span></a></li>
        <?}?>
    <?}?>
    </ul>
<?endif?>