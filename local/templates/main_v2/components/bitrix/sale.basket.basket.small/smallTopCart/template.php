<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arOrder = array(
    "NAME" => "ASC",
    "ID" => "ASC"
);
$arFilter = array(
    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
    "LID" => SITE_ID,
    "ORDER_ID" => "NULL"
);
$resBasket = CSaleBasket::GetList($arOrder, $arFilter);
while ($arBasket = $resBasket->Fetch()) {
    if ($arBasket['CAN_BUY'] == 'N' || $arBasket['DELAY'] == 'Y') {
        CSaleBasket::Delete($arBasket['ID']);
    }
}

$totalCount = 0;
$totalPrice = 0;
if ($arResult["READY"]=="Y")
{
    foreach ($arResult["ITEMS"] as &$v) {
        $totalCount += $v['QUANTITY'];
        $totalPrice += $v['QUANTITY'] * $v['PRICE'];
    }
}
?>

<a href="/personal/cart/"><span class="ico ico-delivery"></span>
    <span class="item item-dotted">сумма <?=FormatCurrency($totalPrice, 'RUB')?></span>
    <span class="backet"></span>
</a>