<?php

$arRes = [];
$resSections = CIBlockPropertyEnum::GetList(
    ['SORT' => 'ASC', 'ID' => 'ASC'],
    ['IBLOCK_ID' => CATALOG_IB, 'CODE' => 'AREATYPE']
);

while ($arSection = $resSections->Fetch()) {
    if (!isset($arRes[$arSection['ID']])) {
        $arType = CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => 20, 'NAME' => $arSection['VALUE']],
            false,
            false,
            ['PROPERTY_LINK', 'PROPERTY_ALL'])->Fetch();
        $arRes[$arSection['ID']] = [
            'NAME' => $arSection['VALUE'],
            'LINK' => $arType['PROPERTY_LINK_VALUE'],
            'ALL' => $arType['PROPERTY_ALL_VALUE'],
            'SECTIONS' => [],
            'EMPTY' => 1
        ];
    }
}

foreach ($arResult["SECTIONS"] as $arSection) {
    $resElements = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => CATALOG_IB,
            'SECTION_ID' => $arSection['ID'],
            'ACTIVE' => 'Y',
            'INCLUDE_SUBSECTIONS' => 'Y'
        ],
        false,
        false,
        ['PROPERTY_AREATYPE']);

    while ($arElement = $resElements->Fetch()) {
        if (!isset($arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['SECTIONS'][$arSection['ID']])) {
            $arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['SECTIONS'][$arSection['ID']] = $arSection;
            $arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['EMPTY'] = 0;
        }
    }
}

$arResult = $arRes;