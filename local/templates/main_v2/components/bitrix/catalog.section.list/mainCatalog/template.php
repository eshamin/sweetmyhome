<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$bOpen = false;
$sectionName = '';
foreach ($arResult as $categoryId => $arSections) {
    if ($arSections['EMPTY']) {
        continue;
    }
    $bOpen = true;
    $totalCount = 0; ?>
    <div class="catalog-tab__item">
        <a class="catalog-tab__title" href="<?=$arSections['LINK'];?>">
            <h2 class="title title_size_18">
                <img src="<?=SITE_TEMPLATE_PATH/*CFile::GetPath($arSection['UF_MENU_ICO'])*/?>/img/category-pic.svg" class="image" /><?=$arSections['NAME'] ?>
            </h2>
        </a>
        <div class="catalog-tab__list">
<?
    foreach($arSections["SECTIONS"] as $arSection) {
        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));

        if ($totalCount < 9) {
            $totalCount++; ?>
            <a id="<?=$this->GetEditAreaId($arSection['ID']); ?>" class="catalog-tab__list-item" href="<?=$arSection['SECTION_PAGE_URL'] ?>"><?=$arSection['NAME'] ?></a>
        <?
        }
    }
    ?>
        </div>
        <div class="catalog-tab__footer">
            <a class="catalog-tab__footer-item" href="<?=$arSections['LINK'];?>"><?=$arSections['ALL'];?><span class="ico ico-angle-right"></span></a>
        </div>
    </div>
<?}?>