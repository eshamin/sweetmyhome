<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="dropdown-catalog">
<?
$bOpenFirst = false;
$bOpenSecond = false;
$bStop = false;
$sectionSecondUrl = '';
$totalCount = 0;
$totalPreTotalCount = 0;
$menuCount = 0;
debug($arResult);
foreach ($arResult as $categoryOfDayId => $arSections) {
    if ($arSections['EMPTY']) {
        continue;
    }
    $menuCount++;?>
    <li>
        <a href="javascript: void(0)" class="b-dropright-catalog-toggle"><?=$arSections["NAME"]?><span class="ico ico-angle-right"></span></a>
        <div class="b-dropright-catalog" id="menuCount<?=$menuCount?>">
            <ul>
<?
    foreach ($arSections["SECTIONS"] as $arSection) {
        $totalCount++;
    ?>

        <?if ($arSection['DEPTH_LEVEL'] == 2 && $bStop) {
            continue;
        }

        $totalPreTotalCount++;
        ?>
        <?if ($arSection['DEPTH_LEVEL'] == 1 && $bOpenSecond) {
            $bOpenSecond = false;
            $bStop = false;
            $countThird = 0;
        ?>
            </ul>
        </li>

    <?if ($totalCount > 8) {?>
        </ul>
        <ul>
        <?
        $totalCount = 0;
    }?>

    <?}?>

        <?if ($arSection['DEPTH_LEVEL'] == 1) {
            $bOpenSecond = true;
            $sectionSecondUrl = $arSection["SECTION_PAGE_URL"];
            ?>
            <li class="holder">
                <ul>
                    <li class="header"><a href="<?=$sectionSecondUrl?>"><?=$arSection["NAME"]?></a></li>
        <?}?>

        <?if ($arSection['DEPTH_LEVEL'] == 2) {
            $countThird++;
            if ($countThird > 5) {
                ?>
                <li class="all-category"><a href="<?= $sectionSecondUrl ?>">Все категории&nbsp;&nbsp;&nbsp;<img src="<?= SITE_TEMPLATE_PATH ?>/img/gt.png" alt=""/></a></li>
                <?
                $bStop = true;
            } else {
                ?>
                <li><a href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a></li>
            <?
            }
        }
    }

    $bStop = false;
    $bOpenSecond = false;
    $totalCount = 0;
    $countThird = 0;
    $index = (ceil($totalPreTotalCount / 8) - 1) ? : 1;
    $index = ($index < 3) ? $index : 3;
    $totalPreTotalCount = 0;
    ?>
                    <script>
                        $('#menuCount<?=$menuCount?>').addClass('column<?=$index?>');
                    </script>
                </ul>
                <ul class="good-of-day">
                    <li>
                        <?=getProductOfDay($categoryOfDayId)?>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </li>
    <?
}
    $index = (ceil($totalPreTotalCount / 8) - 1) ? : 1;
    $index = ($index < 3) ? $index : 3;
    $totalPreTotalCount = 0;
    ?>
                <script>
                    $('#menuCount<?=$menuCount?>').addClass('column<?=$index?>');
                </script>
                    </ul>

    </li>
</ul>
