<?php

$arRes = [];
$resSections = CIBlockPropertyEnum::GetList(
    ['SORT' => 'ASC', 'ID' => 'ASC'],
    ['IBLOCK_ID' => CATALOG_IB, 'CODE' => 'AREATYPE']
);

while ($arSection = $resSections->Fetch()) {
    if (!isset($arRes[$arSection['ID']])) {
        $arSort = [
            'SHOW_COUNTER' => 'DESC'
        ];
        $arFilter = [
            'PROPERTY_AREATYPE' => $arSection['ID'],
            'PROPERTY_PRODUCTOFDAY_VALUE' => 'Y',
            'ACTIVE' => 'Y'
        ];

        $arSelect = [
            'ID',
            'NAME',
            'CATALOG_GROUP_1',
            'PREVIEW_PICTURE'
        ];

        $arProduct = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect)->Fetch();
        if ($arProduct) {
            $resComments = CIBlockSection::GetList(
                array('DATE_CREATED' => 'DESC'),
                array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID_VALUE' => $arProduct['ID']),
                false,
                false,
                array('PROPERTY_RATING'));
            $totalRating = 0;
            $totalCount = 0;
            while ($comment = $resComments->Fetch()) {
                $totalRating += $comment['PROPERTY_RATING_VALUE'];
                $totalCount++;
            }

            if ($totalCount > 0) {
                $totalRating = floor($totalRating / $totalCount);
            } else {
                $totalRating = 1;
            }

            $arProduct['RATING'] = $totalRating;
        }

        $arType = CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => 20, 'NAME' => $arSection['VALUE']],
            false,
            false,
            ['PROPERTY_LINK', 'PROPERTY_ALL'])->Fetch();
        $arRes[$arSection['ID']] = [
            'NAME' => $arSection['VALUE'],
            'LINK' => $arType['PROPERTY_LINK_VALUE'],
            'ALL' => $arType['PROPERTY_ALL_VALUE'],
            'SECTIONS' => [],
            'PRODUCTOFDAY' => $arProduct
        ];
    }
}

foreach ($arResult["SECTIONS"] as $arSection) {
    $resElements = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => CATALOG_IB,
            'SECTION_ID' => $arSection['ID'],
            'ACTIVE' => 'Y',
            'INCLUDE_SUBSECTIONS' => 'Y'
        ],
        false,
        false,
        ['PROPERTY_AREATYPE']);

    while ($arElement = $resElements->Fetch()) {
        if (!isset($arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['SECTIONS'][$arSection['ID']])) {
            $arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['SECTIONS'][$arSection['ID']] = $arSection;
            //$arRes[$arElement['PROPERTY_AREATYPE_ENUM_ID']]['COUNT']++;
        }
    }
}

$arResult = $arRes;