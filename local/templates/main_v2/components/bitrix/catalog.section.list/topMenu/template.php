<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="b-main-menu__dropdown b-dropdown">
<?
$bOpenFirst = false;
$bOpenSecond = false;
$bStop = false;
$sectionSecondUrl = '';
$totalCount = 0;
$totalPreTotalCount = 0;
$menuCount = 0;

foreach ($arResult as $arSections) {
    $totalCount = count($arSections['SECTIONS']);
    if (!$totalCount) {
        continue;
    }

    $arProduct = false;
    $styleNumber = (floor($totalCount / 10) + 1 < 3) ? floor($totalCount / 10) + 1 : 3;

    if (!empty($arSections['PRODUCTOFDAY'])) {
        $styleNumber++;
        $arProduct = $arSections['PRODUCTOFDAY'];
    }
    ?>
    <li class="b-dropdown__item">
        <a href="<?=$arSections['LINK']?>" class="b-dropright-catalog-toggle"><?=$arSections["NAME"]?><span class="ico ico-angle-right"></span></a>
        <div class="b-dropright-catalog b-dropright-catalog-<?=$styleNumber?>">
            <div class="b-dropright-catalog__list">
                <div class="b-dropright-<?=$styleNumber?> b-dropright-catalog__list-items">
                    <ul class="g-left">
                        <?
                        $secCount = 0;
                        foreach ($arSections["SECTIONS"] as $arSection) {
                            $secCount++;
                            if ($secCount == 30) {?>
                                <li class="b-dropright-catalog__list-item"><a href="/catalog/">Все товары<span class="ico ico-angle-right"></span></a></li>
                            <?
                                break;
                            } else {
                        ?>
                        <li class="b-dropright-catalog__list-item"><a href="<?= $arSection["SECTION_PAGE_URL"] ?>"><?= $arSection["NAME"] ?></a></li>
                        <?}?>
                        <?if ($secCount % 10 == 0) {?>
                    </ul>
                    <ul class="g-left">
                        <?}?>
                        <?}?>
                        <li class="g-clear"></li>
                    </ul>
                </div>
                <?if ($arProduct) {?>
                <div class="b-dropright-1-of-<?=$styleNumber?> g-left">
                    <ul class="b-good-of-day">
                        <li class="b-products__item-container">
                            <h4 class="b-good-of-day__h4">Товар дня!</h4>
                            <a href="#">
                                <img class="b-product__image" src="<?=CFile::GetPath($arProduct["PREVIEW_PICTURE"])?>" alt=""/>
                            </a>
                            <?if ($arProduct['RATING'] > 0) {?>
                            <span class="b-top-panel__feedback-stars">
                                <?for ($i = 0; $i < $arProduct['RATING']; $i++) {?>
                                <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gold"></span>
                                <?}?>
                                <?for ($i = $arProduct['RATING']; $i < 5; $i++) {?>
                                <span class="b-top-panel__feedback-star b-top-panel__feedback-star-gray"></span>
                                <?}?>
                            </span>
                            <?}?>

                            <p class="b-product__title"><?=$arProduct['NAME']?></p>
                            <div class="b-product__price">
                                <span>
                                    <span>
                                        <?=(strlen(FormatCurrency($arProduct['CATALOG_PRICE_1'], 'RUB')) > 0) ? FormatCurrency($arProduct['CATALOG_PRICE_1'], 'RUB') : '&nbsp;'?>
                                    </span>
                                </span>
                                <a href="<?=$arProduct['ADD_URL']?>"><span class="ico ico-delivery2"></span></a>
<!--                                <a class="js-button js-button-white add2basket" item-id="--><?//=$arProduct['ID']?><!--" href="javascript:void(0)">Купить</a>-->
                            </div>
                            <!--<a href="#" class="sevenday"><img src="<?= SITE_TEMPLATE_PATH ?>/img/bullet.png" alt=""/>&nbsp;&nbsp;Под заказ 7 дней</a>-->
                        </li>
                    </ul>
                </div>
                <div class="g-clear" ></div>
                <?}?>
            </div>
        </div>
    </li>
    <?}?>
</ul>
