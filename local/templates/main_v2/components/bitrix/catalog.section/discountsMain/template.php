<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult['ITEMS'])) { ?>
    <ul class="b-products__list">
        <?foreach ($arResult['ITEMS'] as $key => $arItem) {
            $strMainID = $this->GetEditAreaId($arItem['ID'] . $key);
            ?>
            <li class="b-products__item" id="<?=$strMainID?>">
                <div class="b-products__item-container">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>">
                        <img class="b-product__image" src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="" />
                    </a>
                    <p class="b-product__title">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" title="<?=$arItem['NAME']?>"><?=$arItem['NAME']?></a>
                    </p>
                    <div class="b-product__price">
                        <span><?=$arItem['PRICES']['BASE']['PRINT_VALUE']?></span>
                        <a href="<?=$arItem['ADD_URL']?>"><span class="ico ico-delivery2"></span></a>
                        <a href="javascript: void(0)" class="add-to-favorites" item-id="<?=$arItem['ID']?>"><span class="star"></span></a>
                    </div>
                </div>
            </li>
            <?
        }
        ?>
    </ul>
<?}
?>