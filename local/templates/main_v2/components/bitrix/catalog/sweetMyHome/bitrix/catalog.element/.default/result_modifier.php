<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
global $USER;

global $arrFilter;

/*-- Цвета и модификации --*/
if ($arResult['PROPERTIES']['SERIATWO']['VALUE']) {
    $arSort = array('SORT');
    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => CATALOG_IB,
        '!ID' => $arResult['ID'],
        'PROPERTY_SERIATWO' => $arResult['PROPERTIES']['SERIATWO']['VALUE']
    );
    $arSelect = array(
        'ID',
        'NAME',
        'CODE',
        'DETAIL_PAGE_URL',
        'PROPERTY_COLOR_IMAGE',
        'PROPERTY_COLORTEXT',
        'DETAIL_PICTURE',
        'CATALOG_GROUP_1',
        'PROPERTY_SIZE1',
        'PROPERTY_SIZE2',
        'PROPERTY_SIZE3',
        'PROPERTY_SER',
        'COMPARE_URL'
    );

    $resSameProducts = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, false, $arSelect);

    while ($sameProduct = $resSameProducts->GetNext()) {
        $colorImage = CIBlockPropertyEnum::GetByID($sameProduct['PROPERTY_COLORTEXT_ENUM_ID']);
        $sameProduct['PICTURE'] = $colorImage['XML_ID'];
        $arResult['SAME_PRODUCTS'][] = $sameProduct;
    }
}

/*-- Похожие товары --*/
$arSort = array('SORT', 'CATALOG_PRICE_1');
$arFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_ID' => CATALOG_IB,
    'SECTION_ID' => $arResult['IBLOCK_SECTION_ID'],
    '!ID' => $arResult['ID'],
    '>=CATALOG_PRICE_1' => $arResult['PRICES']['BASE']['VALUE'] - $arResult['PRICES']['BASE']['VALUE'] * 0.1,
    '<=CATALOG_PRICE_1' => $arResult['PRICES']['BASE']['VALUE'] + $arResult['PRICES']['BASE']['VALUE'] * 0.1,
);

if ($arResult['PROPERTIES']['COLORTEXT']['VALUE']) {
    $arFilter['PROPERTY_COLORTEXT_VALUE'] = $arResult['PROPERTIES']['COLORTEXT']['VALUE'];
}

$arNavStartParams = array(
    'nTopCount' => 30
);
$arSelect = array(
    'ID', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME', 'DETAIL_PAGE_URL'
);
$resSimilarProducts = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavStartParams, $arSelect);
while ($similarProduct = $resSimilarProducts->GetNext()) {
    $arResult['SIMILAR_PRODUCTS'][] = $similarProduct;
}

/*-- Лучшие аксессуары --*/
$arFilter = array('ID' =>$arResult['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => CATALOG_IB);
$arSelect = array('UF_SECTIONS_RELATED');
$arSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect)->Fetch();

if (count($arSection['UF_SECTIONS_RELATED']) > 0) {
    $arResult['ACCESSORIES_SECTIONS'][0] = 'Рекомендуем';
    foreach ($arSection['UF_SECTIONS_RELATED'] as $section) {
        $arSort = array('CATALOG_PRICE_1' => 'ASC');
        $arFilter = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => CATALOG_IB,
            'SECTION_ID' => $section,
        );

        if ($arResult['PROPERTIES']['COLORTEXT']['VALUE']) {
            $arFilter['PROPERTY_COLORTEXT_VALUE'] = $arResult['PROPERTIES']['COLORTEXT']['VALUE'];
        }

        $arNavStartParams = array(
            'nTopCount' => 50
        );
        $arSelect = array(
            'ID', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME', 'CODE', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL', 'PROPERTY_COLORTEXT'
        );

        $exist = false;
        $resBestAccessories = CIBlockElement::GetList($arSort, array_merge($arFilter, $arrFilter), false, $arNavStartParams, $arSelect);
        while ($bestAccessory = $resBestAccessories->GetNext()) {
            if (!$exist) {
                $arResult['ACCESSORIES'][0][] = $bestAccessory;
                $exist = true;
            }
            $arResult['ACCESSORIES'][$bestAccessory['IBLOCK_SECTION_ID']][] = $bestAccessory;
            $arSec = CIBlockSection::GetByID($bestAccessory['IBLOCK_SECTION_ID'])->Fetch();
            $arResult['ACCESSORIES_SECTIONS'][$bestAccessory['IBLOCK_SECTION_ID']] = $arSec['NAME'];
        }
    }
}

/*-- Баннеры --*/
$arSort = array('SORT');
$arFilter = array('IBLOCK_ID' => 17, 'ACTIVE' => 'Y');
$resBanners = CIBlockElement::GetList($arSort, $arFilter);
while ($banner = $resBanners->Fetch()) {
    $arResult['BANNERS'][] = $banner;
}

/*-- Вы уже смотрели --*/
if (count($_SESSION['VIEWED_PRODUCTS']) > 0) {
    $arSort = array();
    $arFilter = array(
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => CATALOG_IB,
        'ID' => $_SESSION['VIEWED_PRODUCTS'],
        '!ID' => $arResult['ID']
    );
    $arNavStartParams = array(
        'nTopCount' => 30
    );
    $arSelect = array(
        'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME'
    );
    $resViewedProducts = CIBlockElement::GetList($arSelect, array_merge($arFilter, $arrFilter), false, $arNavStartParams, $arSelect);
    while ($viewedProduct = $resViewedProducts->GetNext()) {
        $arResult['VIEWED_PRODUCTS'][] = $viewedProduct;
    }
}

/*-- Люди также покупают --*/
$arSelect = array(
    'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME'
);

CModule::IncludeModule('sale');
$resOrders = CSaleOrder::GetList(array(), array('BASKET_PRODUCT_ID' => $arResult['ID']));
while ($arOrder = $resOrders->Fetch()) {
    $resBaskets = CSaleBasket::GetList(array(), array('ORDER_ID' => $arOrder['ID'], '!PRODUCT_ID' => $arResult['ID']));
    while ($arBasket = $resBaskets->Fetch()) {
        $alsoProduct = CIBlockElement::GetList(
            array(),
            array_merge(array('ID' => $arBasket['PRODUCT_ID'], $arrFilter)),
            false,
            false,
            $arSelect
        )->GetNext();
        if ($alsoProduct) {
            $arResult['ALSO_PRODUCTS'][$arBasket['PRODUCT_ID']] = $alsoProduct;
        }
    }
}

$arOrder = array(
    'SHOW_COUNTER' => 'DESC'
);
$arFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_ID' => CATALOG_IB,
    '!ID' => $arResult['ID']
);
$arNavStartParams = array(
    'nTopCount' => 30
);
$arSelect = array(
    'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'CATALOG_GROUP_1', 'NAME'
);
$resAlsoProducts = CIBlockElement::GetList(
    $arSelect,
    array_merge($arFilter, $arrFilter),
    false,
    $arNavStartParams,
    $arSelect
);
while ($alsoProduct = $resAlsoProducts->GetNext()) {
    $arResult['ALSO_PRODUCTS'][$alsoProduct['ID']] = $alsoProduct;
}

/*-- Отзывы --*/
$arOrder = array('PROPERTY_RATING' => 'DESC');
$arFilter = array('ACTIVE' => 'Y', 'IBLOCK_ID' => 8, 'PROPERTY_PRODUCT_ID' => $arResult['ID']);
$arSelect = array(
    'PROPERTY_RATING',
    'PROPERTY_FIO',
    'PROPERTY_COMMENT',
    'PROPERTY_MINUS',
    'PROPERTY_PLUS',
    'PROPERTY_MAIN'
);
$arResult['STAT_COMMENTS'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
    4 => array(),
    5 => array(),
);
$resComments = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
while ($comment = $resComments->Fetch()) {
    $arResult['ALL_COMMENTS'][] = $comment;
    $arResult['STAT_COMMENTS'][$comment['PROPERTY_RATING_VALUE']][] = $comment;
    if ($comment['PROPERTY_RATING_VALUE'] > 0) {
        $arResult['RATING_COMMENTS']++;
        $arResult['STAT_COMMENTS'][$comment['PROPERTY_RATING_VALUE']]['COUNT']++;
    }

    if (strlen(trim($comment['PROPERTY_COMMENT_VALUE']['TEXT'])) > 0) {
        $arResult['SHOW_COMMENTS'][] = $comment;
    }

    if ($comment['PROPERTY_MAIN_COMMENT_VALUE'] == 'Y') {
        $arResult['MAIN_COMMENT'] = $comment;
    }

    if (count($arResult['MAIN_COMMENT']) < 1 && strlen(trim($comment['PROPERTY_COMMENT_VALUE']['TEXT'])) > 0) {
        $arResult['MAIN_COMMENT'] = $comment;
    }
}
?>