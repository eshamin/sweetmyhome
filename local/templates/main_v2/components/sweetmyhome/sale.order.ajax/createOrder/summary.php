<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<table class="b-makeorder__bottom">
    <tr>
        <td>
            <input type="checkbox" id="b-action" class="yellow-chb"/>
            <!--<label for="b-action" class="yellow-chb-label">Сообщайте мне об акциях, скидках и рекомендациях.</label>-->
            <a href="javascript:void(0)" id="ORDER_CONFIRM_BUTTON" class="js-button js-button-yellow">Подтвердить заказ</a>
        </td>
        <td>
            <p>Итого к оплате</p>
            <span><?=$arResult['ORDER_TOTAL_PRICE_FORMATED']?></span>
            <?if ($arResult['DELIVERY_PRICE'] > 0) {?>
                <p>Включая доставку <?=$arResult['DELIVERY_PRICE_FORMATED']?></p>
            <?}?>
            <!--<p>Включая услуги 1300.-</p>-->
        </td>
    </tr>
</table>