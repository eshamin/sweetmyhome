<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="b-makeorder__bottom-list">
    <h4>Способ оплаты</h4>
    <ul class="b-makeorder__delivery-type">
        <script type="text/javascript">
            function changePaySystem(param)
            {
                if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
                {
                    if (param == 'account')
                    {
                        if (BX("PAY_CURRENT_ACCOUNT"))
                        {
                            BX("PAY_CURRENT_ACCOUNT").checked = true;
                            BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                            BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

                            // deselect all other
                            var el = document.getElementsByName("PAY_SYSTEM_ID");
                            for(var i=0; i<el.length; i++)
                                el[i].checked = false;
                        }
                    }
                    else
                    {
                        BX("PAY_CURRENT_ACCOUNT").checked = false;
                        BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                        BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                    }
                }
                else if (BX("account_only") && BX("account_only").value == 'N')
                {
                    if (param == 'account')
                    {
                        if (BX("PAY_CURRENT_ACCOUNT"))
                        {
                            BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

                            if (BX("PAY_CURRENT_ACCOUNT").checked)
                            {
                                BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                                BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                            }
                            else
                            {
                                BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                                BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                            }
                        }
                    }
                }

                submitForm();
            }
        </script>

        <?
        uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value

        foreach($arResult["PAY_SYSTEM"] as $arPaySystem)
        {
            if (count($arResult["PAY_SYSTEM"]) == 1)
            {
                ?>
                <li>
                    <input type="hidden" name="PAY_SYSTEM_ID" value="<?=$arPaySystem["ID"]?>">
                    <input type="radio"
                        class="yellow-radio"
                        id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
                        name="PAY_SYSTEM_ID"
                        value="<?=$arPaySystem["ID"]?>"
                        <?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
                        onclick="changePaySystem();"
                        />
                    <label class="yellow-radio-label" for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
                        <?=$arPaySystem["PSA_NAME"];?>
                    </label>
                </li>
                <?
            }
            else // more than one
            {
            ?>
                <li>
                    <input type="radio"
                        id="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>"
                        class="yellow-radio"
                        name="PAY_SYSTEM_ID"
                        value="<?=$arPaySystem["ID"]?>"
                        <?if ($arPaySystem["CHECKED"]=="Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"]=="Y")) echo " checked=\"checked\"";?>
                        onclick="changePaySystem();" />
                    <label class="yellow-radio-label" for="ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>" onclick="BX('ID_PAY_SYSTEM_ID_<?=$arPaySystem["ID"]?>').checked=true;changePaySystem();">
                        <?=$arPaySystem["PSA_NAME"];?>
                    </label>
                </li>
            <?
            }
        }
        ?>
<!--
        <li>
            <input type="radio" name="b-makeorder__delivery_type" id="delivery_type7" disabled class="yellow-radio"/>
            <label for="delivery_type7" class="yellow-radio-label">Онлайн оплата банковской картой</label>
        </li>
        <li>
            <input type="radio" name="b-makeorder__delivery_type" id="delivery_type8" disabled class="yellow-radio"/>
            <label for="delivery_type8" class="yellow-radio-label">Электронные деньги</label>
        </li>
        <li>
            <input type="radio" name="b-makeorder__delivery_type" id="delivery_type9" disabled class="yellow-radio"/>
            <label for="delivery_type9" class="yellow-radio-label">Интернет-банкинг</label>
        </li>
        <li>
            <input type="radio" name="b-makeorder__delivery_type" id="delivery_type10" disabled class="yellow-radio"/>
            <label for="delivery_type10" class="yellow-radio-label">Баланс телефона</label>
        </li>
        <li>
            <input type="radio" name="b-makeorder__delivery_type" id="delivery_type11" disabled class="yellow-radio"/>
            <label for="delivery_type11" class="yellow-radio-label">Банковский перевод на реквизиты</label>
        </li>
        <li>
            <input type="radio" name="b-makeorder__delivery_type" id="delivery_type12" disabled class="yellow-radio"/>
            <label for="delivery_type12" class="yellow-radio-label">Безналичный расчет</label>
        </li>-->
    </ul>
</div>