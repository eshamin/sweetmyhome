<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default", $arUserVals = array())
	{
		if (!empty($arSource))
		{
            foreach ($arSource as $arProperties)
            {
                if ($arProperties['CODE'] == 'FLAT') {
                    continue;
                }
                if ($arProperties["TYPE"] == "CHECKBOX")
                {
                    if ($arProperties['CODE'] == 'want') {?>
                        <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" />
                    <?} else {
                        ?>
                        <input type="hidden" name="<?= $arProperties["FIELD_NAME"] ?>" value="" xmlns="http://www.w3.org/1999/html">
                        <input class="yellow-chb" type="checkbox" name="<?= $arProperties["FIELD_NAME"] ?>" id="<?= $arProperties["FIELD_NAME"] ?>" value="Y"<?if ($arProperties["CHECKED"] == "Y") echo " checked"; ?>>
                        <label for="<?= $arProperties["FIELD_NAME"] ?>" class="yellow-chb-label"><?= $arProperties["NAME"] ?>
                        <?if ($arProperties['CODE'] == 'needp') { ?>
                            на <input type="text" placeholder="12" name="ORDER_PROP_29" value=""/> этаж </label>
                        <?
                        } else { ?>
                        </label>
                    <?
                        }
                        if ($arProperties['CODE'] == 'needs') { ?>
                            <ul class="b-makeorder__delivery-type">
                        <?
                        }
                    }
                }
                elseif ($arProperties["TYPE"] == "TEXT" && $arProperties['CODE'] != 'FLOOR')
                {
                    if ($arProperties['CODE'] == 'CITY') {?>
                        <li class="b-makeorder__adress-city">
                            <span><?=$arProperties["NAME"]?></span>
                            <span class="b-makeorder__adress-input-holder">
                                <span class="error-tooltip" id="error-city">Укажите город<span class="error-triangle"></span></span>
                                <input placeholder="<?=$arProperties['DESCRIPTION']?>" type="text" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" required id="<?=$arProperties["FIELD_NAME"]?>" />
                            </span>
                            <span></span>
                        </li>
                    <?} elseif ($arProperties['CODE'] == 'ADDRESS') {?>
                        <li class="b-makeorder__adress-adress">
                            <span><?=$arProperties["NAME"]?></span>
                            <span class="b-makeorder__adress-input-holder">
                                <span class="error-tooltip" id="error-adress">Укажите адрес доставки<span class="error-triangle"></span></span>
                                <input placeholder="<?=$arProperties['DESCRIPTION']?>" type="text" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" required id="<?=$arProperties["FIELD_NAME"]?>" />
                            </span>
                            <span class="b-makeorder__adress-input-holder"><input type="text" name="ORDER_PROP_31" value="" placeholder="кв. или офис"/></span>
                        </li>
                    <?} elseif ($arProperties['CODE'] == 'delive-date') {?>
                        <li class="b-makeorder__adress-data">
                            <span><?=$arProperties["NAME"]?></span>
                            <span class="b-makeorder__adress-input-holder">
                                <span class="error-tooltip" id="error-data">Укажите дату доставки<span class="error-triangle"></span></span>
                                <input id="datepicker" placeholder="00.00.00" type="text" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" required id="<?=$arProperties["FIELD_NAME"]?>" />
                                <span style="margin-left: 24px;">Удобное время уточнит менеджер</span>
                            </span>
                            <span></span>
                        </li>
                    <?} else {?>
                        <li class="b-makeorder__adress-adress">
                            <span><?=$arProperties["NAME"]?></span>
                            <span class="b-makeorder__adress-input-holder">
                                <input placeholder="<?=$arProperties['DESCRIPTION']?>" type="text" maxlength="250" size="<?=$arProperties["SIZE1"]?>" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" />
                            </span>
                            <span></span>
                        </li>
                    <?}
                }
                elseif ($arProperties["TYPE"] == "SELECT")
                {
                    continue;
                    ?>

                    <li>
                        <span>
                            <?=$arProperties["NAME"]?>
                        </span>

                        <span>
                            <select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
                                <?
                                foreach($arProperties["VARIANTS"] as $arVariants):
                                ?>
                                    <option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
                                <?
                                endforeach;
                                ?>
                            </select>
                        </span>
                    </li>
                    <?
                }
                elseif ($arProperties["TYPE"] == "MULTISELECT")
                {
                    continue;
                    ?>
                    <br/>
                    <div class="bx_block r1x3 pt8">
                        <?=$arProperties["NAME"]?>
                        <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                            <span class="bx_sof_req">*</span>
                        <?endif;?>
                    </div>

                    <div class="bx_block r3x1">
                        <select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
                            <?
                            foreach($arProperties["VARIANTS"] as $arVariants):
                            ?>
                                <option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
                            <?
                            endforeach;
                            ?>
                        </select>

                        <?
                        if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                        ?>
                        <div class="bx_description">
                            <?=$arProperties["DESCRIPTION"]?>
                        </div>
                        <?
                        endif;
                        ?>
                    </div>
                    <div style="clear: both;"></div>
                    <?
                }
                elseif ($arProperties["TYPE"] == "TEXTAREA")
                {
                    continue;
                    $rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
                    ?>
                    <br/>
                    <div class="bx_block r1x3 pt8">
                        <?=$arProperties["NAME"]?>
                        <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                            <span class="bx_sof_req">*</span>
                        <?endif;?>
                    </div>

                    <div class="bx_block r3x1">
                        <textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>"><?=$arProperties["VALUE"]?></textarea>

                        <?
                        if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                        ?>
                        <div class="bx_description">
                            <?=$arProperties["DESCRIPTION"]?>
                        </div>
                        <?
                        endif;
                        ?>
                    </div>
                    <div style="clear: both;"></div>
                    <?
                }
                elseif ($arProperties["TYPE"] == "LOCATION")
                {
                    ?>
                    <li>
                        <span>
                            <?=$arProperties["NAME"]?>
                        </span>

                        <?
                        global $locationId;
                        $value = 0;//$locationId;

                        if (isset($arUserVals['DELIVERY_LOCATION']) && $arUserVals['DELIVERY_LOCATION'] > 0) {
                            $value = $arUserVals['DELIVERY_LOCATION'];
                        }

                        // here we can get '' or 'popup'
                        // map them, if needed
                        if(CSaleLocation::isLocationProMigrated())
                        {
                            $locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
                            $locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
                        }
                        ?>

                        <?if($locationTemplateP == 'steps'):?>
                            <input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" value="<?=($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0')?>" />
                        <?endif?>

                        <?CSaleLocation::proxySaleAjaxLocationsComponent(array(
                            "AJAX_CALL" => "Y",
                            "COUNTRY_INPUT_NAME" => "COUNTRY",
                            "REGION_INPUT_NAME" => "REGION",
                            "COUNTRY" => 19,
                            "CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
                            "CITY_OUT_LOCATION" => "Y",
                            "LOCATION_VALUE" => $value,
                            "ORDER_PROPS_ID" => $arProperties["ID"],
                            "ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
                            "SIZE1" => $arProperties["SIZE1"],
                        ),
                        array(
                            "ID" => $value,
                            "CODE" => "",
                            "SHOW_DEFAULT_LOCATIONS" => "Y",

                            // function called on each location change caused by user or by program
                            // it may be replaced with global component dispatch mechanism coming soon
                            "JS_CALLBACK" => "submitFormProxy",

                            // function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
                            // it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
                            "JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

                            // an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
                            // it may be replaced with global component dispatch mechanism coming soon
                            "JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

                            "DISABLE_KEYBOARD_INPUT" => "Y",
                            "PRECACHE_LAST_LEVEL" => "Y",
                            "PRESELECT_TREE_TRUNK" => "Y",
                            "SUPPRESS_ERRORS" => "Y"
                        ),
                        $locationTemplateP,
                        true,
                        ''//'location-block-wrapper'
                        )?>
                    </li>
                    <?
                }
                elseif ($arProperties["TYPE"] == "RADIO")
                {
                    ?>
                    <li class="fl_l"><label><?=$arProperties["NAME"]?></label></li>

                        <?
                        if (is_array($arProperties["VARIANTS"]))
                        {
                            foreach($arProperties["VARIANTS"] as $arVariants) {
                                ?>
                                <li class="fl_l">
                                    <input
                                        type="radio"
                                        class="yellow-radio"
                                        name="<?= $arProperties["FIELD_NAME"] ?>"
                                        id="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"
                                        value="<?= $arVariants["VALUE"] ?>" <?if ($arVariants["CHECKED"] == "Y") echo " checked"; ?> />

                                    <label for="<?= $arProperties["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>" class="yellow-radio-label"><?= $arVariants["NAME"] ?></label>
                                </li>
                            <?
                            }
                        }
                        ?>
                    <div class="clear"></div>
                </ul>
            <div class="clear"></div>
                    <?
                }
                elseif ($arProperties["TYPE"] == "FILE")
                {
                    continue;
                    ?>
                    <br/>
                    <div class="bx_block r1x3 pt8">
                        <?=$arProperties["NAME"]?>
                        <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                            <span class="bx_sof_req">*</span>
                        <?endif;?>
                    </div>

                    <div class="bx_block r3x1">
                        <?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>

                        <?
                        if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
                        ?>
                        <div class="bx_description">
                            <?=$arProperties["DESCRIPTION"]?>
                        </div>
                        <?
                        endif;
                        ?>
                    </div>

                    <div style="clear: both;"></div><br/>
                    <?
                }
                ?>

                <?if(CSaleLocation::isLocationProEnabled()):?>

                    <?
                    $propertyAttributes = array(
                        'type' => $arProperties["TYPE"],
                        'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
                    );

                    if(intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
                        $propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

                    if(intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
                        $propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

                    if($arProperties['IS_ZIP'] == 'Y')
                        $propertyAttributes['isZip'] = true;
                    ?>

                    <script>

                        <?// add property info to have client-side control on it?>
                        (window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
                            'id' => intval($arProperties["ID"]),
                            'attributes' => $propertyAttributes
                        ))?>);

                    </script>
                <?endif?>

                <?
            }
		}
	}
}
?>