<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props_format.php");
if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}

	}
}

$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");

CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>

<a name="order_form"></a>

<div id="order_form_div" class="order-checkout b-makeorder__tab_1">
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
	function getColumnName($arHeader)
	{
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}

if (!function_exists("cmpBySort"))
{
	function cmpBySort($array1, $array2)
	{
		if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
			return -1;

		if ($array1["SORT"] > $array2["SORT"])
			return 1;

		if ($array1["SORT"] < $array2["SORT"])
			return -1;

		if ($array1["SORT"] == $array2["SORT"])
			return 0;
	}
}
?>

<div class="bx_order_make">
	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
	{
		if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
		}
		elseif(!empty($arResult["OK_MESSAGE"]))
		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}
	else
	{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
			if(strlen($arResult["REDIRECT_URL"]) == 0)
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
			}
		}
		else
		{
			?>
			<script type="text/javascript">

			<?if(CSaleLocation::isLocationProEnabled()) {?>

				<?
				// spike: for children of cities we place this prompt
				$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
				?>

				BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
					'source' => $this->__component->getPath().'/get.php',
					'cityTypeId' => intval($city['ID']),
					'messages' => array(
						'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
						'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
						'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
							'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
							'#ANCHOR_END#' => '</a>'
						)).'</div>'
					)
				))?>);

			<?}?>

			var BXFormPosting = false;
			function submitForm(val)
			{
				if (BXFormPosting === true)
					return true;

				BXFormPosting = true;
				if(val != 'Y')
					BX('confirmorder').value = 'N';

				var orderForm = BX('ORDER_FORM');
				BX.showWait();

				<?if(CSaleLocation::isLocationProEnabled()):?>
					BX.saleOrderAjax.cleanUp();
				<?endif?>

				BX.ajax.submit(orderForm, ajaxResult);

				return true;
			}

			function ajaxResult(res)
			{
				var orderForm = BX('ORDER_FORM');
				try
				{
					// if json came, it obviously a successfull order submit

					var json = JSON.parse(res);
					BX.closeWait();

					if (json.error)
					{
						BXFormPosting = false;
						return;
					}
					else if (json.redirect)
					{
						window.top.location.href = json.redirect;
					}
				}
				catch (e)
				{
					// json parse failed, so it is a simple chunk of html

					BXFormPosting = false;
					BX('order_form_content').innerHTML = res;

					<?if(CSaleLocation::isLocationProEnabled()):?>
						BX.saleOrderAjax.initDeferredControl();
					<?endif?>
				}

				BX.closeWait();
				BX.onCustomEvent(orderForm, 'onAjaxSuccess');
			}

			function SetContact(profileId)
			{
				BX("profile_change").value = "Y";
				submitForm();
			}
			</script>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data" novalidate>
				<?=bitrix_sessid_post()?>
				<div id="order_form_content">
				<?
			}
			else
			{
				$APPLICATION->RestartBuffer();
			}

			if($_REQUEST['PERMANENT_MODE_STEPS'] == 1)
			{
				?>
				<input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
				<?
			}

			if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
			{
				foreach($arResult["ERROR"] as $v)
					echo ShowError($v);
				?>
				<script type="text/javascript">
					top.BX.scrollToNode(top.BX('ORDER_FORM'));
				</script>
				<?
			}?>
            <input type="hidden" name="PERSON_TYPE" value="1" />
            <input type="hidden" name="PERSON_TYPE_OLD" value="1" />
            <div class="b-makeorder__tab b-makeorder__tab-current b-makeorder__order">
                <div class="b-makeorder__order-wrapper">
                    <div>
                    <div class="fl_l">
                        <div>
                            <h2>Заказ</h2>
                            <div class="b-makeorder__order-delivery">
                                <div class="b-makeorder__tablinks">
                                    <a href="javascript:void(0)" class="b-makeorder__currenttab"><h2>Контактная информация</h2></a>
                                    <!--<a href="javascript:void(0)"><h2>Самовывоз</h2></a>-->
                                </div>
                                <div class="b-makeorder__tabs1 b-makeorder__tabs-current">
                                    <ul class="b-makeorder__adress">
                                        <?PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], $arResult["USER_VALS"]);?>
                                    </ul>

                                    <?include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");?>

                                    <ul class="b-makeorder__services">
                                        <li>
                                            <img src="<?=SITE_TEMPLATE_PATH?>/img/service-check.png" alt=""/>
                                            <a href="javascript:void(0)" class="b-makeorder__services-link">Дополнительные услуги</a>
                                            <div>
                                                <?PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"], $arResult["USER_VALS"]);?>
                                            </div>
                                        </li>
                                        <li>
                                            <img src="<?=SITE_TEMPLATE_PATH?>/img/service-check.png" alt=""/>
                                            <a href="javascript:void(0)" class="b-makeorder__services-link">Добавить комментарий</a>
                                            <div>
                                                <textarea name="USER_DESCRIPTION" id="b-makeorder__comment" cols="30" rows="10" placeholder="Ваш комментарий"></textarea>
                                            </div>
                                        </li>
                                    </ul>
                                    <!--<div class="b-makeorder__summary">
                                        <p>Итого стоимость услуг:</p>
                                        <span>1 300.-</span>
                                    </div>-->
                                </div>
                                <div class="b-makeorder__tabs1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fl_l">
                        <div class="b-makeorder__order-right-wrapper">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:sale.basket.basket",
                                "orderBasket",
                                Array(
                                    "COMPONENT_TEMPLATE" => "",
                                    "COLUMNS_LIST" => array("NAME", "PRICE", "QUANTITY", "SUM"),
                                    "PATH_TO_ORDER" => "/personal/order.php",
                                    "HIDE_COUPON" => "Y",
                                    "PRICE_VAT_SHOW_VALUE" => "N",
                                    "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                                    "USE_PREPAYMENT" => "N",
                                    "QUANTITY_FLOAT" => "N",
                                    "SET_TITLE" => "N",
                                    "ACTION_VARIABLE" => "action"
                                )
                            );?>
                        </div>
                        <?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.full.php");?>
                    </div>
                    <div class="clear"></div>
                    </div>
                </div>
            </div>

            <!--Таб для быстрого заказа-->
            <div class="b-makeorder__tab">
                <input type="hidden" id="fast-order" value="N" />

                <table class="b-makeorder__fastorder">
                    <tr>
                        <td>Мобильный телефон</td>
                        <td class="b-input-holder">
                            <div>
                                <span class="error-tooltip" id="error-mobile">Укажите номер телефона<span class="error-triangle"></span></span>
                                <input name="mobile" required type="text" class="b-makeorder__fastorder-gray b-makeorder__userphone" placeholder="+7"/>
                            </div>
                        </td>
                        <td rowspan="2" style="vertical-align:top; padding-top: 40px;">
                            <p>По указанному номеру позвонит менеджер магазина, чтобы подтвердить заказ. Статус и подтверждение заказа будут отправлены по почте и смс.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>Имя и фамилия</td>
                        <td>
                            <div class="b-cross__holder">
                                <span class="error-tooltip" id="error-name">Укажите фамилию, имя<span class="error-triangle"></span></span>
                                <input type="text" name="name" required class="b-makeorder__fastorder-cross b-makeorder__fastorder-yellow" placeholder="Фамилия, имя"/><span class="b-makeorder__fastorder-cross-span"></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
<?
            include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");

			if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
				echo $arResult["PREPAY_ADIT_FIELDS"];
			?>

			<?if($_POST["is_ajax_post"] != "Y")
			{
				?>
					</div>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<input type="hidden" name="json" value="Y">

				</form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N")
				{
					?>
					<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
					<?
				}
			}
			else
			{
				?>
				<script type="text/javascript">
					top.BX('confirmorder').value = 'Y';
					top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
		}
	}
	?>
	</div>
</div>

<?if(CSaleLocation::isLocationProEnabled()):?>

	<div style="display: none">
		<?// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.steps",
			".default",
			array(
			),
			false
		);?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:sale.location.selector.search",
			".default",
			array(
			),
			false
		);?>
	</div>

<?endif?>

<script type="text/x-template" id="remove-from-basket">
    <div class="feedback-thank">
        <h2>Удалить товар из корзины?</h2>
        <p class="muted">Вот те раз! Все ведь шло так хорошо, вы же почти его купили, как же так((</p>
        <a class="fl_l js-button js-button-white" href="javascript:void(0)">Вернуться в корзину</a>
        <a class="fl_r js-button js-button-yellow" href="javascript:void(0)">Удалить товар</a>
    </div>
</script>

<script>
    function selectOrder(link)
    {
        var $tabs = $('.b-makeorder__tab');
        var $that = $(link);
        if ($that.hasClass('b-makeorder__currenttab')) {
            return false;
        }
        if ($that.hasClass('fast')) {
            $('#fast-order').val('Y');
        } else {
            $('#fast-order').val('N');
        }
        $('.error-tooltip').css({"display": "none"});
        $('.b-makeorder__adress-error').removeClass('b-makeorder__adress-error');
        $( '.b-makeorder__tablink').find('a').toggleClass( 'b-makeorder__currenttab' );
        $tabs.toggleClass( 'b-makeorder__tab-current' );
    }
</script>