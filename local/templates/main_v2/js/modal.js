'use strict';
var MODAL = MODAL || {};
(function(window){

    function showPreloader(){
        var $preloader = $( '<div/>' ).addClass( "b-preloader" );
        $( 'body' ).append( $preloader );
        $preloader.fadeIn( 200 );
        $( '.b-overlay' ).addClass( 'b-overlay__popup-open' );
    };

    function showOverlay(){
        var $overlay = $( '<div/>' ).addClass( "b-overlay" );
        var overlay_class = 'b-overlay__popup-open';
        $( 'body' ).append( $overlay );
        $overlay.fadeIn( 400 );

        if( $overlay.hasClass( overlay_class ) ){
            return false;
        };
        $overlay.removeClass( overlay_class );
        $overlay.on( 'click', function(){
            removePreloader();
            closePopup();
            closeOverlay();
        } );
    };

    function showPopup( html ){
        var $popup = $( '<div class="b-popup"><a href="javascript:void(0)" class="b-popup__close"></a></div>' );

        $popup.append( html );

        $( 'body' ).append( $popup );

        var popup_width = $popup.outerWidth(), popup_height = $popup.outerHeight();
        $popup.css( { "margin-left": - popup_width / 2, "margin-top": - popup_height / 2 } );
        $popup.fadeIn( 400 );

        $( '.b-overlay' ).addClass( 'b-overlay__popup-open' );

        var $close_button = $( '.b-popup__close' );
        $close_button.on( 'click', function(){
            removePreloader();
            closePopup();
            closeOverlay();
        } );

        var $cancel_link = $( '.b-popup__close-link' );
        $cancel_link.on( 'click', function(){
            removePreloader();
            closePopup();
            closeOverlay();
        } );
    };

    function removePreloader(){
        $( '.b-preloader' ).remove();
    };

    function closePopup(){
        $( '.b-popup' ).remove();
    };

    function closeOverlay(){
        $( '.b-overlay' ).remove();
    };

    MODAL.showPopup = showPopup;
    MODAL.showPreloader = showPreloader;
    MODAL.showOverlay = showOverlay;
    MODAL.removePreloader = removePreloader;
    MODAL.closePopup = closePopup;
    MODAL.closeOverlay = closeOverlay;

    return MODAL;

})(window);/**
 * Created by iceburger on 24.05.2015.
 */
