$(document).ready(function() {
//    var dropdownSearch = $(".dropdown-search"),
//        dropdownSearchToggle = $(".dropdown-search-toggle");
//
//    dropdownSearchToggle.on('click', function() {
//        if(dropdownSearch.css('display')==='block') { dropdownSearch.hide(); } else {$('.dropdown-menu').hide();dropdownSearch.show(); }
//    });
//
//    var dropdownMenuToggle = $(".dropdown-menu-toggle");
//    dropdownMenuToggle.on('click', function() {
//        var dropdownMenu = $(this).parent().find('.menu-' + $(this).data('menu'));
//        if(dropdownMenu.css('display')==='block') { dropdownMenu.hide(); } else { $('.dropdown-menu').hide(); $('.dropdown-search').hide();dropdownMenu.show(); }
//    });


    var currentPage = 1;
    $('.btn-product-more-list').on('click', function() {
        el = $(this);
        postData = {
            action: 'getProducts',
            filter: $('#arFilter').val(),
            sectionId: $('#sectionId').val(),
            PAGEN_1: ++currentPage,
            PAGEN_32: currentPage
        };    
    
        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
    
                el.before(result['PRODUCTS_LIST']);
                $('.btn-product-more-card').before(result['PRODUCTS_CARD']);
                if (currentPage * 30 >= $('#total-count-items').html()) {
                    $('.btn-product-more').remove();
                } else if ((currentPage + 1) * 30 > $('#total-count-items').html()) {
                    var countItems = $('#total-count-items').html() - (currentPage * 30);
                    $('.btn-product-more__text').html('<span>Показать еще ' + countItems + '</span>');
                    $('.total-show-count').html(currentPage * 30);
                } else {
                    $('.total-show-count').html(currentPage * 30);
                }
            }
        });
        return false;
    });
    
    $('.btn-product-more-card').on('click', function() {
        el = $(this);
        postData = {
            action: 'getProducts',
            filter: $('#arFilter').val(),
            sectionId: $('#sectionId').val(),
            PAGEN_1: ++currentPage,
            PAGEN_32: currentPage
        };
    
        BX.ajax({
            url: '/ajax/index.php',
            method: 'POST',
            data: postData,
            dataType: 'json',
            onsuccess: function(result)
            {
                BX.closeWait();
    
                el.before(result['PRODUCTS_CARD']);
                $('.btn-product-more-list').before(result['PRODUCTS_LIST']);
                if (currentPage * 30 >= $('#total-count-items').html()) {
                    $('.btn-product-more').remove();
                } else if ((currentPage + 1) * 30 > $('#total-count-items').html()) {
                    var countItems = $('#total-count-items').html() - (currentPage * 30);
                    $('.btn-product-more__text').html('<span>Показать еще ' + countItems + '</span>');
                    $('.total-show-count').html(currentPage * 30);
                } else {
                    $('.total-show-count').html(currentPage * 30);
                }
            }
        });
        return false;
    });
        
$('.switch_view.lines').on('click',function(){
    $('.lines').addClass('active');
    $('.blocks').removeClass('active');
});
$('.switch_view.blocks').on('click',function(){
    $('.lines').removeClass('active');
    $('.blocks').addClass('active');
});
    
var isFixedSupported = (function(){
    var isSupported = null;
    if (document.createElement) {
        var el = document.createElement("div");
        if (el && el.style) {
            el.style.position = "fixed";
            el.style.top = "10px";
            var root = document.body;
            if (root && root.appendChild && root.removeChild) {
                root.appendChild(el);
                isSupported = (el.offsetTop === 10);
                root.removeChild(el);
            }
        }
    }
    return isSupported;
})();
window.onload = function(){
    if (!isFixedSupported){
        // добавляем контекст для "старичков"
        document.body.className += ' no-fixed-supported';
        // имитируем position: fixed;
        var bottombar = document.getElementById('fixed-panel');
        var bottomBarHeight = bottombar.offsetHeight;
        var windowHeight = window.innerHeight;
        // обрабатываем события touch и scroll
        window.ontouchmove = function() {
            if (event.target !== bottombar){
                bottombar.style = "";
            }
        };
        window.onscroll = function(){
            var scrollTop = window.scrollY;
            bottombar.style.bottom = (scrollTop + windowHeight - bottomBarHeight) + 'px';
        };
    }
    // первичный scroll
    window.scrollBy(0, 1);
};
        
});
