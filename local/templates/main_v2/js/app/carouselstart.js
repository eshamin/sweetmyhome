/**
 * Created by icebu on 08.02.2016.
 */
function carouselInit(slider, count, rotate, btnPrev, btnNext) {
    var $o = $(slider);
    var $elem = $o.closest('div');
    if ($elem.length) {
        if ($o.children('li').length > count) {
            $elem.Carousel({
                visible: count,
                rotateBy: rotate,
                speed: 500,
                btnNext: btnNext,
                btnPrev: btnPrev,
                auto: false,
                margin: 16,
                position: "h",
                dirAutoSlide: false
            });
        } else {
            $elem.css({overflow: 'visible'});
        }
    }
}

function carouselsStart(){
    var $o = $('.b-goods__more-like-slider ul');
    var btnN, btnP;

    if ($o.length) {
        $o.each( function(){
            var $that = $( this );
            $elem = $that.closest('.b-goods__more-like-slider');
            btnN = $elem.find('.b-goods__more-like-slider-button-next');
            btnP = $elem.find('.b-goods__more-like-slider-button-prev');
            carouselInit($that, 1, 1, btnP, btnN);
        } );
    }

    $o = $( '.b-accessories__slider-wrapper').find( 'ul' );

    if( $o.length ){
        $o.each( function(){
            var $that = $( this );
            $elem = $that.closest('.b-accessories__slider');
            btnN = $elem.find('.b-accessories__slider-button-next');
            btnP = $elem.find('.b-accessories__slider-button-prev');
            btnN.css( {"display":"block"} );
            btnP.css( {"display":"block"} );
            carouselInit($that, 6, 1, btnP, btnN);
        } );
    }
}