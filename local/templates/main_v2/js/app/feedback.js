/**
 * Created by icebu on 08.02.2016.
 */
function createFeedback(){
    var $button = $( '#feedback-create, #feedback-create-bt' );
    if( !$button ){
        return false;
    }
    $button.on( 'click', function(){
        var $that = $( this );
        var productId = $that.data( 'good-id' );
        var productName = $that.data( 'good-name' );
        var feedbackData = { productId:productId, productName:productName };
        var src = MyApp[ 'templates' ][ 'popup' ][ 'feedback' ];
        var HTML = src( feedbackData );
        MODAL.showOverlay();
        MODAL.showPopup( HTML );
        putStars( $( '.b-top-panel__feedback-stars' ) );
    } );
}
function putStars( elem ){
    var $that = $( elem );
    console.log( $that );
    var $stars = $that.find( '.b-top-panel__feedback-star-likelink' );
    if( !$stars ){
        return false;
    }
    $stars.css( { "cursor":"pointer" } );
    if( $stars.length ){
        $stars.on( 'mouseenter', function(){
            var $that = $( this );
            var indice = $that.index() + 1;
            $stars
                .removeClass( 'b-top-panel__feedback-star-gold' )
                .addClass( 'b-top-panel__feedback-star-gray' );
            for( var i = 0; i < indice; i++ ){
                $stars
                    .eq( i )
                    .removeClass( 'b-top-panel__feedback-star-gray' )
                    .addClass( 'b-top-panel__feedback-star-gold' );
            }
            $( '#rating' ).val( indice );
        } );
        $stars.on( 'click', function(){
            var indice = $( this ).index() + 1;
            $stars
                .removeClass( 'b-top-panel__feedback-star-gold' )
                .addClass( 'b-top-panel__feedback-star-gray' );
            for( var i = 0; i < indice; i++ ){
                $stars
                    .eq( i )
                    .removeClass( 'b-top-panel__feedback-star-gray' )
                    .addClass( 'b-top-panel__feedback-star-gold' );
            }
            $( '#rating' ).val( indice );
        } );
    }
    feedbackCreate();
}

function feedbackCreate(){
    $( 'input[name="feedback"]').on( 'click', function( e ){
        e.preventDefault();
        var $that = $( this );
        var $form = $that.closest( 'form' );
        var errors = formValidate( $form );
        if( errors ){
            return false;
        }
        var dataToSend = serializeArrayToJson( $form.serializeArray() );
        BX.ajax( {
            url:"/ajax/",
            method:"POST",
            data:dataToSend,
            dataType:"JSON",
            onsuccess:function( data ){
                MODAL.closePopup();
                var src = MyApp[ 'templates' ][ 'popup' ][ 'feedbackadded' ];
                var HTML = src();
                MODAL.showPopup( HTML );
                setTimeout( function(){
                    MODAL.closePopup();
                    MODAL.closeOverlay();
                }, 6000 );
            },
            onerror:function( data ){
                console.log( data );
            }
        } );
    } );
}