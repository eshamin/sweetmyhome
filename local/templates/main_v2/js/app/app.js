/**
 * Created by icebu on 07.02.2016.
 */
jQuery(document).ready(function(){

    /* Masonry Init */
    masonryInit();
    /* Masonry Init */
    /* Tab Links Start */
    tabLinksStart();
    /* Tab Links Start */
    /* flexSlider Init */
    jQuery('.flexslider').flexslider({
        animation: "slide",
        directionNav: false
    });
    /* flexSlider Init */
    /* fix height init */
    fixHeight();
    /* fix height init */
    /* All carousels Init */
    carouselsStart();
    /* All carousels Init */
    /* digital spinner Init */
    if( $( '.js-digital-spinner' ) ){
        spinnerStart();
    }
    /* digital spinner Init */
    /* call back Init */
    callBackStart();
    /* call back Init */
    /* City Choice Init */
    cityChoiceStart();
    /* City Choice Init */
    /* create feedback Init */
    createFeedback();
    /* create feedback Init */
    /* product card thumbs Init */
    productThumbStart();
    /* product card thumbs Init */
    /* price-delivery-warranty Init*/
    priceDelWarTabsStart();
    /* price-delivery-warranty Init*/
    /* увеличенная картинка */
    photoZoom();
    /* увеличенная картинка */
    /* маленький фикс для высоты блока товара */
    heightTinyFix();
    /* маленький фикс для высоты блока товара */
    /* add one click order Init */
    addOneClickOrder();
    /* add one click order Init */

});

jQuery(window).resize(function() {
    fixHeight();
});

jQuery(window).scroll(function(event) {
    jQuery('#fixed-panel').css('left',- $(this).scrollLeft()+'px')
});
