/**
 * Created by icebu on 08.02.2016.
 */
function onlyDigits() {
    if( !this.value ) return false;
    this.value = this.value.replace(/[^\d]/g, "");
    if( this.value == "" || this.value == 0 ){
        this.value = 1;
    }
}

function serializeArrayToJson( array ){
    var result = {};
    var temp;
    for( var i = 0, len = array.length; i < len; i++ ){
        temp = array[ i ];
        var name = temp.name;
        result[ name ] = temp.value;
    }
    return result;
}

function formValidate( form ){
    var $form = $( form );
    var $inputs = $form.find( 'input:required, textarea:required' );
    var errors = 0;
    $inputs.each( function(){
        var $that = $( this );
        var val = $that.val() || $that.text();
        if( val ){
            $that.removeClass( 'input-error' );
        }
        else{
            $that.addClass( 'input-error' );
            errors++;
        }
    } );
    return errors;
}

function fixHeight() {
    var maxHeight = jQuery('.grid .max-height').height();
    var smallHeight = (maxHeight -  (jQuery('.grid .min-height > a:first-child img').height() + 23 + 83*2)) /4;
    jQuery('.grid .sub').each(function(){
        jQuery(this).css('padding-top',smallHeight+'px').css('padding-bottom',smallHeight+'px')
    })
}
