/**
 * Created by icebu on 07.02.2016.
 */
jQuery(document).ready(function(){

    /* Masonry Init */
    masonryInit();
    /* Masonry Init */
    /* Tab Links Start */
    tabLinksStart();
    /* Tab Links Start */
    /* flexSlider Init */
    jQuery('.flexslider').flexslider({
        animation: "slide",
        directionNav: false
    });
    /* flexSlider Init */
    /* fix height init */
    fixHeight();
    /* fix height init */
    /* All carousels Init */
    carouselsStart();
    /* All carousels Init */
    /* digital spinner Init */
    if( $( '.js-digital-spinner' ) ){
        spinnerStart();
    }
    /* digital spinner Init */
    /* call back Init */
    callBackStart();
    /* call back Init */
    /* City Choice Init */
    cityChoiceStart();
    /* City Choice Init */
    /* create feedback Init */
    createFeedback();
    /* create feedback Init */
    /* product card thumbs Init */
    productThumbStart();
    /* product card thumbs Init */
    /* price-delivery-warranty Init*/
    priceDelWarTabsStart();
    /* price-delivery-warranty Init*/
    /* увеличенная картинка */
    photoZoom();
    /* увеличенная картинка */
    /* маленький фикс для высоты блока товара */
    heightTinyFix();
    /* маленький фикс для высоты блока товара */
    /* add one click order Init */
    addOneClickOrder();
    /* add one click order Init */

});

jQuery(window).resize(function() {
    fixHeight();
});

jQuery(window).scroll(function(event) {
    jQuery('#fixed-panel').css('left',- $(this).scrollLeft()+'px')
});

/**
 * Created by icebu on 08.02.2016.
 */
function carouselInit(slider, count, rotate, btnPrev, btnNext) {
    var $o = $(slider);
    var $elem = $o.closest('div');
    if ($elem.length) {
        if ($o.children('li').length > count) {
            $elem.Carousel({
                visible: count,
                rotateBy: rotate,
                speed: 500,
                btnNext: btnNext,
                btnPrev: btnPrev,
                auto: false,
                margin: 16,
                position: "h",
                dirAutoSlide: false
            });
        } else {
            $elem.css({overflow: 'visible'});
        }
    }
}

function carouselsStart(){
    var $o = $('.b-goods__more-like-slider ul');
    var btnN, btnP;

    if ($o.length) {
        $o.each( function(){
            var $that = $( this );
            $elem = $that.closest('.b-goods__more-like-slider');
            btnN = $elem.find('.b-goods__more-like-slider-button-next');
            btnP = $elem.find('.b-goods__more-like-slider-button-prev');
            carouselInit($that, 1, 1, btnP, btnN);
        } );
    }

    $o = $( '.b-accessories__slider-wrapper').find( 'ul' );

    if( $o.length ){
        $o.each( function(){
            var $that = $( this );
            $elem = $that.closest('.b-accessories__slider');
            btnN = $elem.find('.b-accessories__slider-button-next');
            btnP = $elem.find('.b-accessories__slider-button-prev');
            btnN.css( {"display":"block"} );
            btnP.css( {"display":"block"} );
            carouselInit($that, 6, 1, btnP, btnN);
        } );
    }
}
/**
 * Created by icebu on 08.02.2016.
 */
function createFeedback(){
    var $button = $( '#feedback-create, #feedback-create-bt' );
    if( !$button ){
        return false;
    }
    $button.on( 'click', function(){
        var $that = $( this );
        var productId = $that.data( 'good-id' );
        var productName = $that.data( 'good-name' );
        var feedbackData = { productId:productId, productName:productName };
        var src = MyApp[ 'templates' ][ 'popup' ][ 'feedback' ];
        var HTML = src( feedbackData );
        MODAL.showOverlay();
        MODAL.showPopup( HTML );
        putStars( $( '.b-top-panel__feedback-stars' ) );
    } );
}
function putStars( elem ){
    var $that = $( elem );
    console.log( $that );
    var $stars = $that.find( '.b-top-panel__feedback-star-likelink' );
    if( !$stars ){
        return false;
    }
    $stars.css( { "cursor":"pointer" } );
    if( $stars.length ){
        $stars.on( 'mouseenter', function(){
            var $that = $( this );
            var indice = $that.index() + 1;
            $stars
                .removeClass( 'b-top-panel__feedback-star-gold' )
                .addClass( 'b-top-panel__feedback-star-gray' );
            for( var i = 0; i < indice; i++ ){
                $stars
                    .eq( i )
                    .removeClass( 'b-top-panel__feedback-star-gray' )
                    .addClass( 'b-top-panel__feedback-star-gold' );
            }
            $( '#rating' ).val( indice );
        } );
        $stars.on( 'click', function(){
            var indice = $( this ).index() + 1;
            $stars
                .removeClass( 'b-top-panel__feedback-star-gold' )
                .addClass( 'b-top-panel__feedback-star-gray' );
            for( var i = 0; i < indice; i++ ){
                $stars
                    .eq( i )
                    .removeClass( 'b-top-panel__feedback-star-gray' )
                    .addClass( 'b-top-panel__feedback-star-gold' );
            }
            $( '#rating' ).val( indice );
        } );
    }
    feedbackCreate();
}

function feedbackCreate(){
    $( 'input[name="feedback"]').on( 'click', function( e ){
        e.preventDefault();
        var $that = $( this );
        var $form = $that.closest( 'form' );
        var errors = formValidate( $form );
        if( errors ){
            return false;
        }
        var dataToSend = serializeArrayToJson( $form.serializeArray() );
        BX.ajax( {
            url:"/ajax/",
            method:"POST",
            data:dataToSend,
            dataType:"JSON",
            onsuccess:function( data ){
                MODAL.closePopup();
                var src = MyApp[ 'templates' ][ 'popup' ][ 'feedbackadded' ];
                var HTML = src();
                MODAL.showPopup( HTML );
                setTimeout( function(){
                    MODAL.closePopup();
                    MODAL.closeOverlay();
                }, 6000 );
            },
            onerror:function( data ){
                console.log( data );
            }
        } );
    } );
}
function heightTinyFix(){
	if( $( '.b-goods__good-more-about').length ){
		var good_hei = $( '.b-goods__good-more-about' ).outerHeight();
		good_hei = ( good_hei < 400 ) ? 400:good_hei;
		$( '.b-goods__good').css( { "min-height":good_hei } );
	};
}

function photoZoom(){
	var $zoom = $( '.b-goods__good-photo-zoom' );
	if( !$zoom ){
		return false;
	}
	$zoom.on( 'click', function(){
		MODAL.showOverlay();
		$( '.b-overlay' ).css( { "background":"#353535" } );
		var id = 'good-gallery';
		renderPopupHTML( '#' + id, null, 'b-popup__good' );
	} );
}

function priceDelWarTabsStart(){
	var $del_container = $( '.b-goods__good-more-delivery' );
	if( !$del_container ){
		return false;
	}
	var $delivery_tablinks = $del_container.find( '.b-makeorder__tablinks_2').find( 'a' );
	var $delivery_tabs = $del_container.find( '.b-makeorder__tabs' );

	$delivery_tablinks.on( 'click', function(){
		var $that = $( this );
		var indice = $that.index();

		if( $that.hasClass( 'b-makeorder__currenttab' ) ){
			return false;
		}
		$del_container.find( '.b-makeorder__currenttab' ).removeClass( 'b-makeorder__currenttab' );
		$del_container.find( '.b-makeorder__tabs-current').removeClass( 'b-makeorder__tabs-current' );
		$that.addClass( 'b-makeorder__currenttab' );
		$delivery_tabs.eq( indice).addClass( 'b-makeorder__tabs-current' );
	} );
}

function productThumbStart(){
	var $thumbs = $( '.b-goods__good-photo-thumbs li' );
	if( !$thumbs ){
		return false;
	}
	$thumbs.on( 'click', function(){
		var $that = $( this );
		$( '.b-goods__good-photo-thumbs-current').removeClass( 'b-goods__good-photo-thumbs-current' );
		$that.addClass( 'b-goods__good-photo-thumbs-current' );
		var src = $that.find( 'a' ).data( 'img' );
		$( '.b-goods__good-photo-main a' ).prop( 'href', src );
		$( '.b-goods__good-photo-main a img' ).prop( 'src', src );
		$( '.cloud-zoom, .cloud-zoom-gallery' ).CloudZoom();
	} );
}

function cityChoiceStart(){
	var $city_panel_toggler = $( '.b-infolist' ).find( '.dropdown-menu-toggle' );
	var $city_panel = $city_panel_toggler.find( '.b-dropright-catalog' );
	$city_panel_toggler.on( 'mouseover', function(){
		$city_panel.fadeIn();
	} );
	$city_panel_toggler.on( 'mouseleave', function(){
		$city_panel.fadeOut();
	} );
	$( '.js-infolist__confirm' ).on( 'click', function(){
		$city_panel.fadeOut();
		var city = $city_panel.find( 'input[name=selected_city]' ).val();
		$city_panel_toggler.find( '.info-location' ).html( city );
	} );
	$( '.js-infolist__choice').on( 'click', function(){
		$city_panel.fadeOut();
		var src = MyApp[ 'templates' ][ 'popup' ][ 'yourcitylist' ];
		var HTML = src();
		MODAL.showOverlay();
		MODAL.showPopup( HTML );
	} );
	$( document ).on( 'click', '.abc-city-list a, .preselected-city a', function(){
		var $that = $( this );
		var val = $that.html();
		$( 'input[name="selected_city"]' ).val( val );
		$( '.b-infolist .location').html( val );
		MODAL.closePopup();
		MODAL.closeOverlay();
		$( '.js-infolist__confirm' ).trigger( 'click' );
	} );
	$(document).on('click', '.showCities', function() {
		var letter = $(this).html();
		var postData = {
			'action': 'getCities',
			'letter': letter
		};

		BX.ajax({
			url: '/ajax/index.php',
			method: 'POST',
			data: postData,
			dataType: 'json',
			onsuccess: function(result)
			{
				BX.closeWait();
				if (result['CODE'] == 'SUCCESS') {
					$('.abc-city-list').html(result['CITIES']);
				}
			}
		});
	});
}

function callBackStart(){
	$( '.contacts-callback' ).on( 'click', function(){
		var src = MyApp[ 'templates' ][ 'popup' ][ 'oneclickcall' ];
		var HTML = src();
		MODAL.showOverlay();
		MODAL.showPopup( HTML );
		makeDateTimePicker();
		$("#user-phone").mask("7(999)999-9999");
	} );
}

function tabLinksStart(){
	var $tablink = $( '.b-params-tabs-link' );
	if( !$tablink ){
		return false;
	}
	$tablink.on( 'click', function(){
		var $tab = $( '.b-params__tabs-tab-current' );
		var $that = $( this );
		if( $that.hasClass( 'b-params-tabs-link-current' ) ){
			return false;
		};
		var indice = $that.index();
		$( '.b-params-tabs-link-current' ).removeClass( 'b-params-tabs-link-current' );
		$tab.removeClass( 'b-params__tabs-tab-current' );
		$tablink.eq( indice).addClass( 'b-params-tabs-link-current' );
		$( '.b-params__tabs-tab' ).eq( indice).addClass( 'b-params__tabs-tab-current' );

		/* поправим высоту параметров */
		tab_hei = $tab.outerHeight();
		if( tab_hei > 727 ) {
			$('.b-params').css({"height": tab_hei});
		}
		/* поправим высоту параметров */
	} );
}

function masonryInit(){
	// Инициализация Масонри, после загрузки изображений
	var $container = $('.grid');
	if(!$container){
		return false;
	}
	$container.imagesLoaded( function() {
		$container.masonry({
			columnWidth: '.grid-sizer',
			itemSelector: '.grid-item',
			percentPosition: true
		});
	});
}

function spinnerStart(){
	$( '.b-goods__good-more-buy .js-digital-spinner-up').on( 'click', function(){
		var $that = $( this );
		var $input = $that.closest( '.js-digital-spinner').find( 'input' );
		var val = parseInt( $input.val() );
		if( !val || val == 0 ){
			val = 1;
			$input.val( val );
		};
		val++;
		$input.val( val );
	} );
	$( '.b-goods__good-more-buy .js-digital-spinner-down').on( 'click', function(){
		var $that = $( this );
		var $input = $that.closest( '.js-digital-spinner').find( 'input' );
		var val = parseInt( $input.val() );
		if( !val || val == 0 ){
			val = 1;
			$input.val( val );
		};
		if( val == 1 ){
			return false;
		};
		val--;
		$input.val( val );
	} );
}

function addOneClickOrder(){
	$('#order-oneclick').on('click', function(e) {
		var phone_maska=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
		$phone = $("#oneclick-phone");
		phone = $phone.val();
		if (!(phone_maska.test(phone))){
			$phone.addClass('input-error');
		}else{
			$phone.removeClass('input-error');
			postData = {
				action: 'addOneClickOrder',
				mobile: $phone.val(),
				productId: $(this).attr('item-id')
			};
			BX.ajax({
				url: '/ajax/',
				method: 'POST',
				data: postData,
				dataType: 'json',
				onsuccess: function (result) {
					BX.closeWait();
					if (result['CODE'] == 'SUCCESS') {
						var src = MyApp[ 'templates' ][ 'popup' ][ 'oneclickordercreated' ];
						var HTML = src( { "order":result['ORDER_ID'] } );
						MODAL.showOverlay();
						MODAL.showPopup( HTML );
					} else {
						alert(result['MESSAGE']);
					}
				}
			});
		}
		return false;
	});
}

function validateOneClickForm(){
	var oneClickForm = $( '#one-click' );
	if( !oneClickForm ){
		return false;
	}
	var requiredInputs = oneClickForm.find( '.required' ).find( 'input' );
	var is_validate = true;
	requiredInputs.each( function(){
		var that = $( this );
		if( that.val() == '' ){
			that.addClass( 'b-input__error' );
			is_validate = false;
		}
		else{
			that.removeClass( 'b-input__error' );
		}
	} );
	return is_validate;
}

function makeDateTimePicker() {
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '<Пред',
		nextText: 'След>',
		currentText: 'Сегодня',
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
		dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
		dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
		dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
		weekHeader: 'Не',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};

	$.datepicker.setDefaults($.datepicker.regional['ru']);

	$("#call-date").datepicker({minDate: 0, maxDate: "+3D"});

	$('#call-time').timepicker({
		showPeriodLabels: false, // показывать обозначения am и pm
		hourText: 'Часы', // название заголовка часов
		minuteText: 'Минуты', // название заголовка минут
		showMinutes: true, // показывать блок с минутами
		rows: 4, // сколько рядов часов и минут выводить
		timeSeparator: ':', // разделитель часов и минут
		defaultTime: '', // не подсвечивать время
		hours: {
			starts: 10, // настройка часов
			ends: 20 // от - до
		},
		minutes: {
			starts: 0, // настройка минут
			ends: 40, // от - до
			interval: 20 // интервал между минутами
		},
		onHourShow: function (hour) {
			var check_dat = $('#call-date').datepicker('getDate');
			if (check_dat) {
				var dat = new Date();
				var day_now = dat.getDate();
				var day_selected = check_dat.getDate();
				if (day_now < day_selected) {
					return true;
				}
				else {
					var hour_now = dat.getHours();
					if (hour < hour_now) {
						return false;
					}
					else {
						if (hour == hour_now) {
							return dat.getMinutes() <= 30;
						}
						else {
							return true;
						}
					}
				}
			}
			else {
				return false;
			}
		},
		onMinuteShow: function (hour, minute) {
			var check_dat = $('#call-date').datepicker('getDate');
			if (check_dat) {
				var dat = new Date();
				var day_now = dat.getDate();
				var day_selected = check_dat.getDate();
				if (day_now < day_selected) {
					return true;
				}
				var hour_now = dat.getHours();
				if (hour > hour_now) {
					return true;
				}
				else {
					var min_now = dat.getMinutes();
					if (minute < min_now) {
						return false;
					}
					else {
						return (minute - min_now) > 10;
					}
				}
			}
			else {
				return false;
			}
		}
	});
	/* Call back modal section */

	$(document).on('keydown', '#call-time', function () {
		return false;
	});
	$(document).on('keydown', '#call-date', function () {
		return false;
	});

	$(document).on('change', '#call-date', function () {
		$('#call-time').val('');
	});
}


this["MyApp"] = this["MyApp"] || {};
this["MyApp"]["templates"] = this["MyApp"]["templates"] || {};
this["MyApp"]["templates"]["popup"] = this["MyApp"]["templates"]["popup"] || {};
this["MyApp"]["templates"]["popup"]["feedback"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var alias1=this.lambda, alias2=this.escapeExpression;

  return "<div class=\"feedback-create\">\r\n    <h2>Отзыв о товаре</h2>\r\n    <p class=\"feedback-name\">"
    + alias2(alias1((depth0 != null ? depth0.productName : depth0), depth0))
    + "</p>\r\n    <form action=\"\" method=\"post\">\r\n        <input type=\"hidden\" value=\""
    + alias2(alias1((depth0 != null ? depth0.productId : depth0), depth0))
    + "\" name=\"productId\">\r\n        <input type=\"hidden\" value=\""
    + alias2(alias1((depth0 != null ? depth0.productName : depth0), depth0))
    + "\" name=\"productName\">\r\n        <input type=\"hidden\" value=\"addFeedback\" name=\"action\">\r\n        <input type=\"hidden\" value=\"add\" name=\"feedback\">\r\n        <div class=\"feedback-create-positive\">\r\n            <p>Достоинства</p>\r\n            <textarea name=\"positive\" id=\"positive\" cols=\"30\" rows=\"10\"></textarea>\r\n        </div>\r\n        <div class=\"feedback-create-negative\">\r\n            <p>Недостатки</p>\r\n            <textarea name=\"negative\" id=\"negative\" cols=\"30\" rows=\"10\"></textarea>\r\n        </div>\r\n        <div class=\"clear\"></div>\r\n        <div class=\"feedback-create-comments\">\r\n            <p>Комментарии</p>\r\n            <textarea name=\"comments\" id=\"comments\" cols=\"30\" rows=\"10\" required></textarea>\r\n        </div>\r\n        <p>\r\n            Оценка \r\n            <span class=\"b-top-panel__feedback-stars\">\r\n                <span class=\"b-top-panel__feedback-star b-top-panel__feedback-star-gray b-top-panel__feedback-star-likelink\"></span>\r\n                <span class=\"b-top-panel__feedback-star b-top-panel__feedback-star-gray b-top-panel__feedback-star-likelink\"></span>\r\n                <span class=\"b-top-panel__feedback-star b-top-panel__feedback-star-gray b-top-panel__feedback-star-likelink\"></span>\r\n                <span class=\"b-top-panel__feedback-star b-top-panel__feedback-star-gray b-top-panel__feedback-star-likelink\"></span>\r\n                <span class=\"b-top-panel__feedback-star b-top-panel__feedback-star-gray b-top-panel__feedback-star-likelink\"></span>\r\n            </span>\r\n        </p>\r\n        <input type=\"hidden\" name=\"rating\" value=\"\" id=\"rating\" />\r\n        <div class=\"feedback-create-personal\">\r\n            <div class=\"feedback-create-personal-name\">\r\n                <p>Ваше имя</p>\r\n                <input type=\"text\" name=\"username\" required/>\r\n            </div>\r\n            <div class=\"feedback-create-personal-mail\">\r\n                <p>Ваш e-mail</p>\r\n                <input type=\"text\" name=\"usermail\" required/>\r\n            </div>\r\n            <input type=\"submit\" class=\"btn btn-y\" name=\"feedback\" value=\"Сохранить\" />\r\n        </div>\r\n    </form>\r\n</div>";
},"useData":true});
this["MyApp"]["templates"]["popup"]["feedbackadded"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"feedback-thank\">\r\n    <h2>Отзыв о товаре</h2>\r\n    <p class=\"feedback-name\"></p>\r\n    <p>Спасибо! Ваш отзыв появится на сайте после проверки модератором.</p>\r\n</div>\r\n";
},"useData":true});
this["MyApp"]["templates"]["popup"]["oneclickcall"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    var helper, alias1=helpers.helperMissing, alias2="function", alias3=this.escapeExpression;

  return "<div id=\"one-click\">\r\n    <h5 class=\"b-popup__h5\"><span class=\"one-click-title\">Заказать обратный звонок</span> <span class=\"close-one\"></span></h5>\r\n    <div class=\"all-box\">\r\n        <form>\r\n            <div class=\"outer-box required g-left b-oneclick__box b-oneclick__box--first\">\r\n                <label for=\"name\" class=\"b-oneclick__box-label\">Ваше имя<span style=\"color:#d10000;\">*</span></label>\r\n                <div class=\"input_box \">\r\n                    <input type=\"text\" name=\"name\" id=\"user-name\" value=\"\" placeholder=\"Укажите имя\" class=\"b-popup__input\">\r\n                </div>\r\n                <div class=\"clear\"></div>\r\n            </div>\r\n            <div class=\"outer-box required g-left b-oneclick__box\">\r\n                <label for=\"phone\" class=\"b-oneclick__box-label\">Контактный телефон<span style=\"color:#d10000;\">*</span></label>\r\n                <div class=\"input_box \">\r\n                    <input type=\"text\" name=\"phone\" id=\"user-phone\" value=\"\" placeholder=\"Контактный телефон\" class=\"b-popup__input\">\r\n                </div>\r\n                <div class=\"clear\"></div>\r\n            </div>\r\n            <div class=\"outer-box required g-clear g-left b-oneclick__box b-oneclick__box--first\">\r\n                <label for=\"call_date\" class=\"b-oneclick__box-label\">Дата звонка<span style=\"color:#d10000;\">*</span></label>\r\n                <div class=\"input_box\">\r\n                    <input type=\"text\" name=\"call_date\" id=\"call-date\" value=\""
    + alias3(((helper = (helper = helpers.date || (depth0 != null ? depth0.date : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"date","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Желаемая дата звонка\" class=\"b-popup__input\">\r\n                </div>\r\n                <div class=\"clear\"></div>\r\n            </div>\r\n            <div class=\"outer-box b-oneclick__box\">\r\n                <label for=\"call_time\" class=\"b-oneclick__box-label\">Время звонка</label>\r\n                <div class=\"input_box \">\r\n                    <input type=\"text\" name=\"call_time\" id=\"call-time\" value=\""
    + alias3(((helper = (helper = helpers.time || (depth0 != null ? depth0.time : depth0)) != null ? helper : alias1),(typeof helper === alias2 ? helper.call(depth0,{"name":"time","hash":{},"data":data}) : helper)))
    + "\" placeholder=\"Желаемое время звонка\" class=\"b-popup__input\">\r\n                </div>\r\n                <div class=\"clear\"></div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class=\"clear\"></div>\r\n    <fieldset class=\"b-popup__buttons\">\r\n        <a id=\"one-click-buy\" class=\"button btn btn-y b-popup__btn b-oneclick__btn\" href=\"javascript:void(0)\">Заказать</a>\r\n    </fieldset>\r\n</div>";
},"useData":true});
this["MyApp"]["templates"]["popup"]["oneclickordercreated"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div id=\"order-created\">\r\n    <div class=\"order-created\">\r\n        <h2>Спасибо, ваш заказ №<span id=\"orderID\">"
    + this.escapeExpression(this.lambda((depth0 != null ? depth0.order : depth0), depth0))
    + "</span> создан.</h2>\r\n        <p>Наш менеджер свяжется с вами в ближайшее время.</p>\r\n        <span>С уважением, команда artopt.ru</span>\r\n        <p>Наш телефон +7 800 775 90 21 бесплатно по всей России</p>\r\n    </div>\r\n</div>";
},"useData":true});
this["MyApp"]["templates"]["popup"]["yourcitylist"] = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
    return "<div class=\"city-list\">\r\n    <h2>Выберите город</h2>\r\n    <div class=\"input-holder\">\r\n        <input type=\"text\" name=\"city_name\" id=\"city_name\" placeholder=\"Введите название города\"/>\r\n        <a class=\"btn btn-y\" href=\"javascript:void(0)\">Найти</a>\r\n        <div class=\"abc-holder\">\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">А</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Б</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">В</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Г</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Д</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Е</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Ж</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">З</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">И</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Й</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">К</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Л</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">М</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Н</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">О</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">П</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Р</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">С</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Т</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">У</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Ф</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Х</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Ц</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Ч</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Ш</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Щ</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Э</a>\r\n            <a href=\"javascript:void(0)\" class=\"showCities\">Я</a>\r\n        </div>\r\n        <div class=\"divider\"></div>\r\n        <ul class=\"preselected-city\">\r\n            <li class=\"g-left\"><a href=\"?location=19\">Москва</a></li>\r\n            <li class=\"g-left\"><a href=\"?location=30\">Санкт-Петербург</a></li>\r\n            <li class=\"g-left\"><a href=\"?location=1699\">Нижний Новгород</a></li>\r\n            <li class=\"g-left\"><a href=\"?location=1832\">Самара</a></li>\r\n            <li class=\"g-left\"><a href=\"?location=2208\">Екатеринбург</a></li>\r\n            <li class=\"g-left\"><a href=\"?location=1559\">Казань</a></li>\r\n            <li class=\"g-left\"><a href=\"?location=2664\">Омск</a></li>\r\n            <li class=\"clear\">&nbsp;</li>\r\n        </ul>\r\n        <ul class=\"abc-city-list\">\r\n            <li class=\"clear\">&nbsp;</li>\r\n        </ul>\r\n    </div>\r\n</div>";
},"useData":true});
/**
 * Created by icebu on 08.02.2016.
 */
function onlyDigits() {
    if( !this.value ) return false;
    this.value = this.value.replace(/[^\d]/g, "");
    if( this.value == "" || this.value == 0 ){
        this.value = 1;
    }
}

function serializeArrayToJson( array ){
    var result = {};
    var temp;
    for( var i = 0, len = array.length; i < len; i++ ){
        temp = array[ i ];
        var name = temp.name;
        result[ name ] = temp.value;
    }
    return result;
}

function formValidate( form ){
    var $form = $( form );
    var $inputs = $form.find( 'input:required, textarea:required' );
    var errors = 0;
    $inputs.each( function(){
        var $that = $( this );
        var val = $that.val() || $that.text();
        if( val ){
            $that.removeClass( 'input-error' );
        }
        else{
            $that.addClass( 'input-error' );
            errors++;
        }
    } );
    return errors;
}

function fixHeight() {
    var maxHeight = jQuery('.grid .max-height').height();
    var smallHeight = (maxHeight -  (jQuery('.grid .min-height > a:first-child img').height() + 23 + 83*2)) /4;
    jQuery('.grid .sub').each(function(){
        jQuery(this).css('padding-top',smallHeight+'px').css('padding-bottom',smallHeight+'px')
    })
}
