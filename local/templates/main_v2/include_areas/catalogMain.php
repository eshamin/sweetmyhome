<div class="tab-pane" id="catalog">
    <div class="catalog-tab">
        <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "mainCatalog", Array(
            "IBLOCK_TYPE" => "catalog",	// Тип инфоблока
            "IBLOCK_ID" => CATALOG_IB,	// Инфоблок
            "SECTION_ID" => "",	// ID раздела
            "SECTION_CODE" => "",	// Код раздела
            "COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
            "TOP_DEPTH" => "3",	// Максимальная отображаемая глубина разделов
            "SECTION_FIELDS" => array(	// Поля разделов
                0 => "",
                1 => "",
            ),
            "SECTION_USER_FIELDS" => array(	// Свойства разделов
                0 => "",
                1 => "",
            ),
            "SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CACHE_TIME" => "36000",	// Время кеширования (сек.)
            "CACHE_GROUPS" => "N",	// Учитывать права доступа
            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
        ),
            false
        );?>
    </div>
</div>