<?
$tablinks = [
    '/order/' => 'Заказ',
    '/delivery/' => 'Доставка и услуги',
    '/payment/' => 'Оплата',
    '/returns/' => 'Гарантии и возврат',
    '/contacts/' => 'Адрес',
];
?>
<div class="b-content__tablinks">
    <?foreach ($tablinks as $tablink => $tabname) {
        $tabclass = '';
        if (stristr($_SERVER['REQUEST_URI'], $tablink) !== false) {
            $tablink = 'javascript:void(0)';
            $tabclass = ' class="b-content__currenttab"';
            
        }
    ?>
    <a href="<?=$tablink?>"<?=$tabclass?>><span><?=$tabname?></span></a>
    <?}?>
</div>
