<div class="footer-social">
    <p>Мы в соцсетях</p>
    <a href="http://vkontakte.ru/" target="_blank" title="Vkontakte">
        <span class="ico ico-vk"></span>
    </a>
    <a href="http://www.facebook.com/" target="_blank" title="Facebook">
        <span class="ico ico-fb"></span>
    </a>
    <!--
                            <a href="http://www.odnoklassniki.ru/" target="_blank" title="Odnoklassniki">
                                <span class="ico ico_ok"></span>
                            </a>
    -->
    <a href="http://www.twitter.com/" target="_blank" title="Twitter">
        <span class="ico ico_tw"></span>
    </a>
    <a href="https://plus.google.com/" target="_blank" title="Google+">
        <span class="ico ico_gp"></span>
    </a>
</div>