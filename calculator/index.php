<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Конструктор букетов");
?>
<section class="section">
<div class="container">
	<h2 class="section-title"><span>Конструктор букета</span></h2>
	<p class="section-desc">
		 Работаем для Вас
	</p>
    <div class="constructor">
        <div class="constructor-wrap">
        	<?
        	if (!isset($_REQUEST['pack'])) {
				$sectionId = 2;
        	} elseif ($_REQUEST['pack'] == 'Y') {
				$sectionId = 8;
        	}

        	if (isset($_REQUEST['florist']) && ($_REQUEST['florist'] == 'Y')) {
        		$sectionId = 3;
				global $arrFilter;

				unset($arrFilter['!PROPERTY_DESIGN_VALUE']);
				$arrFilter['PROPERTY_DESIGN_VALUE'] = 'Y';
        		/*if (!isset($_REQUEST['pack'])) {

        		} elseif ($_REQUEST['pack'] == 'Y') {
					//$arrFilter[''] = '';
        		}*/
        	}
        	?>
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "calculator", Array(
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					"CACHE_GROUPS" => "Y",	// Учитывать права доступа
					"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"COUNT_ELEMENTS" => "N",	// Показывать количество элементов в разделе
					"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
					"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
					"SECTION_CODE" => "",	// Код раздела
					"SECTION_FIELDS" => array(	// Поля разделов
						0 => "",
						1 => "",
					),
					"SECTION_ID" => $sectionId,	// ID раздела
					"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
					"SECTION_USER_FIELDS" => array(	// Свойства разделов
						0 => "UF_CSS_COLOR_CLASS",
						1 => "",
					),
					"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
					"TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
					"VIEW_MODE" => "LINE",	// Вид списка подразделов
				),
				false
			);?>

            <div class="futureBouquet">
                <ul class="futureTop">
                    <li>
                    <?if (isset($_REQUEST['pack']) && $_REQUEST['pack'] == 'Y') {
						$link = $APPLICATION->GetCurPageParam('', ['pack']);
						$class = '';
						$title = 'цветы штучно';
					} else {
						$link = $APPLICATION->GetCurPageParam('pack=Y', ['pack']);
						$class = 'active';
						$title = 'цветы пачками';
					}?>
                        <a class="<?=$class?>" href="<?=$link?>"><?=$title?></a>
                    </li>
                    <li>
                        Ваш будущий букет
                    </li>
                    <li>
                    <?if (isset($_REQUEST['florist']) && $_REQUEST['florist'] == 'Y') {
						$linkFlorist = $APPLICATION->GetCurPageParam('', ['florist']);
						$classFlorist = '';
						$title = 'Без советов';
					} else {
						$linkFlorist = $APPLICATION->GetCurPageParam('florist=Y', ['florist']);
						$classFlorist = 'active';
						$title = 'Советы флориста';
					}?>
                        <a class="<?=$classFlorist?>" href="<?=$linkFlorist?>"><?=$title?></a>
                    </li>
                </ul>
                <div id="futureBouquet">
                </div>
				<?/*$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "calculator", Array(
					"ACTION_VARIABLE" => "basketAction",	// Название переменной действия
						"ADDITIONAL_PICT_PROP_1" => "-",	// Дополнительная картинка [Каталог]
						"AUTO_CALCULATION" => "Y",	// Автопересчет корзины
						"BASKET_IMAGES_SCALING" => "adaptive",	// Режим отображения изображений товаров
						"COLUMNS_LIST_EXT" => array(	// Выводимые колонки
							0 => "NAME",
							1 => "PRICE",
							2 => "QUANTITY",
							3 => "SUM",
							4 => "DELETE",
							5 => "",
						),
						"COLUMNS_LIST_MOBILE" => array(	// Колонки, отображаемые на мобильных устройствах
							0 => "NAME",
							1 => "PRICE",
							2 => "QUANTITY",
							3 => "SUM",
							4 => "DELETE",
							5 => "",
						),
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CORRECT_RATIO" => "Y",	// Автоматически рассчитывать количество товара кратное коэффициенту
						"DEFERRED_REFRESH" => "N",	// Использовать механизм отложенной актуализации данных товаров с провайдером
						"DISCOUNT_PERCENT_POSITION" => "bottom-right",	// Расположение процента скидки
						"DISPLAY_MODE" => "extended",	// Режим отображения корзины
						"EMPTY_BASKET_HINT_PATH" => "/",	// Путь к странице для продолжения покупок
						"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",	// Текст заголовка "Подарки"
						"GIFTS_CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"GIFTS_HIDE_BLOCK_TITLE" => "N",	// Скрыть заголовок "Подарки"
						"GIFTS_HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
						"GIFTS_MESS_BTN_BUY" => "Выбрать",	// Текст кнопки "Выбрать"
						"GIFTS_MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"GIFTS_PAGE_ELEMENT_COUNT" => "4",	// Количество элементов в строке
						"GIFTS_PLACE" => "BOTTOM",	// Вывод блока "Подарки"
						"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки
						"GIFTS_SHOW_OLD_PRICE" => "N",	// Показывать старую цену
						"GIFTS_TEXT_LABEL_GIFT" => "Подарок",	// Текст метки "Подарка"
						"HIDE_COUPON" => "N",	// Спрятать поле ввода купона
						"LABEL_PROP" => "",	// Свойства меток товара
						"PATH_TO_ORDER" => "/personal/order/",	// Страница оформления заказа
						"PRICE_DISPLAY_MODE" => "Y",	// Отображать цену в отдельной колонке
						"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС
						"PRODUCT_BLOCKS_ORDER" => "props,sku,columns",	// Порядок отображения блоков товара
						"QUANTITY_FLOAT" => "N",	// Использовать дробное значение количества
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки рядом с изображением
						"SHOW_FILTER" => "Y",	// Отображать фильтр товаров
						"SHOW_RESTORE" => "Y",	// Разрешить восстановление удалённых товаров
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"TOTAL_BLOCK_DISPLAY" => array(	// Отображение блока с общей информацией по корзине
							0 => "top",
						),
						"USE_DYNAMIC_SCROLL" => "Y",	// Использовать динамическую подгрузку товаров
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_GIFTS" => "N",	// Показывать блок "Подарки"
						"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
						"USE_PRICE_ANIMATION" => "Y",	// Использовать анимацию цен
					),
					false
				);*/?>
            </div>
            <div class="futureLast">
                <p>Ваш букет соберет наш профессиональный флорист</p>
            </div>
        </div>
    </div>
</div>
 </section><? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>