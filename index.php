<?$isMain = true;?>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?>
<?$APPLICATION->SetTitle('Мой милый дом');?>
    <section class="section">
        <div class="container">
            <h2 class="section-title">
                <span>Наши потрясающие композиции</span>
            </h2>
            <p class="section-desc">Лучшее для Вас</p>
            <div class="ourFlowers tabs">
				<div class="ournav-scr">
					<ul class="ourNav dec">
						<li class="tabs__tab active"><span>готовые композиции</span></li>
						<li class="tabs__tab"><span>подарки</span></li>
						<li class="tabs__tab"><span>аксессуары</span></li>
						<li class="tabs__tab"><span>Новинки</span></li>
						<li class="tabs__tab"><span>популярные</span></li>
						<!--<li class="tabs__tab"><span>цветы в коробках</span></li>-->
					</ul>
				</div>
				<div class="tabs_content">
				<?/*Букеты*/?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "mainCatalog", Array(
					"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
						"ACTIVE" => "Y",
						"BLOCK" => "Bouquets",
						"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
						"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"CUSTOM_FILTER" => "",
						"DETAIL_URL" => "/bouquets/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
						"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
						"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"LABEL_PROP" => array(	// Свойства меток товара
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
						"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
						"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
						"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
						"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
						"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"OFFERS_FIELD_CODE" => array(	// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
						"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "mainBouquets",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Товары",	// Название категорий
						"PAGE_ELEMENT_COUNT" => "4",	// Количество элементов на странице
						"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(	// Тип цены
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
						"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
						"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
						"RCM_TYPE" => "personal",	// Тип рекомендации
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
						"SECTION_ID" => "3",	// ID раздела
						"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
						"SECTION_URL" => "/bouquets/",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства раздела
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
						"SEF_RULE" => "",	// Правило для обработки
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
						"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
						"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
						"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
						"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
						"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
						"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
						"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
					),
					false
				);?>
				<?/*Подарки*/?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "mainCatalog", Array(
					"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
						"BLOCK" => "Presents",
						"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
						"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"CUSTOM_FILTER" => "",
						"DETAIL_URL" => "/presents/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
						"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
						"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"LABEL_PROP" => array(	// Свойства меток товара
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
						"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
						"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
						"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
						"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
						"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"OFFERS_FIELD_CODE" => array(	// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
						"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "mainPresents",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Товары",	// Название категорий
						"PAGE_ELEMENT_COUNT" => "4",	// Количество элементов на странице
						"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(	// Тип цены
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
						"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
						"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
						"RCM_TYPE" => "personal",	// Тип рекомендации
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
						"SECTION_ID" => "4",	// ID раздела
						"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
						"SECTION_URL" => "/presents/",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства раздела
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
						"SEF_RULE" => "",	// Правило для обработки
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
						"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
						"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
						"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
						"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
						"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
						"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
						"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
					),
					false
				);?>
				<?/*Аксессуары*/?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "mainCatalog", Array(
					"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
						"BLOCK" => "Accessories",
						"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
						"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"CUSTOM_FILTER" => "",
						"DETAIL_URL" => "/accessories/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
						"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
						"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"LABEL_PROP" => array(	// Свойства меток товара
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
						"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
						"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
						"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
						"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
						"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"OFFERS_FIELD_CODE" => array(	// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
						"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "mainAccessories",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Товары",	// Название категорий
						"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
						"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(	// Тип цены
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
						"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
						"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
						"RCM_TYPE" => "personal",	// Тип рекомендации
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
						"SECTION_ID" => "5",	// ID раздела
						"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
						"SECTION_URL" => "/accessories/",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства раздела
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
						"SEF_RULE" => "",	// Правило для обработки
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
						"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
						"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
						"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
						"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
						"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
						"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
						"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
					),
					false
				);?>

				<?/*Новинки*/?>
				<?
				global $arrFilterNovelties;
				$arrFilterNovelties['PROPERTY_NOVELTY_VALUE'] = 'Да';
				?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "mainCatalog", Array(
					"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
						"BLOCK" => "Novelties",
						"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
						"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"CUSTOM_FILTER" => "",
						"DETAIL_URL" => "/#SECTION_CODE#/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
						"FILTER_NAME" => "arrFilterNovelties",	// Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
						"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"LABEL_PROP" => array(	// Свойства меток товара
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
						"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
						"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
						"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
						"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
						"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"OFFERS_FIELD_CODE" => array(	// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
						"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "mainNovelties",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Товары",	// Название категорий
						"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
						"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(	// Тип цены
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
						"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
						"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
						"RCM_TYPE" => "personal",	// Тип рекомендации
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
						"SECTION_ID" => "",	// ID раздела
						"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
						"SECTION_URL" => "/#SECTION_CODE#/",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства раздела
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
						"SEF_RULE" => "",	// Правило для обработки
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
						"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
						"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
						"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
						"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
						"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
						"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
						"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
					),
					false
				);?>

				<?/*Популярные*/?>
				<?
				global $arrFilterHits;
				$arrFilterHits['PROPERTY_HIT_VALUE'] = 'Да';
				?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "mainCatalog", Array(
					"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
						"BLOCK" => "Hits",
						"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
						"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"CUSTOM_FILTER" => "",
						"DETAIL_URL" => "/#SECTION_CODE#/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
						"FILTER_NAME" => "arrFilterHits",	// Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
						"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"LABEL_PROP" => array(	// Свойства меток товара
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
						"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
						"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
						"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
						"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
						"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"OFFERS_FIELD_CODE" => array(	// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
						"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "mainHits",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Товары",	// Название категорий
						"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
						"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(	// Тип цены
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
						"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
						"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
						"RCM_TYPE" => "personal",	// Тип рекомендации
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
						"SECTION_ID" => "",	// ID раздела
						"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
						"SECTION_URL" => "/#SECTION_CODE#/",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства раздела
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
						"SEF_RULE" => "",	// Правило для обработки
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SHOW_ALL_WO_SECTION" => "Y",	// Показывать все элементы, если не указан раздел
						"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
						"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
						"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
						"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
						"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
						"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
						"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
					),
					false
				);?>

				<?/*Цветы в коробках*?>
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "mainCatalog", Array(
					"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие
						"BLOCK" => "Pack",
						"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара
						"ADD_PROPERTIES_TO_BASKET" => "N",	// Добавлять в корзину свойства товаров и предложений
						"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки
						"AJAX_MODE" => "N",	// Включить режим AJAX
						"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
						"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
						"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
						"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
						"BACKGROUND_IMAGE" => "-",	// Установить фоновую картинку для шаблона из свойства
						"BASKET_URL" => "/personal/cart/",	// URL, ведущий на страницу с корзиной покупателя
						"BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства
						"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
						"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
						"CUSTOM_FILTER" => "",
						"DETAIL_URL" => "/pack/#ELEMENT_CODE#/",	// URL, ведущий на страницу с содержимым элемента раздела
						"DISABLE_INIT_JS_IN_COMPONENT" => "N",	// Не подключать js-библиотеки в компоненте
						"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
						"DISPLAY_COMPARE" => "N",	// Разрешить сравнение товаров
						"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
						"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем элементы
						"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки элементов
						"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки элементов
						"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки элементов
						"ENLARGE_PRODUCT" => "STRICT",	// Выделять товары в списке
						"FILTER_NAME" => "arrFilter",	// Имя массива со значениями фильтра для фильтрации элементов
						"HIDE_NOT_AVAILABLE" => "Y",	// Недоступные товары
						"HIDE_NOT_AVAILABLE_OFFERS" => "Y",	// Недоступные торговые предложения
						"IBLOCK_ID" => IB_CATALOG,	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
						"LABEL_PROP" => array(	// Свойства меток товара
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_MOBILE" => array(	// Свойства меток товара, отображаемые на мобильных устройствах
							0 => "NOVELTY",
							1 => "HIT",
						),
						"LABEL_PROP_POSITION" => "top-left",	// Расположение меток товара
						"LAZY_LOAD" => "N",	// Показать кнопку ленивой загрузки Lazy Load
						"LINE_ELEMENT_COUNT" => "3",	// Количество элементов выводимых в одной строке таблицы
						"LOAD_ON_SCROLL" => "N",	// Подгружать товары при прокрутке до конца
						"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"
						"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
						"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
						"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
						"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара
						"META_DESCRIPTION" => "-",	// Установить описание страницы из свойства
						"META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства
						"OFFERS_FIELD_CODE" => array(	// Поля предложений
							0 => "",
							1 => "",
						),
						"OFFERS_LIMIT" => "0",	// Максимальное количество предложений для показа (0 - все)
						"OFFERS_SORT_FIELD" => "sort",	// По какому полю сортируем предложения товара
						"OFFERS_SORT_FIELD2" => "id",	// Поле для второй сортировки предложений товара
						"OFFERS_SORT_ORDER" => "asc",	// Порядок сортировки предложений товара
						"OFFERS_SORT_ORDER2" => "desc",	// Порядок второй сортировки предложений товара
						"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
						"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
						"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
						"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
						"PAGER_TEMPLATE" => "mainPack",	// Шаблон постраничной навигации
						"PAGER_TITLE" => "Товары",	// Название категорий
						"PAGE_ELEMENT_COUNT" => "8",	// Количество элементов на странице
						"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
						"PRICE_CODE" => array(	// Тип цены
							0 => "BASE",
						),
						"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
						"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",	// Порядок отображения блоков товара
						"PRODUCT_DISPLAY_MODE" => "N",	// Схема отображения
						"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
						"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
						"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",	// Вариант отображения товаров
						"PRODUCT_SUBSCRIPTION" => "Y",	// Разрешить оповещения для отсутствующих товаров
						"PROPERTY_CODE_MOBILE" => "",	// Свойства товаров, отображаемые на мобильных устройствах
						"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],	// Параметр ID продукта (для товарных рекомендаций)
						"RCM_TYPE" => "personal",	// Тип рекомендации
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_CODE_PATH" => "",	// Путь из символьных кодов раздела
						"SECTION_ID" => "8",	// ID раздела
						"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы
						"SECTION_URL" => "/pack/",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства раздела
							0 => "",
							1 => "",
						),
						"SEF_MODE" => "Y",	// Включить поддержку ЧПУ
						"SEF_RULE" => "",	// Правило для обработки
						"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
						"SET_LAST_MODIFIED" => "Y",	// Устанавливать в заголовках ответа время модификации страницы
						"SET_META_DESCRIPTION" => "Y",	// Устанавливать описание страницы
						"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
						"SET_STATUS_404" => "N",	// Устанавливать статус 404
						"SET_TITLE" => "N",	// Устанавливать заголовок страницы
						"SHOW_404" => "N",	// Показ специальной страницы
						"SHOW_ALL_WO_SECTION" => "N",	// Показывать все элементы, если не указан раздел
						"SHOW_CLOSE_POPUP" => "Y",	// Показывать кнопку продолжения покупок во всплывающих окнах
						"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки
						"SHOW_FROM_SECTION" => "N",	// Показывать товары из раздела
						"SHOW_MAX_QUANTITY" => "N",	// Показывать остаток товара
						"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
						"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
						"SHOW_SLIDER" => "Y",	// Показывать слайдер для товаров
						"SLIDER_INTERVAL" => "3000",	// Интервал смены слайдов, мс
						"SLIDER_PROGRESS" => "N",	// Показывать полосу прогресса
						"TEMPLATE_THEME" => "blue",	// Цветовая тема
						"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
						"USE_MAIN_ELEMENT_SECTION" => "N",	// Использовать основной раздел для показа элемента
						"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами
						"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
					),
					false
				);*/?>
				</div>
				<?
				$APPLICATION->IncludeFile(
					SITE_TEMPLATE_PATH . '/include/features.php',
					[],
					['mode' => 'html']);
				?>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <h2 class="section-title">
                <span>Отзывы наших клиентов</span>
            </h2>
            <p class="section-desc">Они выбрали нас</p>
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "reviewsMain", Array(
				"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
					"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					"AJAX_MODE" => "N",	// Включить режим AJAX
					"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
					"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
					"CACHE_GROUPS" => "Y",	// Учитывать права доступа
					"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
					"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
					"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
					"DISPLAY_DATE" => "N",	// Выводить дату элемента
					"DISPLAY_NAME" => "Y",	// Выводить название элемента
					"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
					"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
					"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
					"FIELD_CODE" => array(	// Поля
						0 => "DETAIL_PICTURE",
						1 => "",
					),
					"FILTER_NAME" => "",	// Фильтр
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
					"IBLOCK_ID" => IB_REVIEWS,	// Код информационного блока
					"IBLOCK_TYPE" => "content",	// Тип информационного блока (используется только для проверки)
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
					"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
					"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
					"NEWS_COUNT" => "20",	// Количество новостей на странице
					"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
					"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
					"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
					"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
					"PAGER_TEMPLATE" => "semcvetov",	// Шаблон постраничной навигации
					"PAGER_TITLE" => "Новости",	// Название категорий
					"PARENT_SECTION" => "",	// ID раздела
					"PARENT_SECTION_CODE" => "",	// Код раздела
					"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
					"PROPERTY_CODE" => array(	// Свойства
						0 => "DEFAULT",
						1 => "",
					),
					"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
					"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
					"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
					"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
					"SET_STATUS_404" => "N",	// Устанавливать статус 404
					"SET_TITLE" => "N",	// Устанавливать заголовок страницы
					"SHOW_404" => "N",	// Показ специальной страницы
					"SORT_BY1" => "PROPERTY_DEFAULT",	// Поле для первой сортировки новостей
					"SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
					"SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
					"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
					"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
				),
				false
			);?>
        </div>
        <div class="container">
			<?
			$APPLICATION->IncludeFile(
				SITE_TEMPLATE_PATH . '/include/banners.php',
				[],
				['mode' => 'html']);
			?>
        </div>
    </section>
    <section class="section">

		<?
		$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH . '/include/floristGallery.php',
			[],
			['mode' => 'html']);
		?>

    </section>
	<?
	$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH . '/include/subscribeForm.php',
		[],
		['mode' => 'html']);
	?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>