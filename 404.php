<?php
global $APPLICATION;

if(!defined('ERROR_404')) {
    include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404", "Y");

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	CBXShortUri::CheckUri();
    $APPLICATION->SetTitle("Страница не найдена");
} else {
    require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_after.php');
}?>
    <div class="notFound">
        <p>Извините, страница не найдена.</p>
        <div class="notFound-img" style=""></div>
        <span>Возможно, Вы искали что-то из этого:</span>
        <div class="Ctop">
            <ul class="Fmenu">
                <li>
                    <a href="/calculator/">собрать букет</a>
                </li>
                <li>
                    <a href="/bouquets/">готовые букеты</a>
                </li>
            </ul>
            <ul class="Fmenu">
                <li>
                    <a href="/presents/">ПОДАРКИ</a>
                </li>
                <li>
                    <a href="/actions/">Акции</a>
                </li>
            </ul>
            <ul class="Fmenu">
                <li>
                    <a href="/about/">О НАС</a>
                </li>
                <li>
                    <a href="/about/delivery/">ДОСТАВКА И ОПЛАТА</a>
                </li>
            </ul>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
